[![Codacy Badge](https://api.codacy.com/project/badge/Grade/13e635ced51e40f48ed9583ea41d6b48)](https://www.codacy.com/app/lauramenendezperez4/smireservercore?utm_source=lauramenendez@bitbucket.org&amp;utm_medium=referral&amp;utm_content=lauramenendez/smireservercore&amp;utm_campaign=Badge_Grade)

# Herramienta web para diseñar cuestionarios

Aplicación de la parte servidor en *.NET Core 2* para la gestión y respuesta de cuestionarios que emplean **Fuzzy Rating Scales**.
A continuación se mencionan sus roles principales junto con sus funciones:

### Usuarios privilegiados

Los usuarios privilegiados están formados por el administrador y los usuarios expertos.

* El administrador gestiona estos últimos, aceptándolos y dándolos de baja del sistema si es necesario.

* Los usuarios expertos son los capacitados para crear y gestionar cuestionarios, así como usuarios genéricos dentro del mismo.

### Usuarios genéricos

Los usuarios genéricos solamente pueden responder cuestionarios que les han sido asignados. Al iniciar sesión, se les asignará un identificador alfanumérico con el que podrán seguir respondiendo el cuestionario si así lo desean.

## Estructura

### packages

Paquetes necesarios para la ejecución del proyecto, librerías externas... etc.

### serv-smire

Código fuente de la aplicación.

### tests

Pruebas unitarias tanto de los controladores como de los servicios.

## Despliegue

El proyecto completo se encuentra desplegado en [Zadeh](http://zadeh.ciencias.uniovi.es).
