﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using serv_smire.Controllers;
using serv_smire.Controllers.Impl;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Models.Cuestionarios;
using serv_smire.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using tests.Controllers.Help;

namespace tests.Controllers
{
    public class TestModelosController
    {
        private IModelosController _controller;
        private Mock<IModeloRepository> _mockRepository;

        private Data _data;

        public TestModelosController()
        {
            _mockRepository = new Mock<IModeloRepository>();
            _data = new Data();
        }

        //************ MÉTODOS GET **************

        [Fact]
        public void TestModelos_Get_NoHay()
        {
            _mockRepository.Setup(mr => mr.GetModelos(It.IsAny<string>())).Returns(new List<Modelo>());
            _controller = new ModelosController(_mockRepository.Object);

            ICollection<Modelo> mods = _controller.GetModelos(_data.cuestionario.Titulo);
            Assert.Equal(0, mods.Count);
        }

        [Fact]
        public void TestModelos_Get_SiHay()
        {
            _mockRepository.Setup(mr => mr.GetModelos(It.IsAny<string>())).Returns(new List<Modelo>() { _data.modelo0, _data.modelo1 });
            _controller = new ModelosController(_mockRepository.Object);

            ICollection<Modelo> mods = _controller.GetModelos(_data.cuestionario.Titulo);
            Assert.Equal(2, mods.Count);
        }

        //************ OPERACIONES CRUD **************

        [Fact]
        public void TestModelos_Add()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);
            _mockRepository.Setup(mr => mr.Add(It.IsAny<Modelo>())).Returns(_data.modelo1);
            _controller = new ModelosController(_mockRepository.Object);

            IActionResult response = _controller.PostModelo(_data.modelo1);

            var res = Assert.IsType<Modelo>(Validate.ValidateCreated(response));
            Assert.Equal(_data.modelo1.Nombre, res.Nombre);
        }

        [Fact]
        public void TestModelos_Add_NullParam()
        {
            _controller = new ModelosController(_mockRepository.Object);

            IActionResult response = _controller.PostModelo(null);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestModelos_Add_ExistsId()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _controller = new ModelosController(_mockRepository.Object);

            IActionResult response = _controller.PostModelo(_data.modelo0);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Modelo).ToString());
        }

        [Fact]
        public void TestModelos_Add_ExistsName()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>(), It.IsAny<string>())).Returns(true);
            _controller = new ModelosController(_mockRepository.Object);

            IActionResult response = _controller.PostModelo(_data.modelo0);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Modelo).ToString());
        }

        [Fact]
        public void TestModelos_Delete()
        {
            _mockRepository.Setup(mr => mr.GetModelo(It.IsAny<long>())).Returns(_data.modelo1);
            _mockRepository.Setup(mr => mr.Delete(It.IsAny<long>()));

            _controller = new ModelosController(_mockRepository.Object);

            IActionResult response = _controller.DeleteModelo(_data.modelo1.ID);

            var res = Assert.IsType<Modelo>(Validate.ValidateOk(response));
            Assert.Equal(_data.modelo1.Nombre, res.Nombre);
        }

        [Fact]
        public void TestModelos_Delete_NotExists()
        {
            _controller = new ModelosController(_mockRepository.Object);

            IActionResult response = _controller.DeleteModelo(_data.modelo0.ID);

            Validate.ValidateNotFoundObj(response);
        }

        [Fact]
        public void TestModelos_Delete_Original()
        {
            _mockRepository.Setup(mr => mr.GetModelo(It.IsAny<long>())).Returns(_data.modelo0);
            _mockRepository.Setup(mr => mr.Delete(It.IsAny<long>())).Callback(() =>
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            });

            _controller = new ModelosController(_mockRepository.Object);

            IActionResult response = _controller.DeleteModelo(_data.modelo0.ID);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.Unknown).ToString());        }
    }
}
