﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace tests.Controllers.Help
{
    static class Validate
    {
        public static object ValidateOk(IActionResult response)
        {
            Assert.NotNull(response);
            var res = Assert.IsType<OkObjectResult>(response);
            return res.Value;
        }

        public static object ValidateCreated(IActionResult response)
        {
            Assert.NotNull(response);
            var res = Assert.IsType<CreatedResult>(response);
            return res.Value;
        }

        public static void ValidateBadRequest(IActionResult response, string msg)
        {
            Assert.NotNull(response);
            var res = Assert.IsType<BadRequestObjectResult>(response);
            Assert.Equal(msg, res.Value.ToString());
        }

        public static void ValidateUnauthorized(IActionResult response)
        {
            Assert.NotNull(response);
            var res = Assert.IsType<ObjectResult>(response);
            Assert.Equal(StatusCodes.Status401Unauthorized, res.StatusCode);
        }

        public static void ValidateNotFoundObj(IActionResult response)
        {
            Assert.NotNull(response);
            Assert.IsType<NotFoundObjectResult>(response);
        }

        public static void ValidateNotFound(IActionResult response)
        {
            Assert.NotNull(response);
            Assert.IsType<NotFoundResult>(response);
        }
    }
}
