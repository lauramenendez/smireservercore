﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using serv_smire.Controllers;
using serv_smire.Controllers.Impl;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Helper.Security;
using serv_smire.Helper.Validate;
using serv_smire.Models.Usuario;
using serv_smire.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using tests.Controllers.Help;

namespace tests.Controllers
{
    public class TestLoginController
    {
        private ILoginController _controller;

        private Mock<IGenericoRepository> _mockGenerico;
        private Mock<IExpertoRepository> _mockExperto;
        private Mock<ICuestionarioRepository> _mockCuestionario;

        private Data _data;

        public TestLoginController()
        {
            _mockGenerico = new Mock<IGenericoRepository>();
            _mockExperto = new Mock<IExpertoRepository>();
            _mockCuestionario = new Mock<ICuestionarioRepository>();

            _data = new Data();
        }

        //************ EXPERTO **************

        [Fact]
        public void TestLogin_ExpertoCorrecto()
        {
            _mockExperto.Setup(mr => mr.GetExperto(It.IsAny<string>())).Returns(_data.expertoCif);
            _controller = new LoginController(_mockGenerico.Object, _mockExperto.Object, _mockCuestionario.Object, new ValidateUser(new JwtManager(null)));

            IActionResult response = _controller.PostExperto(_data.experto);

            var res = Assert.IsType<Experto>(Validate.ValidateOk(response));
            Assert.Equal(_data.experto.Usuario, res.Usuario);
        }

        [Fact]
        public void TestLogin_Experto_TodoNull()
        {
            _controller = new LoginController(_mockGenerico.Object, _mockExperto.Object, _mockCuestionario.Object, new ValidateUser(new JwtManager(null)));

            IActionResult response = _controller.PostExperto(new Experto());

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestLogin_Experto_UsuarioMal()
        {
            _controller = new LoginController(_mockGenerico.Object, _mockExperto.Object, _mockCuestionario.Object, new ValidateUser(new JwtManager(null)));

            IActionResult response = _controller.PostExperto(_data.experto);

            Validate.ValidateUnauthorized(response);
        }

        [Fact]
        public void TestLogin_Experto_ContrasenaMal()
        {
            _data.expertoCif.Codigo = "";
            _mockExperto.Setup(mr => mr.GetExperto(It.IsAny<string>())).Returns(_data.expertoCif);
            _controller = new LoginController(_mockGenerico.Object, _mockExperto.Object, _mockCuestionario.Object, new ValidateUser(new JwtManager(null)));

            var con = _data.experto.Contrasena;
            _data.experto.Contrasena = "experto1234";
            _data.experto.Codigo = "";
            IActionResult response = _controller.PostExperto(_data.experto);

            Validate.ValidateUnauthorized(response);

            _data.experto.Contrasena = con;
        }

        [Fact]
        public void TestLogin_Experto_NoAceptado()
        {
            _data.expertoCif.Aceptado = false;
            _mockExperto.Setup(mr => mr.GetExperto(It.IsAny<string>())).Returns(_data.expertoCif);
            _controller = new LoginController(_mockGenerico.Object, _mockExperto.Object, _mockCuestionario.Object, new ValidateUser(new JwtManager(null)));

            IActionResult response = _controller.PostExperto(_data.experto);

            Validate.ValidateUnauthorized(response);

            _data.expertoCif.Aceptado = true;
        }

        //************ GENÉRICO **************

        [Fact]
        public async void TestLogin_GenericoCorrectoNoIdent()
        {
            _data.genericoCif.Cuestionarios = Data.cuestionarios;
            _mockGenerico.Setup(mr => mr.GetGenerico(It.IsAny<string>())).Returns(_data.genericoCif);
            _mockCuestionario.Setup(mr => mr.GetCuestionarioModelo(It.IsAny<long>())).ReturnsAsync(_data.cuestionarioWithModel);

            _controller = new LoginController(_mockGenerico.Object, _mockExperto.Object, _mockCuestionario.Object, new ValidateUser(new JwtManager(null)));

            IActionResult response = await _controller.PostGenerico(_data.genericoWithCuest);

            var res = Assert.IsType<Generico>(Validate.ValidateOk(response));
            Assert.Equal(_data.genericoWithCuest.Usuario, res.Usuario);
        }

        [Fact]
        public async void TestLogin_Generico_TodoNull()
        {
            _controller = new LoginController(_mockGenerico.Object, _mockExperto.Object, _mockCuestionario.Object, new ValidateUser(new JwtManager(null)));

            IActionResult response = await _controller.PostGenerico(new Generico());

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public async void TestLogin_Generico_UsuarioMal()
        {
            _controller = new LoginController(_mockGenerico.Object, _mockExperto.Object, _mockCuestionario.Object, new ValidateUser(new JwtManager(null)));

            IActionResult response = await _controller.PostGenerico(_data.genericoWithCuest);

            Validate.ValidateUnauthorized(response);
        }

        [Fact]
        public async void TestLogin_Generico_ContrasenaMal()
        {
            _data.genericoCif.Cuestionarios = Data.cuestionarios;
            _mockGenerico.Setup(mr => mr.GetGenerico(It.IsAny<string>())).Returns(_data.genericoCif);

            _controller = new LoginController(_mockGenerico.Object, _mockExperto.Object, _mockCuestionario.Object, new ValidateUser(new JwtManager(null)));

            string contrasena = _data.genericoWithCuest.Contrasena;
            _data.genericoWithCuest.Contrasena = "meh";

            IActionResult response = await _controller.PostGenerico(_data.genericoWithCuest);
            _data.genericoWithCuest.Contrasena = contrasena;

            Validate.ValidateUnauthorized(response);
        }

        [Fact]
        public async void TestLogin_Generico_NoCuestionario()
        {
            _mockGenerico.Setup(mr => mr.GetGenerico(It.IsAny<string>())).Returns(_data.genericoCif);

            _controller = new LoginController(_mockGenerico.Object, _mockExperto.Object, _mockCuestionario.Object, new ValidateUser(new JwtManager(null)));
            
            IActionResult response = await _controller.PostGenerico(_data.genericoWithCuest);

            Validate.ValidateUnauthorized(response);
        }

        [Fact]
        public async void TestLogin_Generico_NoActivo()
        {
            _mockGenerico.Setup(mr => mr.GetGenerico(It.IsAny<string>())).Returns(_data.genericoCif);

            _controller = new LoginController(_mockGenerico.Object, _mockExperto.Object, _mockCuestionario.Object, new ValidateUser(new JwtManager(null)));

            long cuest = _data.genericoWithCuest.CuestionarioEscogidoID;
            _data.genericoWithCuest.CuestionarioEscogidoID = 1;
            
            IActionResult response = await _controller.PostGenerico(_data.genericoWithCuest);

            Validate.ValidateUnauthorized(response);

            _data.genericoWithCuest.CuestionarioEscogidoID = cuest;
        }

        [Fact]
        public async void TestLogin_GenericoCorrectoWithIdent()
        {
            _data.genericoCif2.Cuestionarios = Data.cuestionarios;
            _mockGenerico.Setup(mr => mr.CheckIdentificador(It.IsAny<string>())).Returns(true);
            _mockGenerico.Setup(mr => mr.GetGenerico(It.IsAny<string>())).Returns(_data.genericoCif2);
            _mockCuestionario.Setup(mr => mr.GetCuestionarioModelo(It.IsAny<long>(), It.IsAny<string>())).ReturnsAsync(_data.cuestionarioWithModel);

            _controller = new LoginController(_mockGenerico.Object, _mockExperto.Object, _mockCuestionario.Object, new ValidateUser(new JwtManager(null)));

            IActionResult response = await _controller.PostGenerico(_data.genericoWithCuestAndIdent);

            var res = Assert.IsType<Generico>(Validate.ValidateOk(response));
            Assert.Equal(_data.genericoWithCuestAndIdent.Usuario, res.Usuario);
        }

        [Fact]
        public async void TestLogin_GenericoIdent_NoExiste()
        {
            _data.genericoCif.Cuestionarios = Data.cuestionarios;
            _mockGenerico.Setup(mr => mr.CheckIdentificador(It.IsAny<string>())).Returns(false);
            _mockGenerico.Setup(mr => mr.GetGenerico(It.IsAny<string>())).Returns(_data.genericoCif);
            _mockCuestionario.Setup(mr => mr.GetCuestionarioModelo(It.IsAny<long>(), It.IsAny<string>())).ReturnsAsync(_data.cuestionarioWithModel);

            _controller = new LoginController(_mockGenerico.Object, _mockExperto.Object, _mockCuestionario.Object, new ValidateUser(new JwtManager(null)));

            IActionResult response = await _controller.PostGenerico(_data.genericoWithCuestAndIdent);

            Validate.ValidateUnauthorized(response);
        }
    }
}
