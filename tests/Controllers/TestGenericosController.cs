﻿using Moq;
using Xunit;
using serv_smire.Services;
using System;
using System.Collections.Generic;
using System.Text;
using tfgserver.Controllers;
using serv_smire.Controllers.Impl;
using serv_smire.Models.Usuario;
using Microsoft.AspNetCore.Mvc;
using serv_smire.Helper.ErrorHandler;
using tests.Controllers.Help;

namespace tests.Controllers
{
    public class TestGenericosController
    {
        private IGenericosController _controller;
        private Mock<IGenericoRepository> _mockRepository;
        private Data _data;
        
        public TestGenericosController()
        {
            _mockRepository = new Mock<IGenericoRepository>();
            _data = new Data();
        }

        //************ MÉTODOS GET **************

        [Fact]
        public void TestGenericos_GetAll_SiHay()
        {
            _mockRepository.Setup(mr => mr.GetGenericos()).Returns(new List<Generico>() { _data.generico, _data.generico2 });
            _controller = new GenericosController(_mockRepository.Object);

            ICollection<Generico> gen = _controller.GetGenericos();
            Assert.Equal(2, gen.Count);
        }

        [Fact]
        public void TestGenericos_GetAll_NoHay()
        {
            _mockRepository.Setup(mr => mr.GetGenericos()).Returns(new List<Generico>());
            _controller = new GenericosController(_mockRepository.Object);

            ICollection<Generico> gen = _controller.GetGenericos();
            Assert.Equal(0, gen.Count);
        }

        [Fact]
        public void TestGenericos_DisponibilidadNombre_Disponible()
        {
            _mockRepository.Setup(mr => mr.CheckExistUsername(It.IsAny<string>())).Returns(true);
            _controller = new GenericosController(_mockRepository.Object);

            bool disponible = _controller.GetDisponibilidadUsuario(_data.generico.Usuario);
            Assert.True(disponible);
        }

        [Fact]
        public void TestGenericos_DisponibilidadNombre_NoDisponible()
        {
            _mockRepository.Setup(mr => mr.CheckExistUsername(It.IsAny<string>())).Returns(false);
            _controller = new GenericosController(_mockRepository.Object);

            bool disponible = _controller.GetDisponibilidadUsuario(_data.generico.Usuario);
            Assert.False(disponible);
        }

        //************ OPERACIONES CRUD **************

        [Fact]
        public void TestGenericos_Update()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _mockRepository.Setup(mr => mr.Update(It.IsAny<Generico>()));

            _controller = new GenericosController(_mockRepository.Object);

            IActionResult response = _controller.PutGenerico(_data.generico.ID, _data.generico);

            Assert.IsType<StatusCodeResult>(response);
        }

        [Fact]
        public void TestGenericos_Update_IncorrectId()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);
            _mockRepository.Setup(mr => mr.Update(It.IsAny<Generico>()));
            _controller = new GenericosController(_mockRepository.Object);

            IActionResult response = _controller.PutGenerico(_data.generico.ID, _data.generico);

            Validate.ValidateNotFoundObj(response);
        }

        [Fact]
        public void TestGenericos_Update_NullParam()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _mockRepository.Setup(mr => mr.Update(It.IsAny<Generico>()));

            _controller = new GenericosController(_mockRepository.Object);

            IActionResult response = _controller.PutGenerico(_data.generico.ID, null);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestGenericos_Add()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);
            _mockRepository.Setup(mr => mr.CheckExistUsername(It.IsAny<string>())).Returns(false);
            _mockRepository.Setup(mr => mr.Add(It.IsAny<Generico>()));

            _controller = new GenericosController(_mockRepository.Object);

            IActionResult response = _controller.PostGenerico(_data.generico);

            var res = Assert.IsType<Generico>(Validate.ValidateCreated(response));
            Assert.Equal(_data.generico.Usuario, res.Usuario);
        }

        [Fact]
        public void TestGenericos_Add_NullParam()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);
            _mockRepository.Setup(mr => mr.Add(It.IsAny<Generico>()));

            _controller = new GenericosController(_mockRepository.Object);

            IActionResult response = _controller.PostGenerico(null);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestGenericos_Add_AlreadyExistsId()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _mockRepository.Setup(mr => mr.CheckExistUsername(It.IsAny<string>())).Returns(true);
            _mockRepository.Setup(mr => mr.Add(It.IsAny<Generico>()));

            _controller = new GenericosController(_mockRepository.Object);

            IActionResult response = _controller.PostGenerico(_data.generico);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Usuario).ToString());
        }

        [Fact]
        public void TestGenericos_Add_AlreadyExistsName()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<string>(), It.IsAny<long>())).Returns(true);
            _mockRepository.Setup(mr => mr.Add(It.IsAny<Generico>()));

            _controller = new GenericosController(_mockRepository.Object);

            IActionResult response = _controller.PostGenerico(_data.generico);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Usuario).ToString());
        }

        [Fact]
        public void TestGenericos_Delete()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _mockRepository.Setup(mr => mr.GetGenerico(It.IsAny<long>())).Returns(_data.generico);
            _mockRepository.Setup(mr => mr.Delete(It.IsAny<long>()));

            _controller = new GenericosController(_mockRepository.Object);

            IActionResult response = _controller.DeleteGenerico(_data.generico.ID);

            var res = Assert.IsType<Generico>(Validate.ValidateOk(response));
            Assert.Equal(_data.generico.Usuario, res.Usuario);
        }

        [Fact]
        public void TestGenericos_Delete_NotExists()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);

            _controller = new GenericosController(_mockRepository.Object);

            IActionResult response = _controller.DeleteGenerico(_data.generico.ID);

            Validate.ValidateNotFoundObj(response);
        }
    }
}
