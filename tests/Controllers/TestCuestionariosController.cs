using System;
using Xunit;
using serv_smire.Controllers;
using serv_smire.Services;
using serv_smire.Models.Cuestionarios;
using System.Collections.Generic;
using Moq;
using serv_smire.Controllers.Impl;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using serv_smire.Helper.ErrorHandler;
using tests.Controllers.Help;

namespace tests.Controllers
{
    public class TestCuestionariosController
    {
        private ICuestionariosController _controller;
        private Mock<ICuestionarioRepository> _mockRepository;

        public TestCuestionariosController()
        {
            _mockRepository = new Mock<ICuestionarioRepository>();
        }

        // GET: api/cuestionarios/activos

        [Fact]
        public async void TestCuestionarios_GetActivos_NoHay()
        {
            _mockRepository.Setup(mr => mr.GetCuestionariosActivos()).ReturnsAsync(new List<Cuestionario>());
            _controller = new CuestionariosController(_mockRepository.Object);

            ICollection<Cuestionario> cuests = await _controller.GetCuestionariosActivos();
            Assert.Equal(0, cuests.Count);
        }

        [Fact]
        public async void TestCuestionarios_GetActivos_SiHay()
        {
            _mockRepository.Setup(mr => mr.GetCuestionariosActivos()).ReturnsAsync(Data.cuestionarios.Where(c => c.Activo).ToList());
            _controller = new CuestionariosController(_mockRepository.Object);

            ICollection<Cuestionario> cuests = await _controller.GetCuestionariosActivos();
            Assert.Equal(1, cuests.Count);
            Assert.Equal(2, cuests.First().ID);
        }

        // GET: api/cuestionarios/all

        [Fact]
        public void TestCuestionarios_GetAll_NoHay()
        {
            _mockRepository.Setup(mr => mr.GetCuestionarios()).Returns(new List<Cuestionario>());
            _controller = new CuestionariosController(_mockRepository.Object);

            ICollection<Cuestionario> cuests = _controller.GetCuestionarios();
            Assert.Equal(0, cuests.Count);
        }

        [Fact]
        public void TestCuestionarios_GetAll_SiHay()
        {
            _mockRepository.Setup(mr => mr.GetCuestionarios()).Returns(Data.cuestionarios);
            _controller = new CuestionariosController(_mockRepository.Object);

            ICollection<Cuestionario> cuests = _controller.GetCuestionarios();
            Assert.Equal(2, cuests.Count);
        }

        // GET: api/cuestionarios/experto/{user}

        [Fact]
        public void TestCuestionarios_GetByUser_NoHay()
        {
            _mockRepository.Setup(mr => mr.GetCuestionariosUsuario(It.IsAny<long>()))
                .Returns(new List<Cuestionario>());
            _controller = new CuestionariosController(_mockRepository.Object);

            ICollection<Cuestionario> cuests = _controller.GetCuestionariosByUser(21541215);
            Assert.Equal(0, cuests.Count);
        }

        [Fact]
        public void TestCuestionarios_GetByUser_SiHay()
        {
            _mockRepository.Setup(mr => mr.GetCuestionariosUsuario(It.IsAny<long>()))
                .Returns((long id) => Data.cuestionarios.Where(c => c.Experto_ID == id).ToList());
            _controller = new CuestionariosController(_mockRepository.Object);

            ICollection<Cuestionario> cuests = _controller.GetCuestionariosByUser(2);
            Assert.Equal(2, cuests.Count);
        }

        // PUT: api/Cuestionarios/{id}

        [Fact]
        public void TestCuestionarios_Update()
        {
            DateTime fecha = new DateTime();
            Cuestionario actualizar = new Cuestionario
            {
                Titulo = "UPDATE TITLE",
                FechaModificacion = fecha
            };

            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<string>(), It.IsAny<long>())).Returns(false);
            _mockRepository.Setup(mr => mr.Update(It.IsAny<Cuestionario>())).Returns(actualizar);
            _controller = new CuestionariosController(_mockRepository.Object);

            Cuestionario cuest = Data.cuestionarios.First(c => c.Activo);
            IActionResult response = _controller.PutCuestionario(cuest.ID, cuest);

            var res = Assert.IsType<Cuestionario>(Validate.ValidateOk(response));

            Assert.Equal(actualizar.Titulo, res.Titulo);
            Assert.Equal(fecha, res.FechaModificacion);
        }

        [Fact]
        public void TestCuestionarios_Update_IncorrectId()
        {
            _controller = new CuestionariosController(_mockRepository.Object);

            IActionResult response = _controller.PutCuestionario(545, new Cuestionario());

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestCuestionarios_Update_NullParam()
        {
            _controller = new CuestionariosController(_mockRepository.Object);

            IActionResult response = _controller.PutCuestionario(1, null);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestCuestionarios_Update_NotExist()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);
            _controller = new CuestionariosController(_mockRepository.Object);

            IActionResult response = _controller.PutCuestionario(0, new Cuestionario());

            Validate.ValidateNotFoundObj(response);
        }

        [Fact]
        public void TestCuestionarios_Update_ExistTitle()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<string>(), It.IsAny<long>())).Returns(true);
            _controller = new CuestionariosController(_mockRepository.Object);

            IActionResult response = _controller.PutCuestionario(0, new Cuestionario());

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Cuestionario).ToString());
        }

        // POST: api/cuestionarios/save

        [Fact]
        public void TestCuestionarios_Add()
        {
            Cuestionario nuevo = new Cuestionario()
            {
                Titulo = "NUEVO CUESTIONARIO"
            };

            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);
            _mockRepository.Setup(mr => mr.Add(It.IsAny<Cuestionario>())).Returns(nuevo);
            _controller = new CuestionariosController(_mockRepository.Object);

            IActionResult response = _controller.PostCuestionario(nuevo);

            var res = Assert.IsType<Cuestionario>(Validate.ValidateCreated(response));
            Assert.Equal(nuevo.Titulo, res.Titulo);
        }

        [Fact]
        public void TestCuestionarios_Add_NullParam()
        {
            _controller = new CuestionariosController(_mockRepository.Object);

            IActionResult response = _controller.PostCuestionario(null);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestCuestionarios_Add_AlreadyExistsId()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _controller = new CuestionariosController(_mockRepository.Object);

            IActionResult response = _controller.PostCuestionario(new Cuestionario());

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Cuestionario).ToString());
        }

        [Fact]
        public void TestCuestionarios_Add_AlreadyExistsName()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<string>(), It.IsAny<long>())).Returns(true);
            _controller = new CuestionariosController(_mockRepository.Object);

            IActionResult response = _controller.PostCuestionario(new Cuestionario());

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Cuestionario).ToString());
        }

        // DELETE: api/Cuestionarios/{cuestionarioId}

        [Fact]
        public void TestCuestionarios_Delete()
        {
            Cuestionario borrar = new Cuestionario()
            {
                Titulo = "CUESTIONARIO BORRAR",
                RespuestasDescargadas = true
            };

            _mockRepository.Setup(mr => mr.GetCuestionario(It.IsAny<long>())).Returns(borrar);
            _mockRepository.Setup(mr => mr.Delete(It.IsAny<long>()));
            _controller = new CuestionariosController(_mockRepository.Object);

            IActionResult response = _controller.DeleteCuestionario(It.IsAny<long>());

            var res = Assert.IsType<Cuestionario>(Validate.ValidateOk(response));
            Assert.Equal(borrar.Titulo, res.Titulo);
        }

        [Fact]
        public void TestCuestionarios_Delete_SinDescargar()
        {
            Cuestionario borrar = new Cuestionario()
            {
                Titulo = "CUESTIONARIO BORRAR",
                RespuestasDescargadas = false
            };

            _mockRepository.Setup(mr => mr.GetCuestionario(It.IsAny<long>())).Returns(borrar);
            _mockRepository.Setup(mr => mr.Delete(It.IsAny<long>())).Callback(() => {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.DeleteCuestionario).ToString());
            });
            _controller = new CuestionariosController(_mockRepository.Object);

            IActionResult response = _controller.DeleteCuestionario(It.IsAny<long>());

            Validate.ValidateBadRequest(response, new { Message = ErrorMsg.getMsg(eError.DeleteCuestionario) }.ToString());
        }

        [Fact]
        public void TestCuestionarios_Delete_NotExists()
        {
            _controller = new CuestionariosController(_mockRepository.Object);

            IActionResult response = _controller.DeleteCuestionario(It.IsAny<long>());

            Validate.ValidateNotFound(response);
        }
    }
}
