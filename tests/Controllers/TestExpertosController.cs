﻿using Moq;
using Xunit;
using serv_smire.Controllers;
using serv_smire.Services;
using System;
using System.Collections.Generic;
using System.Text;
using serv_smire.Controllers.Impl;
using serv_smire.Models.Usuario;
using Microsoft.AspNetCore.Mvc;
using serv_smire.Helper.ErrorHandler;
using tests.Controllers.Help;

namespace tests.Controllers
{
    public class TestExpertosController
    {
        private IExpertosController _controller;
        private Mock<IExpertoRepository> _mockRepository;
        private Data _data;

        public TestExpertosController()
        {
            _data = new Data();
            _mockRepository = new Mock<IExpertoRepository>();
        }

        //************ MÉTODOS GET **************

        // GET: api/Expertos

        [Fact]
        public void TestExpertos_GetExpertos_NoHay()
        {
            _mockRepository.Setup(mr => mr.GetExpertos()).Returns(new List<Experto>());
            _controller = new ExpertosController(_mockRepository.Object, null);

            ICollection<Experto> exs = _controller.GetExpertos();
            Assert.Equal(0, exs.Count);
        }

        [Fact]
        public void TestExpertos_GetExpertos_SiHay()
        {
            _mockRepository.Setup(mr => mr.GetExpertos()).Returns(new List<Experto>() { _data.experto, _data.expertoNoAcep });
            _controller = new ExpertosController(_mockRepository.Object, null);

            ICollection<Experto> exs = _controller.GetExpertos();
            Assert.Equal(2, exs.Count);
        }

        // GET: api/expertos/check/{username}

        [Fact]
        public void TestExpertos_CheckUsername_Disponible()
        {
            _mockRepository.Setup(mr => mr.CheckExistUsername(It.IsAny<string>())).Returns(true);
            _controller = new ExpertosController(_mockRepository.Object, null);

            bool existe = _controller.GetDisponibilidadUsuario(It.IsAny<string>());
            Assert.True(existe);
        }

        [Fact]
        public void TestExpertos_CheckUsername_NoDisponible()
        {
            _mockRepository.Setup(mr => mr.CheckExistUsername(It.IsAny<string>())).Returns(false);
            _controller = new ExpertosController(_mockRepository.Object, null);

            bool existe = _controller.GetDisponibilidadUsuario(It.IsAny<string>());
            Assert.False(existe);
        }

        //************ OPERACIONES CRUD **************

        [Fact]
        public void TestExpertos_Add()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<string>(), It.IsAny<long>())).Returns(false);
            _mockRepository.Setup(mr => mr.Add(It.IsAny<Experto>()));
            _mockRepository.Setup(mr => mr.GetCorreosAdmin()).Returns(new List<string>());

            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.PostExperto(_data.experto);
            
            var res = Assert.IsType<Experto>(Validate.ValidateCreated(response));
            Assert.Equal(_data.experto.Usuario, res.Usuario);
        }

        [Fact]
        public void TestExpertos_Add_AlreadyExistId()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.PostExperto(_data.experto);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Usuario).ToString());
        }

        [Fact]
        public void TestExpertos_Add_AlreadyExistName()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<string>(), It.IsAny<long>())).Returns(true);
            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.PostExperto(_data.experto);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Usuario).ToString());
        }

        [Fact]
        public void TestExpertos_Add_Null()
        {
            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.PostExperto(null);
            
            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        // DELETE: api/Expertos/{id}

        [Fact]
        public void TestExpertos_Delete()
        {
            _mockRepository.Setup(mr => mr.GetExperto(It.IsAny<long>())).Returns(_data.experto);
            _mockRepository.Setup(mr => mr.Delete(It.IsAny<long>()));

            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.DeleteExperto(_data.experto.ID);

            var res = Assert.IsType<Experto>(Validate.ValidateOk(response));
            Assert.Equal(_data.experto.Usuario, res.Usuario);
        }

        [Fact]
        public void TestExpertos_Delete_NotExist()
        {
            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.DeleteExperto(It.IsAny<long>());

            Assert.IsType<NotFoundObjectResult>(response);
        }

        [Fact]
        public void TestExpertos_Delete_Admin()
        {
            _mockRepository.Setup(mr => mr.GetExperto(It.IsAny<long>())).Returns(_data.admin);
            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.DeleteExperto(_data.admin.ID);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.DeleteAdmin).ToString());
        }

        // PUT: api/Expertos/{id}

        [Fact]
        public void TestExpertos_Update()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<string>(), It.IsAny<long>())).Returns(false);
            _mockRepository.Setup(mr => mr.Update(It.IsAny<Experto>()));

            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.PutExperto(_data.experto.ID, _data.experto);

            var res = Assert.IsType<Experto>(Validate.ValidateOk(response));
            Assert.Equal(_data.experto.Usuario, res.Usuario);
        }

        [Fact]
        public void TestExpertos_Update_Null()
        {
            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.PutExperto(1, null);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestExpertos_Update_IdIncorrecto()
        {
            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.PutExperto(0, _data.experto);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestExpertos_Update_NotExist()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);
            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.PutExperto(_data.experto.ID, _data.experto);

            Assert.IsType<NotFoundObjectResult>(response);
        }

        [Fact]
        public void TestExpertos_Update_AlreadyExist()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<string>(), It.IsAny<long>())).Returns(true);
            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.PutExperto(_data.experto.ID, _data.experto);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Usuario).ToString());
        }

        // PUT: api/Expertos/contrasena/{id}

        [Fact]
        public void TestExpertos_UpdateContrasena()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _mockRepository.Setup(mr => mr.UpdateContrasena(It.IsAny<Experto>()));

            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.PutContrasena(_data.expertoNuevaCon.ID, _data.expertoNuevaCon);

            var res = Assert.IsType<Experto>(Validate.ValidateOk(response));
            Assert.Equal(_data.experto.Usuario, res.Usuario);
        }

        [Fact]
        public void TestExpertos_UpdateContrasena_Null()
        {
            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.PutContrasena(1, null);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestExpertos_UpdateContrasena_IdIncorrecto()
        {
            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.PutContrasena(55745157, _data.experto);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestExpertos_UpdateContrasena_FaltaContrasenas()
        {
            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.PutContrasena(_data.experto.ID, _data.experto);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestExpertos_UpdateContrasena_NotExist()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);

            _controller = new ExpertosController(_mockRepository.Object, null);

            IActionResult response = _controller.PutContrasena(_data.expertoNuevaCon.ID, _data.expertoNuevaCon);

            Assert.IsType<NotFoundObjectResult>(response);
        }
    }
}
