﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using serv_smire.Controllers;
using serv_smire.Controllers.Impl;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Models.Cuestionarios;
using serv_smire.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using tests.Controllers.Help;

namespace tests.Controllers
{
    public class TestRespuestasController
    {
        private IRespuestasController _controller;
        private Mock<IRespuestaRepository> _mockRepository;

        private Data _data;

        public TestRespuestasController()
        {
            _mockRepository = new Mock<IRespuestaRepository>();
            _data = new Data();
        }

        //************ MÉTODOS GET **************

        [Fact]
        public void TestRespuestas_Get_NoHay()
        {
            _mockRepository.Setup(mr => mr.GetRespuestas(It.IsAny<string>(), It.IsAny<string>())).Returns(new List<Respuesta>());
            _controller = new RespuestasController(_mockRepository.Object);

            ICollection<Respuesta> res = _controller.GetRespuestas(_data.experto.Usuario, _data.cuestionario.Titulo);
            Assert.Equal(0, res.Count);
        }

        [Fact]
        public void TestRespuestas_Get_SiHay()
        {
            _mockRepository.Setup(mr => mr.GetRespuestas(It.IsAny<string>(), It.IsAny<string>())).Returns(new List<Respuesta>() { _data.respuesta, _data.respuesta2 });
            _controller = new RespuestasController(_mockRepository.Object);

            ICollection<Respuesta> res = _controller.GetRespuestas(_data.experto.Usuario, _data.cuestionario.Titulo);
            Assert.Equal(2, res.Count);
        }

        //************ OPERACIONES CRUD **************

        [Fact]
        public async void TestRespuestas_Add()
        {
            _mockRepository.Setup(mr => mr.Add(new Respuesta[0]));

            _controller = new RespuestasController(_mockRepository.Object);

            Respuesta[] array = { _data.respuesta, _data.respuesta2 };
            IActionResult response = await _controller.PostRespuestas(array);

            var res = Assert.IsType<Respuesta[]>(Validate.ValidateOk(response));
            Assert.Equal(2, res.Length);
        }

        [Fact]
        public async void TestRespuestas_Add_NullParam()
        {
            _mockRepository.Setup(mr => mr.Add(new Respuesta[0]));

            _controller = new RespuestasController(_mockRepository.Object);

            IActionResult response = await _controller.PostRespuestas(null);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }
    }
}
