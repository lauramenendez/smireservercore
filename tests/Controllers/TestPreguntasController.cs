﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using serv_smire.Controllers;
using serv_smire.Controllers.Impl;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Models.Cuestionarios;
using serv_smire.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using tests.Controllers.Help;

namespace tests.Controllers
{
    public class TestPreguntasController
    {
        private IPreguntasController _controller;
        private Mock<IPreguntaRepository> _mockRepository;

        private Data _data;

        public TestPreguntasController()
        {
            _mockRepository = new Mock<IPreguntaRepository>();
            _data = new Data();
        }

        // GET: api/preguntas/{id}

        [Fact]
        public void TestPreguntas_Get_NoExiste()
        {
            _mockRepository.Setup(mr => mr.GetPreguntas(It.IsAny<long>())).Returns(new List<Pregunta>());
            _controller = new PreguntasController(_mockRepository.Object);

            ICollection<Pregunta> cuests = _controller.GetPreguntas(It.IsAny<long>());
            Assert.Equal(0, cuests.Count);
        }

        [Fact]
        public void TestPreguntas_Get_SiExiste()
        {
            _mockRepository.Setup(mr => mr.GetPreguntas(It.IsAny<long>())).Returns(new List<Pregunta>() { _data.pregunta, _data.pregunta2 });
            _controller = new PreguntasController(_mockRepository.Object);

            ICollection<Pregunta> cuests = _controller.GetPreguntas(It.IsAny<long>());
            Assert.Equal(2, cuests.Count);
        }

        // PUT: api/Preguntas/{id}

        [Fact]
        public void TestPreguntas_Update()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _mockRepository.Setup(mr => mr.Update(It.IsAny<Pregunta>()));
            _controller = new PreguntasController(_mockRepository.Object);

            IActionResult response = _controller.PutPregunta(_data.pregunta.ID, _data.pregunta);

            Assert.IsType<StatusCodeResult>(response);
        }

        [Fact]
        public void TestPreguntas_Update_IncorrectId()
        {
            _controller = new PreguntasController(_mockRepository.Object);

            IActionResult response = _controller.PutPregunta(545, new Pregunta());

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestPreguntas_Update_NullParam()
        {
            _controller = new PreguntasController(_mockRepository.Object);

            IActionResult response = _controller.PutPregunta(1, null);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestPreguntas_Update_NotExist()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);
            _controller = new PreguntasController(_mockRepository.Object);

            IActionResult response = _controller.PutPregunta(0, new Pregunta());

            Validate.ValidateNotFoundObj(response);
        }

        // PUT: api/Preguntas/posiciones

        [Fact]
        public void TestPreguntas_UpdatePos()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _mockRepository.Setup(mr => mr.UpdatePosiciones(It.IsAny<Pregunta>(), It.IsAny<Pregunta>()));
            _controller = new PreguntasController(_mockRepository.Object);

            Pregunta[] preguntas = { _data.pregunta, _data.pregunta2 };
            IActionResult response = _controller.PutPosiciones(preguntas);

            var res = Assert.IsType<Pregunta[]>(Validate.ValidateOk(response));

            Assert.Equal(2, res.Length);
        }

        [Fact]
        public void TestPreguntas_UpdatePos_NullParam()
        {
            _controller = new PreguntasController(_mockRepository.Object);

            IActionResult response = _controller.PutPosiciones(null);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestPreguntas_UpdatePos_IncorrectSize()
        {
            _controller = new PreguntasController(_mockRepository.Object);

            Pregunta[] preguntas = { _data.pregunta };
            IActionResult response = _controller.PutPosiciones(preguntas);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestPreguntas_UpdatePos_NotExist()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);
            _controller = new PreguntasController(_mockRepository.Object);

            Pregunta[] preguntas = { _data.pregunta, _data.pregunta2 };
            IActionResult response = _controller.PutPosiciones(preguntas);

            Validate.ValidateNotFoundObj(response);
        }

        // POST: api/Preguntas

        [Fact]
        public void TestPreguntas_Add()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(false);
            _mockRepository.Setup(mr => mr.Add(It.IsAny<Pregunta>()));
            _controller = new PreguntasController(_mockRepository.Object);

            IActionResult response = _controller.PostPregunta(_data.pregunta);

            var res = Assert.IsType<Pregunta>(Validate.ValidateCreated(response));

            Assert.Equal(_data.pregunta.Enunciado, res.Enunciado);
        }

        [Fact]
        public void TestPreguntas_Add_NullParam()
        {
            _controller = new PreguntasController(_mockRepository.Object);

            IActionResult response = _controller.PostPregunta(null);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.InvalidParam).ToString());
        }

        [Fact]
        public void TestPreguntas_Add_Exists()
        {
            _mockRepository.Setup(mr => mr.Exists(It.IsAny<long>())).Returns(true);
            _controller = new PreguntasController(_mockRepository.Object);

            IActionResult response = _controller.PostPregunta(_data.pregunta);

            Validate.ValidateBadRequest(response, ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Pregunta).ToString());
        }

        // DELETE: api/Preguntas/{id}
        [Fact]
        public void TestPreguntas_Delete()
        {
            _mockRepository.Setup(mr => mr.GetPregunta(It.IsAny<long>())).Returns(_data.pregunta);
            _mockRepository.Setup(mr => mr.Add(It.IsAny<Pregunta>()));
            _controller = new PreguntasController(_mockRepository.Object);

            IActionResult response = _controller.DeletePregunta(_data.pregunta.ID);

            var res = Assert.IsType<Pregunta>(Validate.ValidateOk(response));

            Assert.Equal(_data.pregunta.Enunciado, res.Enunciado);
        }

        [Fact]
        public void TestPreguntas_Delete_NotExists()
        {
            _controller = new PreguntasController(_mockRepository.Object);

            IActionResult response = _controller.DeletePregunta(0);

            Validate.ValidateNotFoundObj(response);
        }

    }
}
