﻿using serv_smire.Helper.Enumerators;
using serv_smire.Models.Cuestionarios;
using serv_smire.Models.Cuestionarios.TiposRespuestas;
using serv_smire.Models.Usuario;
using System;
using System.Collections.Generic;
using System.Text;

namespace tests
{
    class Data
    {
        public readonly String identificador = "PRUEBA";

        public Experto admin = new Experto
        {
            ID = 1,
            Nombre = "admin",
            Apellidos = "apellidos admin",
            Email = "admin@test.com",
            Aceptado = true,
            EsAdmin = true,
            Usuario = "admintest",
            Contrasena = "admintest123"
        };

        public Experto experto = new Experto
        {
            ID = 2,
            Nombre = "experto",
            Apellidos = "apellidos experto",
            Email = "experto@test.com",
            Aceptado = true,
            EsAdmin = false,
            Usuario = "expertotest",
            Contrasena = "expertotest123",
            Codigo = ""
        };

        public Experto experto2 = new Experto
        {
            ID = 3,
            Nombre = "experto 2",
            Apellidos = "apellidos experto 2",
            Email = "experto2@test.com",
            Aceptado = true,
            EsAdmin = false,
            Usuario = "experto2test",
            Contrasena = "experto2test123",
            Codigo = ""
        };

        public Experto expertoCif = new Experto
        {
            ID = 2,
            Nombre = "experto",
            Apellidos = "apellidos experto",
            Email = "experto@test.com",
            Aceptado = true,
            EsAdmin = false,
            Usuario = "expertotest",
            Contrasena = "/cKJ/LGT+brrq9FuK9lb0RgY3ZxNy8I6IK8m/yqjwUU=",
            Codigo = ""
        };

        public Experto expertoNoAcep = new Experto
        {
            ID = 3,
            Nombre = "experto noacep",
            Apellidos = "apellidos experto noacep",
            Email = "expertoNA@test.com",
            Aceptado = false,
            EsAdmin = false,
            Usuario = "expertotestna",
            Contrasena = "expertotestna123"
        };

        public Experto expertoNuevaCon = new Experto
        {
            ID = 4,
            Nombre = "experto",
            Apellidos = "apellidos experto",
            Email = "experto@test.com",
            Aceptado = true,
            EsAdmin = false,
            Usuario = "expertotest",
            Contrasena = "expertotest123",
            NuevaContrasena = "expertotest123NUEVA",
            RepContrasena = "expertotest123NUEVA"
        };

        public Generico generico = new Generico
        {
            ID = 1,
            Usuario = "genericotest",
            Contrasena = "genericotest123",
            Experto_ID = 2
        };

        public Generico generico2 = new Generico
        {
            ID = 2,
            Usuario = "genericotest2",
            Contrasena = "genericotest2123",
            Experto_ID = 3
        };

        public Cuestionario cuestionario = new Cuestionario
        {
            ID = 1,
            Titulo = "titulo test 1",
            Instrucciones = "<p>instrucciones test 1</p>",
            FechaCreacion = new DateTime(),
            FechaModificacion = new DateTime(),
            Activo = true,
            Experto_ID = 2,
            Generico_ID = 1
        };

        public Modelo modelo0 = new Modelo
        {
            ID = 1,
            Nombre = "Modelo 0",
            Cuestionario_ID = 1
        };

        public Modelo modelo1 = new Modelo
        {
            ID = 2,
            Nombre = "Modelo 1",
            Cuestionario_ID = 1
        };

        public  Cuestionario cuestionario2 = new Cuestionario
        {
            ID = 3,
            Titulo = "titulo test 2",
            Instrucciones = "<p>instrucciones test 2</p>",
            FechaCreacion = new DateTime(),
            FechaModificacion = new DateTime(),
            Activo = true,
            Experto_ID = 3,
            Generico_ID = 1
        };

        public Modelo modelo0_2 = new Modelo
        {
            ID = 3,
            Nombre = "Modelo 0",
            Cuestionario_ID = 3
        };

        public  Cuestionario cuestionarioWithModel = new Cuestionario
        {
            ID = 2,
            Titulo = "titulo test 2",
            Instrucciones = "<p>instrucciones test 2</p>",
            FechaCreacion = new DateTime(),
            FechaModificacion = new DateTime(),
            Activo = true,
            Experto_ID = 2,
            Generico_ID = 1,
            ModeloEscogido = modeloEscogido
        };

        public static Modelo modeloEscogido = new Modelo
        {
            ID = 1,
            Nombre = "Modelo 0",
            Cuestionario_ID = 2
        };

        public Cuestionario cuestionarioNoActivo = new Cuestionario
        {
            ID = 2,
            Titulo = "titulo no activo",
            Instrucciones = "<p>instrucciones test no activo</p>",
            FechaCreacion = new DateTime(),
            FechaModificacion = new DateTime(),
            Activo = false,
            Experto_ID = 2,
            Generico_ID = 1
        };

        public Respuesta respuesta = new Respuesta
        {
            Identificador = "ABCD1234",
            TipoRespuesta_ID = 1,
            FechaRespuesta = new DateTime()
        };

        public Respuesta respuesta2 = new Respuesta
        {
            Identificador = "ABCD1234",
            TipoRespuesta_ID = 2,
            FechaRespuesta = new DateTime()
        };

        public static ICollection<Cuestionario> cuestionarios = new List<Cuestionario> {
            new Cuestionario {
                ID = 1,
                Titulo = "titulo test 1",
                Instrucciones = "<p>instrucciones test 1</p>",
                FechaCreacion = new DateTime(),
                FechaModificacion = new DateTime(),
                Activo = false,
                Experto_ID = 2,
                Generico_ID = 1
            },
            new Cuestionario {
                ID = 2,
                Titulo = "titulo test 2",
                Instrucciones = "<p>instrucciones test 2</p>",
                FechaCreacion = new DateTime(),
                FechaModificacion = new DateTime(),
                Activo = true,
                Experto_ID = 2,
                Generico_ID = 1
            }
        };

        public  Generico genericoCif = new Generico
        {
            ID = 1,
            Usuario = "genericotest",
            Contrasena = "Z2VuZXJpY290ZXN0MTIz",
            Experto_ID = 2
        };

        public  Generico genericoCif2 = new Generico
        {
            ID = 1,
            Usuario = "genericotest",
            Contrasena = "Z2VuZXJpY290ZXN0MTIz",
            Experto_ID = 2
        };

        public  Generico genericoWithCuest = new Generico
        {
            ID = 1,
            Usuario = "genericotest",
            Contrasena = "genericotest123",
            Experto_ID = 2,
            CuestionarioEscogidoID = 2
        };

        public  Generico genericoWithCuestAndIdent = new Generico
        {
            ID = 1,
            Usuario = "genericotest",
            Contrasena = "genericotest123",
            Identificador = "identificador",
            Experto_ID = 2,
            CuestionarioEscogidoID = 2
        };

        public Generico genericoWithCuests = new Generico
        {
            ID = 1,
            Usuario = "genericotest",
            Contrasena = "genericotest123",
            Experto_ID = 2,
            Cuestionarios = cuestionarios
        };

        public Pregunta pregunta = new Pregunta()
        {
            ID = 1,
            Enunciado = "pregunta de prueba 1",
            Posicion = 1,
            Bloque_ID = 1
        };

        public Pregunta pregunta2 = new Pregunta()
        {
            ID = 2,
            Enunciado = "pregunta de prueba 2",
            Posicion = 2,
            Bloque_ID = 1
        };

        public Pregunta preguntaConTipos = new Pregunta()
        {
            ID = 3,
            Enunciado = "pregunta de prueba 1",
            Posicion = 1,
            Bloque_ID = 2
        };

        public Pregunta preguntaConTipos2 = new Pregunta()
        {
            ID = 4,
            Enunciado = "pregunta de prueba 1",
            Posicion = 1,
            Bloque_ID = 2
        };

        public Bloque bloque1 = new Bloque
        {
            ID = 1, 
            Nombre = "bloque prueba",
            Posicion = 1,
            Modelo_ID = 1
        };

        public Bloque bloque2 = new Bloque
        {
            ID = 2,
            Nombre = "bloque prueba 2",
            Posicion = 2,
            Modelo_ID = 1
        };

        public Instruccion instruccion = new Instruccion
        {
            ID = 1,
            Filename = "ins1.pdf",
            Filedata = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 },
            Cuestionario_ID = 1
        };

        public TipoRespuesta fecha = new Fecha
        {
            ID = 1
        };
        public TipoRespuesta frs = new FRS
        {
            ID = 2
        };
        public TipoRespuesta intervalo = new Intervalo
        {
            ID = 3
        };
        public TipoRespuesta numero = new Numero
        {
            ID = 4
        };
        public TipoRespuesta texto = new Texto
        {
            ID = 5
        };
        public TipoRespuesta vanalog = new VAnalogica{
            ID = 6 
        };
        public TipoRespuesta unica = new Unica
        {
            ID = 7,
            Etiquetas =
            {
                new Etiqueta
                {
                    Nombre = "etiqUni1",
                    Posicion = 1
                },
                new Etiqueta
                {
                    Nombre = "etiqUni2",
                    Posicion = 2
                }
            }
        };
        public TipoRespuesta multiple = new Multiple
            {
                ID = 8,
                Etiquetas =
                {
                    new Etiqueta
                    {
                        Nombre = "etiqMult1",
                        Posicion = 1
                    },
                    new Etiqueta
                    {
                        Nombre = "etiqMult2",
                        Posicion = 2
                    }
                }
            };

        public TipoRespuesta orden = new Orden
            {
                ID = 9,
                Etiquetas =
                {
                    new Etiqueta
                    {
                        Nombre = "etiqOrd1",
                        Posicion = 1
                    },
                    new Etiqueta
                    {
                        Nombre = "etiqOrd2",
                        Posicion = 2
                    }
                }
            };

        public TipoRespuesta likertTx = new Likert
            {
                ID = 10,
                NRespuestas = 2,
                TipoLikert = ETiposLikert.Texto,
                Etiquetas =
                {
                    new Etiqueta
                    {
                        Nombre = "likert1",
                        Posicion = 1
                    },
                    new Etiqueta
                    {
                        Nombre = "likert2",
                        Posicion = 2
                    }
                }
            };

        public TipoRespuesta likertNum = new Likert
            {
                ID = 11,
                NRespuestas = 2,
                TipoLikert = ETiposLikert.Numero
            };

        public Etiqueta etiqueta = new Etiqueta
        {
            ID = 500,
            Nombre = "etiqueta para añadir",
            Posicion = 1
        };

        /* Respuestas */
        public Respuesta GetRespuesta(string valor, long tr_id)
        {
            return new Respuesta
            {
                Identificador = identificador,
                FechaRespuesta = DateTime.Now,
                Valor = valor,
                TipoRespuesta_ID = tr_id
            };
        }
    }
}
