﻿using Microsoft.EntityFrameworkCore;
using serv_smire.Models.Cuestionarios;
using serv_smire.Services.Impl;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using tests.Repositories.Help;
using Xunit;

namespace tests.Repositories
{
    public class TestRespuestaRepository
    {
        private FakeDbContext _ctx;
        private Data _data;

        public TestRespuestaRepository()
        {
            _data = new Data();
            _ctx = new FakeDbContext(_data);
        }

        [Fact]
        public async Task TestRespuestaRepo_Add_Empty()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new RespuestaRepository(context))
            {
                await repository.Add(new Respuesta[0]);
            }
        }

        [Fact]
        public async Task TestRespuestaRepo_Add_Null() {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new RespuestaRepository(context))
            {
                await repository.Add(null);
            }
        }

        [Fact]
        public void TestRespuestaRepo_Add_TipoNoExiste()
        {
            Respuesta resp = _data.GetRespuesta("prueba", 0);

            using (var context = _ctx.GetContextWithData())
            using (var repository = new RespuestaRepository(context))
            {
                Assert.ThrowsAsync<DbUpdateException>(() => repository.Add(new Respuesta[] { resp }));
                Assert.Equal(0, repository.GetRespuestas(_data.identificador).Count);
            }
        }

        [Theory]
        [InlineData("1996-03-20", 1)]
        [InlineData("0;3;4;5", 2)]
        [InlineData("0;3", 3)]
        [InlineData("3.45", 4)]
        [InlineData("3", 6)]
        [InlineData("etiqUni1", 7)]
        [InlineData("etiqMult1;etiqMult2", 8)]
        [InlineData("etiqOrd1;etiqOrd2", 9)]
        [InlineData("likert1", 10)]
        [InlineData("3", 11)]
        public async Task TestRespuestaRepo_Add_Correcto(string valor, long id)
        {
            Respuesta resp = _data.GetRespuesta(valor, id);

            using (var context = _ctx.GetContextWithData())
            using (var repository = new RespuestaRepository(context))
            {
                await repository.Add(new Respuesta[] { resp });
                Assert.Equal(1, repository.GetRespuestas(_data.identificador).Count);

                context.Entry(resp).State = EntityState.Detached;

                repository.Delete(_data.identificador);
                Assert.Equal(0, repository.GetRespuestas(_data.identificador).Count);
            }
        }

        [Theory]
        [InlineData(null, 1)]
        [InlineData("1996", 1)]
        [InlineData("13;2;a;3", 2)]
        [InlineData("13;", 3)]
        [InlineData("13;", 4)]
        [InlineData("afsdsf", 6)]
        [InlineData("etiqUni1;etiqUni2", 7)]
        [InlineData("dsgdsf", 7)]
        [InlineData("etiqUni1", 8)]
        [InlineData("etiqOrd1", 9)]
        [InlineData("likert1;likert2", 10)]
        [InlineData("likert1", 11)]
        public void TestRespuestaRepo_Add_Excepcion(string valor, long id)
        {
            Respuesta resp = _data.GetRespuesta(valor, id);

            using (var context = _ctx.GetContextWithData())
            using (var repository = new RespuestaRepository(context))
            {
                Assert.ThrowsAsync<InvalidOperationException>(() => repository.Add(new Respuesta[] { resp }));
            }
        }
    }
}
