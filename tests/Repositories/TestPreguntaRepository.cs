﻿using Microsoft.EntityFrameworkCore;
using serv_smire.Models.Cuestionarios;
using serv_smire.Services.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tests.Repositories.Help;
using Xunit;

namespace tests.Repositories
{
    public class TestPreguntaRepository
    {
        private FakeDbContext _ctx;
        private Data _data;

        public TestPreguntaRepository()
        {
            _data = new Data();
            _ctx = new FakeDbContext(_data);
        }

        [Fact]
        public void TestPreguntaRepo_Get_Exists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                Pregunta aux = repository.GetPregunta(_data.pregunta.ID);

                Assert.NotNull(aux);
                Assert.Equal(aux.Enunciado, _data.pregunta.Enunciado);
            }
        }

        [Fact]
        public void TestPreguntaRepo_Get_NotExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                Assert.Null(repository.GetPregunta(0));
            }
        }

        [Fact]
        public void TestPreguntaRepo_GetPreguntas_CuestExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                ICollection<Pregunta> aux = repository.GetPreguntas(_data.cuestionario.ID);

                Assert.Equal(4, aux.Count);
            }
        }

        [Fact]
        public void TestPreguntaRepo_GetPreguntas_NotExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                Assert.Equal(0, repository.GetPreguntas(0).Count);
            }
        }

        [Fact]
        public void TestPreguntaRepo_GetPreguntas_Exists_Empty()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                Assert.Equal(0, repository.GetPreguntas(_data.cuestionario2.ID).Count);
            }
        }

        [Fact]
        public void TestPreguntaRepo_Add_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                Action act = () => repository.Add(null);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestPreguntaRepo_Add_BloqueNotExists()
        {
            Pregunta aux = new Pregunta
            {
                Enunciado = "Pregunta 1",
                Bloque_ID = 0
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                Action act = () => repository.Add(aux);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestPreguntaRepo_Add_Delete()
        {
            // Mismo enunciado, mismo bloque
            Pregunta aux = new Pregunta
            {
                ID = 20,
                Enunciado = "Pregunta 1",
                Bloque_ID = _data.bloque1.ID
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                repository.Add(aux);
                context.Entry(aux).State = EntityState.Detached;

                Assert.Equal(5, repository.GetPreguntas(_data.cuestionario.ID).Count);

                context.Entry(_data.pregunta).State = EntityState.Detached;
                context.Entry(_data.pregunta2).State = EntityState.Detached;

                repository.Delete(20);
                Assert.Equal(4, repository.GetPreguntas(_data.cuestionario.ID).Count);
            }
        }

        [Fact]
        public void TestPreguntaRepo_Delete_NotExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                Action act = () => repository.Delete(0);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestPreguntaRepo_UpdatePos_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                Action act = () => repository.UpdatePosiciones(_data.pregunta, null);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestPreguntaRepo_UpdatePos_NotExists()
        {
            Pregunta aux = new Pregunta
            {
                Enunciado = "Pregunta 2",
                Bloque_ID = _data.bloque1.ID
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                Action act = () => repository.UpdatePosiciones(_data.pregunta, aux);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestPreguntaRepo_UpdatePos()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                repository.UpdatePosiciones(_data.pregunta, _data.pregunta2);

                Assert.Equal(4, repository.GetPreguntas(_data.cuestionario.ID).Count);
                Assert.Equal(2, _data.pregunta.Posicion);
                Assert.Equal(1, _data.pregunta2.Posicion);

                repository.UpdatePosiciones(_data.pregunta, _data.pregunta2);
                Assert.Equal(4, repository.GetPreguntas(_data.cuestionario.ID).Count);
                Assert.Equal(1, _data.pregunta.Posicion);
                Assert.Equal(2, _data.pregunta2.Posicion);
            }
        }

        [Fact]
        public void TestPreguntaRepo_Update_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                Action act = () => repository.Update(null);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestPreguntaRepo_Update_NotExists()
        {
            Pregunta aux = new Pregunta
            {
                Enunciado = "Pregunta aux",
                Bloque_ID = _data.bloque1.ID
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                Action act = () => repository.Update(aux);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestPreguntaRepo_Update_CambiaBloque()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                var bloq = _data.pregunta.Bloque_ID;
                _data.pregunta.Bloque_ID = _data.bloque2.ID;

                repository.Update(_data.pregunta);
                Assert.Equal(_data.bloque2.ID, repository.GetPregunta(_data.pregunta.ID).Bloque_ID);

                _data.pregunta.Bloque_ID = bloq;
            }
        }

        [Fact]
        public void TestPreguntaRepo_Update_Add_Delete_TipoSinEtiq()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                _data.pregunta.TiposRespuestas.Add(_data.fecha);
                repository.Update(_data.pregunta);
                
                Assert.Equal(1, repository.GetPregunta(_data.pregunta.ID).TiposRespuestas.Count);

                _data.pregunta.TiposRespuestas.First().Deleted = true;
                repository.Update(_data.pregunta);

                Assert.Equal(0, repository.GetPregunta(_data.pregunta.ID).TiposRespuestas.Count);
            }
        }

        [Fact]
        public void TestPreguntaRepo_Update_Add_Delete_TipoEtiq()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                _data.pregunta.TiposRespuestas.Add(_data.unica);
                repository.Update(_data.pregunta);

                Assert.Equal(1, repository.GetPregunta(_data.pregunta.ID).TiposRespuestas.Count);
                Assert.Equal(2, context.Etiquetas.Where(e => e.TipoRespuesta_ID == _data.unica.ID).ToList().Count);

                _data.pregunta.TiposRespuestas.First().Deleted = true;

                repository.Update(_data.pregunta);
                Assert.Equal(0, repository.GetPregunta(_data.pregunta.ID).TiposRespuestas.Count);
            }
        }
        
        [Fact]
        public void TestPreguntaRepo_Update_Add_Delete_Etiq()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new PreguntaRepository(context))
            {
                _data.pregunta.TiposRespuestas.Add(_data.multiple);
                repository.Update(_data.pregunta);

                Assert.Equal(1, repository.GetPregunta(_data.pregunta.ID).TiposRespuestas.Count);
                Assert.Equal(2, context.Etiquetas.Where(e => e.TipoRespuesta_ID == _data.multiple.ID).ToList().Count);

                _data.pregunta.TiposRespuestas.FirstOrDefault().Etiquetas.Add(_data.etiqueta);
                repository.Update(_data.pregunta);

                Assert.Equal(1, repository.GetPregunta(_data.pregunta.ID).TiposRespuestas.Count);
                Assert.Equal(3, context.Etiquetas.Where(e => e.TipoRespuesta_ID == _data.multiple.ID).ToList().Count);

                _data.pregunta.TiposRespuestas.FirstOrDefault().Etiquetas.LastOrDefault().Deleted = true;
                repository.Update(_data.pregunta);
                Assert.Equal(1, repository.GetPregunta(_data.pregunta.ID).TiposRespuestas.Count);
                Assert.Equal(2, context.Etiquetas.Where(e => e.TipoRespuesta_ID == _data.multiple.ID).ToList().Count);

                _data.pregunta.TiposRespuestas.First().Deleted = true;

                repository.Update(_data.pregunta);
                Assert.Equal(0, repository.GetPregunta(_data.pregunta.ID).TiposRespuestas.Count);
            }
        }
    }
}

