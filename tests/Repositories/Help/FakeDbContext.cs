﻿using Microsoft.EntityFrameworkCore;
using serv_smire.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace tests.Repositories.Help
{
    class FakeDbContext
    {
        private Data _data;

        public FakeDbContext(Data data)
        {
            _data = data;
        }

        public DataModel GetContext()
        {
            var options = new DbContextOptionsBuilder<DataModel>()
                      .UseInMemoryDatabase(Guid.NewGuid().ToString())
                      .Options;
            var context = new DataModel(options);

            return context;
        }

        public DataModel GetContextWithData()
        {
            var options = new DbContextOptionsBuilder<DataModel>()
                      .UseInMemoryDatabase(Guid.NewGuid().ToString())
                      .Options;
            var context = new DataModel(options);

            context.Expertos.Add(_data.admin);
            context.Expertos.Add(_data.experto);
            context.Expertos.Add(_data.experto2);

            context.Genericos.Add(_data.generico);
            context.Genericos.Add(_data.generico2);

            context.Cuestionarios.Add(_data.cuestionario);
            context.Cuestionarios.Add(_data.cuestionarioNoActivo);
            context.Cuestionarios.Add(_data.cuestionario2);

            context.Modelos.Add(_data.modelo0);
            context.Modelos.Add(_data.modelo1);

            context.Modelos.Add(_data.modelo0_2);

            context.Bloques.Add(_data.bloque1);
            context.Bloques.Add(_data.bloque2);

            context.Instruccion.Add(_data.instruccion);

            context.Preguntas.Add(_data.pregunta);
            context.Preguntas.Add(_data.pregunta2);

            context.TipoRespuestas.Add(_data.fecha);
            context.TipoRespuestas.Add(_data.frs);
            context.TipoRespuestas.Add(_data.intervalo);
            context.TipoRespuestas.Add(_data.numero);
            context.TipoRespuestas.Add(_data.texto);
            context.TipoRespuestas.Add(_data.vanalog);
            context.TipoRespuestas.Add(_data.unica);
            context.TipoRespuestas.Add(_data.multiple);
            context.TipoRespuestas.Add(_data.orden);
            context.TipoRespuestas.Add(_data.likertTx);
            context.TipoRespuestas.Add(_data.likertNum);

            _data.preguntaConTipos.TiposRespuestas.Add(_data.texto);
            context.Preguntas.Add(_data.preguntaConTipos);

            _data.preguntaConTipos.TiposRespuestas.Add(_data.fecha);
            _data.preguntaConTipos.TiposRespuestas.Add(_data.numero);
            context.Preguntas.Add(_data.preguntaConTipos2);

            context.Etiquetas.Add(_data.etiqueta);

            context.SaveChanges();

            _data.cuestionario.Experto = _data.experto;
            _data.cuestionario.Generico = _data.generico;
            context.Cuestionarios.Update(_data.cuestionario);

            _data.cuestionarioNoActivo.Experto = _data.experto;
            _data.cuestionarioNoActivo.Generico = _data.generico;
            context.Cuestionarios.Update(_data.cuestionarioNoActivo);

            _data.cuestionario2.Experto = _data.experto2;
            _data.cuestionario2.Generico = _data.generico;
            context.Cuestionarios.Update(_data.cuestionario2);

            context.SaveChanges();
            return context;
        }

        public DataModel GetContextBloquesWithData()
        {
            var options = new DbContextOptionsBuilder<DataModel>()
                      .UseInMemoryDatabase(Guid.NewGuid().ToString())
                      .Options;
            var context = new DataModel(options);

            context.Cuestionarios.Add(_data.cuestionario);
            
            context.Modelos.Add(_data.modelo0);
            context.Modelos.Add(_data.modelo1);

            context.Bloques.Add(_data.bloque1);
            context.Bloques.Add(_data.bloque2);
            
            context.SaveChanges();

            return context;
        }
    }
}
