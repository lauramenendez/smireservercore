﻿using Microsoft.EntityFrameworkCore;
using serv_smire.Models.Usuario;
using serv_smire.Services.Impl;
using System;
using System.Collections.Generic;
using System.Text;
using tests.Repositories.Help;
using Xunit;

namespace tests.Repositories
{
    public class TestGenericoRepository
    {
        private FakeDbContext _ctx;
        private Data _data;

        public TestGenericoRepository()
        {
            _data = new Data();
            _ctx = new FakeDbContext(_data);
        }

        [Fact]
        public void TestGenericoRepo_GetGenericos()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                ICollection<Generico> todos = repository.GetGenericos();

                Assert.Equal(2, todos.Count);
            }
        }

        [Fact]
        public void TestGenericoRepo_GetUsername_Exists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                Generico result = repository.GetGenerico(_data.generico.Usuario);

                Assert.NotNull(result);
                Assert.Equal(_data.generico.Usuario, result.Usuario);
            }
        }

        [Fact]
        public void TestGenericoRepo_GetUsername_NotExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                Assert.Null(repository.GetGenerico("prueba"));
            }
        }

        [Fact]
        public void TestGenericoRepo_GetUsername_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                Assert.Null(repository.GetGenerico(null));
            }
        }

        [Fact]
        public void TestGenericoRepo_GetId_Exists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                Generico result = repository.GetGenerico(_data.generico.ID);

                Assert.NotNull(result);
                Assert.Equal(_data.generico.Usuario, result.Usuario);
            }
        }

        [Fact]
        public void TestGenericoRepo_GetId_NotExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                Assert.Null(repository.GetGenerico(0));
            }
        }

        [Fact]
        public void TestGenericoRepo_Add_SameUsername()
        {
            Generico aux = new Generico
            {
                ID = 10,
                Usuario = _data.generico.Usuario,
                Contrasena = _data.generico.Contrasena,
                Experto_ID = _data.experto.ID
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                //Action act = () => repository.Add(aux);
                //Assert.Throws<DbUpdateException>(act);
            }
        }

        [Fact]
        public void TestGenericoRepo_Add_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                Action act = () => repository.Add(null);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestGenericoRepo_Add_TooLargePass()
        {
            Generico aux = new Generico
            {
                ID = 20,
                Usuario = "meh",
                Contrasena = "123456789123456789123456789"
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                Action act = () => repository.Add(aux);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestGenericoRepo_Add_ExactLargePass()
        {
            Generico aux = new Generico
            {
                ID = 30,
                Usuario = "meh",
                Contrasena = "12345678912345678912"
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                repository.Add(aux);

                ICollection<Generico> todos = repository.GetGenericos();
                Assert.Equal(3, todos.Count);

                context.Entry(aux).State = EntityState.Detached;
                repository.Delete(aux.ID);
            }
        }

        [Fact]
        public void TestGenericoRepo_Add_TooShortPass()
        {
            Generico aux = new Generico
            {
                ID = 40,
                Usuario = "meh",
                Contrasena = "123",
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                Action act = () => repository.Add(aux);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestGenericoRepo_Delete_NotExist()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                Action act = () => repository.Delete(0);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestGenericoRepo_Update_SameUsername()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                //_data.generico.Usuario = _data.generico2.Usuario;

                //Action act = () => repository.Update(_data.generico);
                //Assert.Throws<DbUpdateException>(act);
            }
        }

        [Fact]
        public void TestGenericoRepo_Update_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                Action act = () => repository.Update(null);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestGenericoRepo_Update_TooLargePass()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                _data.generico.Contrasena = "123456789123456789123456789";

                Action act = () => repository.Update(_data.generico);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestGenericoRepo_Update_TooShortPass()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                _data.generico.Contrasena = "123";

                Action act = () => repository.Update(_data.generico);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestGenericoRepo_CheckUsername_True()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                Assert.True(repository.CheckExistUsername(_data.generico.Usuario));
            }
        }

        [Fact]
        public void TestGenericoRepo_CheckUsername_False()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                Assert.False(repository.CheckExistUsername("sdfsdfs"));
            }
        }

        [Fact]
        public void TestGenericoRepo_CheckUsername_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new GenericoRepository(context))
            {
                Assert.False(repository.CheckExistUsername(null));
            }
        }
    }
}
