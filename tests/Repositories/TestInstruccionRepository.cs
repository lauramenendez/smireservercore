﻿using serv_smire.Models.Cuestionarios;
using serv_smire.Services.Impl;
using System;
using System.Collections.Generic;
using System.Text;
using tests.Repositories.Help;
using Xunit;

namespace tests.Repositories
{
    public class TestInstruccionRepository
    {
        private FakeDbContext _ctx;
        private Data _data;

        public TestInstruccionRepository()
        {
            _data = new Data();
            _ctx = new FakeDbContext(_data);
        }

        [Fact]
        public void TestInstruccionRepo_Get()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new InstruccionRepository(context))
            {
                Instruccion instruccion = repository.GetInstruccion(_data.instruccion.ID);
                Assert.NotNull(instruccion);
                Assert.Equal(instruccion.Filename, _data.instruccion.Filename);
            }
        }

        [Fact]
        public void TestInstruccionRepo_Get_NoExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new InstruccionRepository(context))
            {
                Instruccion instruccion = repository.GetInstruccion(0);
                Assert.Null(instruccion);
            }
        }

        [Fact]
        public void TestInstruccionRepo_Add_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new InstruccionRepository(context))
            {
                Action act = () => repository.Add(null);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestInstruccionRepo_Add_Vacia()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new InstruccionRepository(context))
            {
                repository.Add(new List<Instruccion> { });
            }
        }

        [Fact]
        public void TestInstruccionRepo_Delete_NoExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new InstruccionRepository(context))
            {
                Action act = () => repository.Delete(0);
                //Assert.Throws<InvalidOperationException>(act);
            }
        }
    }
}
