﻿using Microsoft.EntityFrameworkCore;
using serv_smire.Models.Cuestionarios;
using serv_smire.Services.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tests.Repositories.Help;
using Xunit;

namespace tests.Repositories
{
    public class TestCuestionarioRepository
    {
        private FakeDbContext _ctx;
        private Data _data;

        public TestCuestionarioRepository()
        {
            _data = new Data();
            _ctx = new FakeDbContext(_data);
        }

        [Fact]
        public void TestCuestionarioRepo_Get_NotExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Assert.Null(repository.GetCuestionario(0));
            }
        }

        [Fact]
        public void TestCuestionarioRepo_Get_Exists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                var cuest = repository.GetCuestionario(_data.cuestionario.ID);
                Assert.NotNull(cuest);
                Assert.Equal(_data.cuestionario.Titulo, cuest.Titulo);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_GetUser_NotExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Assert.Equal(0, repository.GetCuestionariosUsuario(0).Count);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_GetUser_Exists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Assert.Equal(2, repository.GetCuestionariosUsuario(_data.experto.ID).Count);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_GetAll()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Assert.Equal(3, repository.GetCuestionarios().Count);
            }
        }

        [Fact]
        public async Task TestCuestionarioRepo_GetAllActive()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                ICollection<Cuestionario> cuestionarios = await repository.GetCuestionariosActivos();
                Assert.Equal(2, cuestionarios.Count);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_GetCuestModelo_NotExistsCuest()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Action act = () => repository.GetCuestionarioModelo(0, _data.modelo0.ID);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_GetCuestModelo_NotExistsModelo()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Cuestionario cuest = repository.GetCuestionarioModelo(_data.cuestionario.ID, 0);

                Assert.NotNull(cuest);
                Assert.NotNull(cuest.ModeloEscogido);
                Assert.Null(cuest.Modelos);

                // Devuelve el 0
                Assert.Equal(_data.modelo0.ID, cuest.ModeloEscogido.ID);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_GetCuestModelo_ExistsCuestModelo()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Cuestionario cuest = repository.GetCuestionarioModelo(_data.cuestionario.ID, _data.modelo1.ID);

                Assert.NotNull(cuest);
                Assert.NotNull(cuest.ModeloEscogido);
                Assert.Null(cuest.Modelos);

                // Devuelve el pedido
                Assert.Equal(_data.modelo1.ID, cuest.ModeloEscogido.ID);
            }
        }

        [Fact]
        public async Task TestCuestionarioRepo_GetRndModelo_Exists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Cuestionario cuest = await repository.GetCuestionarioModelo(_data.cuestionario2.ID);

                Assert.NotNull(cuest);
                Assert.NotNull(cuest.ModeloEscogido);
                Assert.Null(cuest.Modelos);

                Assert.Equal(_data.modelo0_2.ID, cuest.ModeloEscogido.ID);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_GetRndModelo_NotExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Assert.ThrowsAsync<InvalidOperationException>(async () => await repository.GetCuestionarioModelo(0));
            }
        }

        [Fact]
        public void TestCuestionarioRepo_Add_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Assert.Null(repository.Add(null));
            }
        }

        [Fact]
        public void TestCuestionarioRepo_Add_SameTitle()
        {
            Cuestionario aux = new Cuestionario
            {
                ID = long.MaxValue,
                Titulo = _data.cuestionario.Titulo,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Action act = () => repository.Add(aux);
                //Assert.Throws<DbUpdateException>(act);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_Add_EmptyTitle()
        {
            Cuestionario aux = new Cuestionario
            {
                ID = long.MaxValue,
                Titulo = null,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Action act = () => repository.Add(aux);
                //Assert.Throws<DbUpdateException>(act);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_Add_TooLargeTitle()
        {
            Cuestionario aux = new Cuestionario
            {
                ID = long.MaxValue,
                Titulo = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Action act = () => repository.Add(aux);
                //Assert.Throws<DbUpdateException>(act);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_Delete_NotExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Action act = () => repository.Delete(0);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_Update_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Action act = () => repository.Update(null);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_Update_NotExists()
        {
            Cuestionario aux = new Cuestionario
            {
                Titulo = "aux",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                Action act = () => repository.Update(aux);
                //Assert.Throws<DbUpdateException>(act);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_Update()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                _data.cuestionario.Titulo = "pruebaActualizada";
                _data.cuestionario.Instrucciones = "instruccionesActualizadas";

                repository.Update(_data.cuestionario);

                Assert.Equal("pruebaActualizada", repository.GetCuestionario(_data.cuestionario.ID).Titulo);
                Assert.Equal("instruccionesActualizadas", repository.GetCuestionario(_data.cuestionario.ID).Instrucciones);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_Update_SameTitle()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                _data.cuestionario.Titulo = _data.cuestionario2.Titulo;

                Action act = () => repository.Update(_data.cuestionario);
                //Assert.Throws<DbUpdateException>(act);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_Update_EmptyTitle()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                _data.cuestionario.Titulo = null;

                Action act = () => repository.Update(_data.cuestionario);
                //Assert.Throws<DbUpdateException>(act);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_Update_TooLargeTitle()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                _data.cuestionario.Titulo = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                    "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

                Action act = () => repository.Update(_data.cuestionario);
                //Assert.Throws<DbUpdateException>(act);
            }
        }

        /*[Fact]
        public void TestCuestionarioRepo_Update_AddBloque()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                context.Bloques.Add(_data.bloque1);
                context.SaveChanges();

                context.Entry(_data.bloque1).State = EntityState.Detached;
                context.SaveChanges();

                repository.Update(_data.cuestionario);

                var bloques = repository.GetCuestionarioModelo(_data.cuestionario.ID, _data.modelo0.ID).ModeloEscogido.Bloques;
                Assert.Contains(_data.bloque1.ID, bloques.Select(b => b.ID));
            }
        }

        [Fact]
        public void TestCuestionarioRepo_Update_RemoveBloque()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                context.Bloques.Add(_data.bloque1);
                context.SaveChanges();

                context.Entry(_data.bloque1).State = EntityState.Detached;
                context.SaveChanges();

                repository.Update(_data.cuestionario);

                var bloques = repository.GetCuestionarioModelo(_data.cuestionario.ID, _data.modelo0.ID).ModeloEscogido.Bloques;
                Assert.Contains(_data.bloque1.ID, bloques.Select(b => b.ID));

                _data.bloque1.Deleted = true;
                context.Bloques.Update(_data.bloque1);
                context.SaveChanges();

                repository.Update(_data.cuestionario);

                Assert.Empty(repository.GetCuestionarioModelo(_data.cuestionario.ID, _data.modelo0.ID).ModeloEscogido.Bloques);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_Update_UpdateBloque()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                context.Bloques.Add(_data.bloque1);
                context.SaveChanges();

                repository.Update(_data.cuestionario);

                var bloques = repository.GetCuestionarioModelo(_data.cuestionario.ID, _data.modelo0.ID).ModeloEscogido.Bloques;
                Assert.Contains(_data.bloque1.ID, bloques.Select(b => b.ID));

                _data.bloque1.Nombre = _data.bloque1.Nombre + " actualizado";
                context.Bloques.Update(_data.bloque1);
                context.SaveChanges();

                repository.Update(_data.cuestionario);

                bloques = repository.GetCuestionarioModelo(_data.cuestionario.ID, _data.modelo0.ID).ModeloEscogido.Bloques;
                Assert.Contains(_data.bloque1.ID, bloques.Select(b => b.ID));
                Assert.Equal(_data.bloque1.Nombre, bloques.FirstOrDefault(b => b.ID == _data.bloque1.ID).Nombre);
            }
        }

        [Fact]
        public void TestCuestionarioRepo_Update_Posiciones_NotExistCuest()
        {
            using (var context = _ctx.GetContextBloquesWithData())
            using (var repository = new CuestionarioRepository(context, null))
            {
                context.Entry(_data.bloque1).State = EntityState.Detached;
                context.Entry(_data.bloque2).State = EntityState.Detached;
                context.SaveChanges();

                repository.UpdatePosiciones(_data.cuestionario.ID, _data.bloque1.ID, _data.bloque2.ID);

                var bloques = repository.GetCuestionarioModelo(_data.cuestionario.ID, _data.modelo0.ID).ModeloEscogido.Bloques;
                Assert.Contains(_data.bloque1.ID, bloques.Select(b => b.ID));
                Assert.Contains(_data.bloque2.ID, bloques.Select(b => b.ID));

                Assert.Equal(_data.bloque2.Posicion, bloques.FirstOrDefault(b => b.ID == _data.bloque1.ID).Posicion);
                Assert.Equal(_data.bloque1.Posicion, bloques.FirstOrDefault(b => b.ID == _data.bloque2.ID).Posicion);
            }
        }*/
    }
}
