﻿using Microsoft.EntityFrameworkCore;
using serv_smire.Models.Usuario;
using serv_smire.Services.Impl;
using System;
using System.Collections.Generic;
using System.Text;
using tests.Repositories.Help;
using Xunit;

namespace tests.Repositories
{
    public class TestExpertoRepository
    {
        private FakeDbContext _ctx;
        private Data _data;

        public TestExpertoRepository()
        {
            _data = new Data();
            _ctx = new FakeDbContext(_data);
        }

        [Fact]
        public void TestExpertoRepo_Get()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                ICollection<Experto> todos = repository.GetExpertos();
                Assert.Equal(2, todos.Count);
            }
        }

        [Fact]
        public void TestExpertoRepo_GetUsername_Exists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Experto result = repository.GetExperto(_data.experto.Usuario);

                Assert.NotNull(result);
                Assert.Equal(_data.experto.Usuario, result.Usuario);
            }
        }

        [Fact]
        public void TestExpertoRepo_GetUsername_NotExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Assert.Null(repository.GetExperto("prueba"));
            }
        }

        [Fact]
        public void TestExpertoRepo_GetUsername_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Assert.Null(repository.GetExperto(null));
            }
        }

        [Fact]
        public void TestExpertoRepo_GetId_Exists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Experto result = repository.GetExperto(_data.experto.ID);

                Assert.NotNull(result);
                Assert.Equal(_data.experto.Usuario, result.Usuario);
            }
        }

        [Fact]
        public void TestExpertoRepo_GetId_NotExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Assert.Null(repository.GetExperto(0));
            }
        }

        [Fact]
        public void TestExpertoRepo_GetIdUsername()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Assert.NotNull(repository.GetExperto(_data.experto.Usuario, _data.experto.ID));
            }
        }

        [Fact]
        public void TestExpertoRepo_GetIdUsername_NotExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Assert.Null(repository.GetExperto(_data.experto.Usuario, 0));
            }
        }

        [Fact]
        public void TestExpertoRepo_GetIdUsername_NotExists2()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Assert.Null(repository.GetExperto("afdafda", _data.experto.ID));
            }
        }

        [Fact]
        public void TestExpertoRepo_GetIdUsername_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Assert.Null(repository.GetExperto(null, _data.experto.ID));
            }
        }

        [Fact]
        public void TestExpertoRepo_Add_SameUsername()
        {
            Experto aux = new Experto
            {
                ID = long.MaxValue,
                Usuario = _data.experto.Usuario,
                Nombre = "SameUsername",
                Apellidos = "SameUsername",
                Email = "same@username.com",
                EsAdmin = false,
                Aceptado = false,
                Contrasena = "SameUsername123"
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Action act = () => repository.Add(aux);
                //Assert.Throws<DbUpdateException>(act);
            }
        }

        [Fact]
        public void TestExpertoRepo_Add_Admin()
        {
            Experto aux = new Experto
            {
                Usuario = "isadmin",
                Nombre = "IsAdmin",
                Apellidos = "IsAdmin",
                Email = "is@admin.com",
                EsAdmin = true,
                Aceptado = true,
                Contrasena = "isadmin123"
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Action act = () => repository.Add(aux);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestExpertoRepo_Add_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Action act = () => repository.Add(null);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestExpertoRepo_Add_TooLargePass()
        {
            Experto aux = new Experto
            {
                Usuario = "toolarge",
                Nombre = "TooLarge",
                Apellidos = "TooLarge",
                Email = "too@large.com",
                EsAdmin = false,
                Aceptado = false,
                Contrasena = "123456789123456789123456789"
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Action act = () => repository.Add(null);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestExpertoRepo_Add_TooShortPass()
        {
            Experto aux = new Experto
            {
                Usuario = "tooshort",
                Nombre = "TooShort",
                Apellidos = "TooShort",
                Email = "too@short.com",
                EsAdmin = false,
                Aceptado = false,
                Contrasena = "123"
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Action act = () => repository.Add(null);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestExpertoRepo_Delete_Admin()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Action act = () => repository.Delete(_data.admin.ID);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestExpertoRepo_Delete_NotExist()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Action act = () => repository.Delete(0);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestExpertoRepo_Update_SameUsername()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                _data.experto.Usuario = _data.experto2.Usuario;

                Action act = () => repository.Update(_data.experto);
                //Assert.Throws<DbUpdateException>(act);
            }
        }

        [Fact]
        public void TestExpertoRepo_Update_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                _data.experto.Usuario = _data.experto2.Usuario;

                Action act = () => repository.Update(null);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestExpertoRepo_UpdateCon_TooLargePass()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                _data.experto.Contrasena = _data.experto.Contrasena;
                _data.experto.NuevaContrasena = "123456789123456789123456789";
                _data.experto.RepContrasena = "123456789123456789123456789";

                Action act = () => repository.Update(_data.experto);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestExpertoRepo_UpdateCon_TooShortPass()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                _data.experto.Contrasena = _data.experto.Contrasena;
                _data.experto.NuevaContrasena = "123";
                _data.experto.RepContrasena = "123";

                Action act = () => repository.Update(_data.experto);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestExpertoRepo_UpdateCon_NoCoinciden()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                _data.experto.Contrasena = _data.experto.Contrasena;
                _data.experto.NuevaContrasena = "nueva1234";
                _data.experto.RepContrasena = "nueva12345";

                Action act = () => repository.Update(_data.experto);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestExpertoRepo_UpdateCon_NoCorrecta()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                _data.experto.Contrasena = _data.experto.Contrasena + "1";
                _data.experto.NuevaContrasena = "nueva12345";
                _data.experto.RepContrasena = "nueva12345";

                Action act = () => repository.Update(_data.experto);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestExpertoRepo_CheckUsername_True()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Assert.True(repository.CheckExistUsername(_data.experto.Usuario));
            }
        }

        [Fact]
        public void TestExpertoRepo_CheckUsername_False()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Assert.False(repository.CheckExistUsername("sdfsdfs"));
            }
        }

        [Fact]
        public void TestExpertoRepo_CheckUsername_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ExpertoRepository(context))
            {
                Assert.False(repository.CheckExistUsername(null));
            }
        }
    }
}
