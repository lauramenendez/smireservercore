﻿using Microsoft.EntityFrameworkCore;
using serv_smire.Models.Cuestionarios;
using serv_smire.Services.Impl;
using System;
using System.Collections.Generic;
using System.Text;
using tests.Repositories.Help;
using Xunit;

namespace tests.Repositories
{
    public class TestModeloRepository
    {
        private FakeDbContext _ctx;
        private Data _data;

        public TestModeloRepository()
        {
            _data = new Data();
            _ctx = new FakeDbContext(_data);
        }

        [Fact]
        public void TestModeloRepo_Get_Exists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ModeloRepository(context))
            {
                Modelo modelo = repository.GetModelo(_data.modelo0.ID);

                Assert.NotNull(modelo);
                Assert.Equal(modelo.Nombre, _data.modelo0.Nombre);
            }
        }

        [Fact]
        public void TestModeloRepo_Get_NotExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ModeloRepository(context))
            {
                Assert.Null(repository.GetModelo(0));
            }
        }

        [Fact]
        public void TestModeloRepo_GetModelos_Exists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ModeloRepository(context))
            {
                Assert.Equal(2, repository.GetModelos(_data.cuestionario.Titulo).Count);
            }
        }

        [Fact]
        public void TestModeloRepo_GetModelos_NotExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ModeloRepository(context))
            {
                Assert.Equal(0, repository.GetModelos("noExiste").Count);
            }
        }

        [Fact]
        public void TestModeloRepo_Add_Null()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ModeloRepository(context))
            {
                Action act = () => repository.Add(null);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestModeloRepo_Add_SinOriginal()
        {
            Modelo aux = new Modelo
            {
                Nombre = "Modelo 0",
                Cuestionario_ID = _data.cuestionario2.ID
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new ModeloRepository(context))
            {
                Action act = () => repository.Add(aux);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestModeloRepo_Add_Delete()
        {
            Modelo aux2 = new Modelo
            {
                ID = 20,
                Orden_Bloques = true,
                Orden_Preguntas = false,
                Orden_Tipos = false,
                Cuestionario_ID = _data.cuestionario2.ID
            };

            Modelo aux3 = new Modelo
            {
                ID = 30,
                Orden_Bloques = false,
                Orden_Preguntas = true,
                Orden_Tipos = false,
                Cuestionario_ID = _data.cuestionario2.ID
            };

            using (var context = _ctx.GetContextWithData())
            using (var repository = new ModeloRepository(context))
            {
                // Agregar modelo 
                var modelo2 = repository.Add(aux2);

                context.Entry(modelo2).State = EntityState.Detached;
                context.Entry(aux2).State = EntityState.Detached;

                Assert.Equal("Modelo 1", modelo2.Nombre);
                Assert.Equal(2, repository.GetModelos(_data.cuestionario2.Titulo).Count);

                // Borrar modelo 
                repository.Delete(modelo2.ID);
                Assert.Equal(1, repository.GetModelos(_data.cuestionario2.Titulo).Count);

                // Agregar otro modelo
                var modelo3 = repository.Add(aux3);

                context.Entry(modelo3).State = EntityState.Detached;
                context.Entry(aux3).State = EntityState.Detached;

                Assert.Equal("Modelo 1", modelo3.Nombre);
                Assert.Equal(2, repository.GetModelos(_data.cuestionario2.Titulo).Count);
                
                // Borrar modelo
                repository.Delete(modelo3.ID);
                Assert.Equal(1, repository.GetModelos(_data.cuestionario2.Titulo).Count);
            }
        }

        [Fact]
        public void TestModeloRepo_Delete_NotExists()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ModeloRepository(context))
            {
                Action act = () => repository.Delete(0);
                Assert.Throws<InvalidOperationException>(act);
            }
        }

        [Fact]
        public void TestModeloRepo_Delete_Modelo0()
        {
            using (var context = _ctx.GetContextWithData())
            using (var repository = new ModeloRepository(context))
            {
                Action act = () => repository.Delete(_data.modelo0.ID);
                Assert.Throws<InvalidOperationException>(act);
            }
        }
    }
}
