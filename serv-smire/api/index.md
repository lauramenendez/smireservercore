# Herramienta web para diseñar cuestionarios

Aplicación de la parte servidor en *.NET Core 2* para la gestión y respuesta de cuestionarios que emplean **Fuzzy Rating Scales**.
A continuación se mencionan sus roles principales junto con sus funciones:

### Usuarios privilegiados

Los usuarios privilegiados están formados por el administrador y los usuarios expertos.
* El administrador gestiona estos últimos, aceptándolos y dándolos de baja del sistema si es necesario.
* Los usuarios expertos son los capacitados para crear y gestionar cuestionarios, así como usuarios genéricos dentro del mismo.

### Usuarios genéricos

Los usuarios genéricos solamente pueden responder cuestionarios que les han sido asignados. Al iniciar sesión, se les asignará un identificador alfanumérico con el que podrán seguir respondiendo el cuestionario si así lo desean.

## Estructura

### _site

Carpeta que contiene una página web generada a partir de todos los comentarios en el código de la aplicación utilizando la herramienta [DocFX](https://dotnet.github.io/docfx/).

### Controllers

Controladores que gestionan las peticiones al *servicio REST* realizadas por el cliente.

### Helper

Clases de utilidad.

### Models

Clases que representan los objetos del modelo de dominio.

### Services

Servicios que se comunican con la base de datos para recuperar o actualizar información de la misma.

### wwwroot

Archivos *HTML, JavaScript y CSS* generados a partir de la aplicación en *Angular*.

## Despliegue

El proyecto completo se encuentra desplegado en [Zadeh](http://zadeh.ciencias.uniovi.es).
