﻿using Newtonsoft.Json.Linq;
using System;
using serv_smire.Helper.Factory;
using serv_smire.Models.Cuestionarios.TiposRespuestas;

namespace serv_smire.Helper.JSON
{
    /// <summary>
    /// Clase necesaria para parsear los tipos de respuesta en JSON a los objetos correspondientes
    /// </summary>
    public class JsonTipoRespuesta : JsonCreationConverter<ITipoRespuesta>
    {
        protected override ITipoRespuesta Create(Type objectType, JObject jObject)
        {
            return FactoryTipoRespuesta.CreateTipo(jObject.Value<string>("NombreTipo"));
        }
    }
}