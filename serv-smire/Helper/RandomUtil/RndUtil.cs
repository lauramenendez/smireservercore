﻿using System;
using System.Linq;
using System.Threading;

namespace serv_smire.Helper.RandomUtil
{
    /// <summary>
    /// Clase utilizada para hacer uso de un Random thread safe
    /// </summary>
    public static class RndUtil
    {
        private const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        private static Random _global = new Random();
        private static ThreadLocal<Random> _local = new ThreadLocal<Random>(() =>
        {
            int seed;
            lock (_global) seed = _global.Next();
            return new Random(seed);
        });

        public static int Next()
        {
            return _local.Value.Next();
        }

        public static int Next(int length)
        {
            return _local.Value.Next(length);
        }

        /// <summary>
        /// Este método devuelve posiciones aleatorias (no repetidas) dentro de un rango
        /// cuyo límite es el segundo parámetro
        /// </summary>
        /// <param name="rnd"></param>
        /// <param name="numElementos">Número máximo a considerar</param>
        /// <returns></returns>
        public static int[] PosicionesAleatorias(this Random rnd, int numElementos)
        {
            return Enumerable.Range(1, numElementos).OrderBy(x => rnd.Next()).ToArray();
        }

        /// <summary>
        /// Devuelve un código alfanumérico de la longitud que se pase como parámetro
        /// </summary>
        /// <param name="length">Longitud del código</param>
        /// <returns></returns>
        public static string GetAlphanumericString(int length)
        {
            return new string(Enumerable.Repeat(chars, length).Select(s => s[Next(s.Length)]).ToArray());
        }
    }
}