﻿using System;
using serv_smire.Helper.Enumerators;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Helper.Security;

namespace serv_smire.Helper.Validate
{
    /// <summary>
    /// Clase utilizada para validar las contraseñas, sobre todo por el controlador de inicio de sesión (login)
    /// </summary>
    public static class ValidateContrasena
    {
        /// <summary>
        /// Comprobamos que está entre los tamaños requeridos
        /// </summary>
        /// <param name="contrasena"></param>
        /// <returns></returns>
        public static bool Tamaño(string contrasena)
        {
            if (contrasena.Length > EValidaciones.MaxContrasena || contrasena.Length < EValidaciones.MinContrasena)
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidSizePassword).ToString());
            }
            return true;
        }

        /// <summary>
        /// Comprobamos que ambas contraseñas coinciden, la real con la enviada
        /// </summary>
        /// <param name="contrasena1"></param>
        /// <param name="contrasena2"></param>
        /// <returns></returns>
        public static bool Antigua(string contrasena1, string contrasena2)
        {
            if (!contrasena1.Equals(contrasena2))
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidPassword).ToString());
            }
            return true;
        }

        /// <summary>
        /// Comprobamos que la nueva contraseña no es igual a la antigua
        /// </summary>
        /// <param name="contrasena1"></param>
        /// <param name="contrasena2"></param>
        /// <returns></returns>
        public static bool Distinta(string contrasena1, string contrasena2)
        {
            if (contrasena1.Equals(contrasena2))
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.SamePassword).ToString());
            }
            return true;
        }

        /// <summary>
        /// Comprobamos que ambas contraseñas coinciden (nueva y nueva repetida)
        /// </summary>
        /// <param name="contrasena1"></param>
        /// <param name="contrasena2"></param>
        /// <returns></returns>
        public static bool Coinciden(string contrasena1, string contrasena2)
        {
            if (!contrasena1.Equals(contrasena2))
            { 
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.PasswordsNotMatch).ToString());
            }
            return true;
        }

        /// <summary>
        /// Devuelve la contrasena encriptada para poder compararla,
        /// ya que en el caso de los usuarios expertos no se puede desencriptar
        /// </summary>
        /// <param name="pass"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public static string GetContrasenaEnviada(string pass, string salt)
        {
            var hashCode = salt;
            return Encrypt.EncodePassword(pass, hashCode);
        }
    }
}