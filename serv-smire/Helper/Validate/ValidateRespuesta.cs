﻿using System;
using System.Linq;
using serv_smire.Helper.Enumerators;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Models.Cuestionarios.TiposRespuestas;

namespace serv_smire.Helper.Validate
{
    /// <summary>
    /// Clase que se encarga de validar los tipos de respuesta y que
    /// la respuesta dada cumple con el formato esperado
    /// </summary>
    public static class ValidateRespuesta
    {
        /// <summary>
        /// Comprueba que el tipo de respuesta no es null
        /// y luego que el formato de la respuesta es válido
        /// </summary>
        /// <param name="tr"></param>
        /// <param name="respuesta"></param>
        public static void Validate(TipoRespuesta tr, string respuesta)
        {
            if (CheckNull(tr) || !tr.CheckFormato(respuesta))
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidRespuesta).ToString());
            }
        }

        private static bool CheckNull(TipoRespuesta tr)
        {
            return tr == null;
        }
    }
}