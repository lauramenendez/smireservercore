﻿using System;
using System.Linq;
using System.Threading.Tasks;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Helper.RandomUtil;
using serv_smire.Helper.Security;
using serv_smire.Models.Cuestionarios;
using serv_smire.Models.Usuario;
using serv_smire.Services;

namespace serv_smire.Helper.Validate
{
    /// <summary>
    /// Clase utilizada para validar los datos de los usuarios enviados durante el inicio de sesión
    /// </summary>
    public class ValidateUser
    {
        private const int length = 8;
        private JwtManager manager;

        public ValidateUser(JwtManager _manager)
        {
            manager = _manager;
        }

        /// <summary>
        /// Metodo que valida el usuario experto que intenta logearse:
        /// <list type="bullet">
        ///     <item>Que no sea null</item>
        ///     <item>Que la contraseña sea correcta</item>
        ///     <item>Que este aceptado por el administrador</item>
        /// </list>
        /// </summary>
        /// <param name="enviado">Usuario que envia sus datos</param>
        /// <param name="repo">Repositorio de usuarios</param>
        /// <returns></returns>
        public Experto Validate(Experto enviado, IExpertoRepository repo)
        {
            Experto real = repo.GetExperto(enviado.Usuario);

            if (CheckUsuario(real))
            {
                var enviada = ValidateContrasena.GetContrasenaEnviada(enviado.Contrasena, real.Codigo);

                if (CheckContrasena(enviada, real.Contrasena) && CheckAceptado(real.Aceptado)) {
                    real.Contrasena = null;
                    real.Codigo = null;
                    real.Token = manager.GenerateToken(real.Usuario, real.GetRole());
                    return real;
                }
            }
            
            return null;
        }

        /// <summary>
        /// Metodo que valida el usuario generico que intenta logearse:
        /// <list type="bullet">
        ///     <item>Que no sea null</item>
        ///     <item>Que la contrasena sea correcta</item>
        ///     <item>Que el usuario tiene asignado ese cuestionario</item>
        /// </list>
        /// </summary>
        /// <param name="enviado"></param>
        /// <param name="genRepo"></param>
        /// <param name="cuestRepo"></param>
        /// <returns></returns>
        public async Task<Generico> Validate(Generico enviado, IGenericoRepository genRepo, ICuestionarioRepository cuestRepo)
        {
            Generico real = genRepo.GetGenerico(enviado.Usuario);
            
            if (CheckUsuario(real))
            {
                var cEnviada = enviado.Contrasena;
                var cReal = Encrypt.base64Decode(real.Contrasena);

                if (CheckContrasena(cEnviada, cReal) && CheckCuestionario(real, enviado.CuestionarioEscogidoID))
                {
                    if (string.IsNullOrEmpty(enviado.Identificador))
                    {
                        real.Identificador = GenerateIdentificador();
                        real.CuestionarioEscogido = await cuestRepo.GetCuestionarioModelo(enviado.CuestionarioEscogidoID);
                    }
                    else
                    {
                        CheckIdentificador(enviado.Identificador, genRepo);

                        real.Identificador = enviado.Identificador;
                        real.CuestionarioEscogido = await cuestRepo.GetCuestionarioModelo(enviado.CuestionarioEscogidoID, enviado.Identificador);
                    }

                    CheckNotNull(real.CuestionarioEscogido);
                    CheckCuestionarioActivo(real.CuestionarioEscogido.Activo);

                    real.Token = manager.GenerateToken(real.Usuario, real.GetRole());
                    real.Cuestionarios = null;
                    real.Contrasena = null;
                    return real;
                }
            }
            
            return null;
        }

        /// <summary>
        /// Genera un identificador alfanumerico para el usuario generico que se ha logeado
        /// </summary>
        /// <returns></returns>
        private string GenerateIdentificador()
        {
            return RndUtil.GetAlphanumericString(length);
        }

        /// <summary>
        /// Comprueba que el identificador existe si se envia
        /// </summary>
        /// <param name="identificador"></param>
        /// <returns></returns>
        private bool CheckIdentificador(string identificador, IGenericoRepository genRepo)
        {
            if (!genRepo.CheckIdentificador(identificador))
            {
                throw new UnauthorizedAccessException(ErrorMsg.getMsg(eError.NoExisteIdent).ToString());
            }
            return true;
        }

        /// <summary>
        /// Comprueba que ambas contrasenas sean iguales
        /// </summary>
        /// <param name="enviada"></param>
        /// <param name="real"></param>
        /// <returns></returns>
        private bool CheckContrasena(string enviada, string real)
        {
            if (!enviada.Equals(real))
            {
                throw new UnauthorizedAccessException(ErrorMsg.getMsg(eError.LoginFail).ToString());
            }
            return true;
        }

        /// <summary>
        /// Comprueba que el usuario no es null
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private bool CheckUsuario(IUsuario user)
        {
            if (user == null)
            {
                throw new UnauthorizedAccessException(ErrorMsg.getMsg(eError.LoginFail).ToString());
            }
            return true ;
        }

        /// <summary>
        /// Comprueba que el cuestionario no es null
        /// </summary>
        /// <param name="cuest"></param>
        /// <returns></returns>
        private bool CheckNotNull(Cuestionario cuest)
        {
            if (cuest == null)
            {
                throw new UnauthorizedAccessException(ErrorMsg.getMsg(eError.NoExistCuestionario).ToString());
            }
            return true;
        }

        /// <summary>
        /// Comprueba que el usuario tiene asignado ese cuestionario
        /// </summary>
        /// <param name="real"></param>
        /// <param name="cuestionario"></param>
        /// <returns></returns>
        private bool CheckCuestionario(Generico real, long cuestionario)
        {
            if (!real.Cuestionarios.Where(c => c.ID == cuestionario && c.Activo).Any())
            {
                throw new UnauthorizedAccessException(ErrorMsg.getMsg(eError.IncorrectCuestionario).ToString());
            }
            return true;
        }

        /// <summary>
        /// Comprueba que el usuario este aceptado por el administrador
        /// </summary>
        /// <param name="aceptado"></param>
        /// <returns></returns>
        private bool CheckAceptado(bool aceptado)
        {
            if (!aceptado)
            {
                throw new UnauthorizedAccessException(ErrorMsg.getMsg(eError.UnaceptedExperto).ToString());
            }
            return true;
        }

        /// <summary>
        /// Comprueba que el cuestionario este activo
        /// </summary>
        /// <param name="activo"></param>
        /// <returns></returns>
        private bool CheckCuestionarioActivo(bool activo)
        {
            if (!activo)
            {
                throw new UnauthorizedAccessException(ErrorMsg.getMsg(eError.UnactivatedCuestionario).ToString());
            }
            return true;
        }
    }
}