﻿using log4net;
using System;
using System.Security.Cryptography;
using System.Text;

namespace serv_smire.Helper.Security
{
    /// <summary>
    /// Clase encargada de encriptar las contraseñas tanto de usuarios expertos como genéricos
    /// </summary>
    public static class Encrypt
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // USUARIOS EXPERTOS

        /// <summary>
        /// Genera la sal para la contraseña
        /// </summary>
        /// <param name="length">Tamaño de la sal</param>
        /// <returns></returns>
        public static string GenerateSalt(int length)
        {
            const string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            var rnd = new Random();
            var chars = new char[length];

            for (var i = 0; i < length; i++)
            {
                chars[i] = GetRandomCharacter(allowedChars, rnd);
            }
            return new string(chars);
        }

        private static char GetRandomCharacter(string text, Random rnd)
        {
            int index = rnd.Next(text.Length);
            return text[index];
        }

        /// <summary>
        /// Encripta la contraseña + la sal con SHA 256
        /// </summary>
        /// <param name="pass">Contraseña a encriptar</param>
        /// <param name="salt">Sal</param>
        /// <returns>Contraseña encriptada</returns>
        public static string EncodePassword(string pass, string salt) 
        {
            byte[] bytes = Encoding.Unicode.GetBytes(pass);
            byte[] src = Encoding.Unicode.GetBytes(salt);
            byte[] dst = new byte[src.Length + bytes.Length];

            Buffer.BlockCopy(src, 0, dst, 0, src.Length);
            Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);

            HashAlgorithm algorithm = SHA256Managed.Create();
            byte[] inArray = algorithm.ComputeHash(dst);

            return Convert.ToBase64String(inArray);
        }

        // USUARIOS GENERICOS

        /// <summary>
        /// Codificar un string
        /// </summary>
        /// <param name="sData"></param>
        /// <returns></returns>
        public static string base64Encode(string sData)     
        {
            string encodedData = "";
            try
            {
                byte[] encData_byte = new byte[sData.Length];
                encData_byte = Encoding.UTF8.GetBytes(sData);
                encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception ex)
            {
                Log.Error("base64Encode: " + ex.Message);
            }

            return encodedData;
        }

        /// <summary>
        /// Decodificar un string
        /// </summary>
        /// <param name="sData"></param>
        /// <returns></returns>
        public static string base64Decode(string sData) //Decode    
        {
            string result = "";

            try
            {
                var encoder = new UTF8Encoding();
                Decoder utf8Decode = encoder.GetDecoder();
                byte[] todecodeByte = Convert.FromBase64String(sData);
                int charCount = utf8Decode.GetCharCount(todecodeByte, 0, todecodeByte.Length);
                char[] decodedChar = new char[charCount];

                utf8Decode.GetChars(todecodeByte, 0, todecodeByte.Length, decodedChar, 0);

                result = new String(decodedChar);
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("base64Encode: " + ex.Message);
            }

            return result;
        }
    }
}