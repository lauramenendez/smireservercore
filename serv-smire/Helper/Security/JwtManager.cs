﻿using System;
using log4net;
using System.Security.Cryptography;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace serv_smire.Helper.Security
{
    /// <summary>
    /// Clase encargada de gestionar los tokens JWT para mantener las sesiones
    /// </summary>
    public class JwtManager
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const int Minutes = 120;
        private IConfiguration _config;

        public JwtManager(IConfiguration config)
        {
            _config = config;
        }

        /// <summary>
        /// Genera un token JWT a partir del nombre de usuario y el rol
        /// </summary>
        /// <param name="username">Nombre del usuario</param>
        /// <param name="role">Rol del usuario en el sistema</param>
        /// <param name="expireMinutes">Duración de la sesión, por defecto 120 minutos</param>
        /// <returns></returns>
        public string GenerateToken(string username, string role, int expireMinutes = Minutes)
        {
            string keyOriginal = "ClaveFirmaProvisional";
            string issuer = "";
            string audience = "";

            if(_config != null)
            {
                keyOriginal = _config["Jwt:Key"];
                issuer = _config["Jwt:Issuer"];
                audience = _config["Jwt:Audience"];
            }

            var claims = new[] {
                new Claim("user", username),
                new Claim("rol", role)
            };
            ClaimsIdentity claimsId = new ClaimsIdentity(claims, JwtBearerDefaults.AuthenticationScheme);

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(keyOriginal));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer,
                audience,
                claimsId.Claims,
                notBefore: DateTime.Now,
                expires: DateTime.Now.AddMinutes(expireMinutes),
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}