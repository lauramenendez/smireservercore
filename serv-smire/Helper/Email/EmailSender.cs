﻿using System;
using System.Net;
using System.Net.Mail;
using serv_smire.Models.Usuario;
using log4net;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace serv_smire.Helper.Email
{
    /// <summary>
    /// Clase encargada de enviar los correos al administrador cuando se dé de alta
    /// un nuevo usuario experto en el sistema
    /// </summary>
    public class EmailSender
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string fromName = "SMIRE Research Group";
        private const string subject = "Nuevo usuario experto";

        private const string host = "smtp.gmail.com";
        private const int port = 587;

        // Lo envio desde el correo de SMIRE que cree de prueba
        private static string fromEmail;
        private static string fromPassword; // Contraseña verificacion dos pasos Google

        /// <summary>
        /// Constructor de EmailSender
        /// </summary>
        /// <param name="config">Para extraer parámetros de configuración</param>
        public EmailSender(IConfiguration config)
        {
            fromEmail = config["Email:EmailAccount"];
            fromPassword = config["Email:EmailPassword"];
        }

        /// <summary>
        /// Envia un mensaje al administrador con los datos del nuevo usuario experto
        /// </summary>
        /// <param name="usuario">Datos del nuevo usuario experto</param>
        /// <param name="correosAdmin">Todos los correos de los administradores</param>
        public void Send(Experto usuario, ICollection<string> correosAdmin)
        {
            if (correosAdmin.Count > 0)
            {
                try
                {
                    var fromAddress = new MailAddress(fromEmail, fromName);

                    var smtp = new SmtpClient
                    {
                        Host = host,
                        Port = port,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                    };

                    var message = new MailMessage
                    {
                        Subject = subject,
                        Body = GenerateBody(usuario),
                        From = fromAddress,
                        BodyEncoding = System.Text.Encoding.UTF8,
                        SubjectEncoding = System.Text.Encoding.Default,
                        IsBodyHtml = true
                    };

                    // Añado todos los correos de administrador
                    foreach (string correo in correosAdmin)
                    {
                        message.To.Add(correo);
                    }

                    smtp.Send(message);
                }
                catch (Exception ex)
                {
                    Log.Error("Error send email: " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Metodo para generar el cuerpo en función de los datos del usuario
        /// </summary>
        /// <param name="usuario">Datos del usuario experto</param>
        /// <returns>Correo en HTML</returns>
        private string GenerateBody(Experto usuario)
        {
            string mensaje = $@"
                <h3>Un nuevo usuario se ha registrado</h3>
                <p>Buenos días,</p>
                <p>Un usuario experto se acaba de registrar en la web de cuestionarios.<br/>
                    Por favor, si los datos mostrados a continuación son correctos acceda a la web para aceptarlo en el sistema.</p>
                <ul>
                    <li><b>Nombre:</b> {usuario.Nombre}</li>
                    <li><b>Apellidos:</b> {usuario.Apellidos}</li>
                    <li><b>Correo:</b> {usuario.Email}</li>
                    <li><b>Departamento:</b> {usuario.Departamento}</li>
                    <li><b>Nombre de usuario:</b> {usuario.Usuario}</li>
                </ul>
                <p>Muchas gracias por su atención,<br/>un saludo</p>";
            return mensaje;
        }
    }
}