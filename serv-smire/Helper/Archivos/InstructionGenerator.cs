﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using serv_smire.Models.Cuestionarios;

namespace serv_smire.Helper.Archivos
{
    /// <summary>
    /// Clase para crear los objetos "Instruccion" a almacenar en la base de datos
    /// a partir de los datos de los ficheros enviados al servidor
    /// </summary>
    public static class InstructionGenerator
    {
        /// <summary>
        /// A partir de los datos enviados en el objeto "files" de tipo IFormFileCollection
        /// se crean los objetos de tipo "Instruccion"
        /// </summary>
        /// <param name="files">Datos de los ficheros</param>
        /// <param name="cuestionario_id">ID del cuestionario al que pertenecen</param>
        /// <returns>Devuelve una lista con todos los objetos "Instruccion"</returns>
        public static async Task<ICollection<Instruccion>> Get(IFormFileCollection files, long cuestionario_id)
        {
            var instrucciones = new List<Instruccion>();

            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    var ms = new MemoryStream();
                    await file.CopyToAsync(ms);

                    var fileBytes = ms.ToArray();
                    
                    instrucciones.Add(new Instruccion(file.FileName, fileBytes, cuestionario_id, file.ContentType));
                }
            }

            return instrucciones;
        }
    }
}