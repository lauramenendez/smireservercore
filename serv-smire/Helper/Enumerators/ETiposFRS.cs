﻿using System;

namespace serv_smire.Helper.Enumerators
{
    /// <summary>
    /// Clase que contiene los tipos posibles para el tipo de pregunta
    /// Fuzzy Rating Scales (FRS)
    /// </summary>
    public static class ETiposFRS
    {
        public static String Trapecio { get { return "trapecio"; } }
        public static String Intervalo { get { return "intervalo"; } }
    }
}