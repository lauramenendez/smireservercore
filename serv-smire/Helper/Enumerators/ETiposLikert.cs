﻿using System;

namespace serv_smire.Helper.Enumerators
{
    /// <summary>
    /// Clase que contiene los tipos posibles para el tipo de pregunta escala Likert
    /// </summary>
    public static class ETiposLikert
    {
        public static String Numero { get { return "numérica"; } }
        public static String Texto { get { return "textual"; } }
    }
}