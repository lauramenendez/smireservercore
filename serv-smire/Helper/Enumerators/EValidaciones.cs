﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace serv_smire.Helper.Enumerators
{
    /// <summary>
    /// Clase que contiene los datos de las validaciones que se realizan en la aplicación
    /// </summary>
    public static class EValidaciones
    {
        public static int MinContrasena { get { return 8; } }
        public static int MaxContrasena { get { return 20; } }
    }
}