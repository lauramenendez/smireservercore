﻿using System;

namespace serv_smire.Helper.Enumerators
{
    /// <summary>
    /// Clase que contiene los roles de la aplicación
    /// </summary>
    public static class ERoles
    {
        public const String Admin = "admin"; 
        public const String Experto = "superuser"; 
        public const String Generico = "user";
        public const String Privilegiado = "privilegiado";
        public const String NoIdentificado = "guest";
    }
}