﻿using System;

namespace serv_smire.Helper.Enumerators
{
    /// <summary>
    /// Clase que contiene todos los tipos de pregunta posible que existen en la aplicación
    /// </summary>
    public static class ETiposRespuesta
    {
        // De otra forma no se pueden usar en un switch-case
        public const String Desplegable = "desplegable";
        public const String Fecha = "fecha";
        public const String FRS = "frs";
        public const String Intervalo = "intervalo"; 
        public const String Likert = "likert"; 
        public const String Multiple = "multiple"; 
        public const String NoAplicable = "no aplicable"; 
        public const String Numero = "numero"; 
        public const String Orden = "orden";
        public const String Texto = "texto"; 
        public const String Unica = "unica"; 
        public const String VAnalogica = "visual analogica"; 
    }
}