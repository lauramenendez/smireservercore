﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace serv_smire.Helper.ErrorHandler
{
    public enum eError {
        Database,
        InvalidModelState,
        ModeloIncorrecto,
        InvalidParam,
        DeleteAdmin,
        RequiredRespuesta,
        DeleteCuestionario,
        UpdateCuestionario,
        DeleteGenerico,
        InvalidPassword,
        InvalidSizePassword,
        PasswordsNotMatch,
        InvalidRespuesta,
        UnaceptedExperto,
        LoginFail,
        UnactivatedCuestionario,
        IncorrectCuestionario,
        NoExistCuestionario,
        UniqueConstraint,
        UniqueIndex,
        Unknown,
        SamePassword,
        NoExisteIdent,
        NotFound
    };
}