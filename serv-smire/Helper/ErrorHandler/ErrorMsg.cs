﻿using System.Collections.Generic;

namespace serv_smire.Helper.ErrorHandler
{
    public static class ErrorMsg
    {
        private static IDictionary<eError, string> dicGeneralErrors = new Dictionary<eError, string>
        {
            { eError.InvalidModelState, "Cuerpo de la petición inválido" },
            { eError.InvalidParam, "Parámetros inválidos" },
            { eError.DeleteAdmin, "No se puede eliminar al administrador" },
            { eError.DeleteCuestionario, "No se han descargado las respuestas al menos una vez" },
            { eError.UpdateCuestionario, "El cuestionario está activo" },
            { eError.DeleteGenerico, "El usuario genérico está asociado al menos un cuestionario" },
            { eError.InvalidPassword, "La contraseña es incorrecta" },
            { eError.InvalidSizePassword, "La contraseña tiene un tamaño incorrecto" },
            { eError.PasswordsNotMatch, "Las contraseñas no coinciden" },
            { eError.InvalidRespuesta, "Formato de respuesta inválido" },
            { eError.UnaceptedExperto, "El usuario no ha sido aceptado todavía" },
            { eError.LoginFail, "Usuario/contraseña incorrecto o no existe" },
            { eError.UnactivatedCuestionario, "El cuestionario no está activado" },
            { eError.IncorrectCuestionario, "No tiene asignado ese cuestionario" },
            { eError.NoExistCuestionario, "No existe un cuestionario con ese título" },
            { eError.NoExisteIdent, "No existe ese identificador" },
            { eError.UniqueIndex, "ID repetido" },
            { eError.Unknown, "Error desconocido" },
            { eError.ModeloIncorrecto, "El nombre del modelo es incorrecto" },
            { eError.RequiredRespuesta, "Se deben responder todas las preguntas obligatorias" },
            { eError.Database, "Ha habido un error con la base de datos" },
            { eError.SamePassword, "La contraseña no puede ser la misma que la anterior" },
            { eError.NotFound, "No encontrado" }
        };

        private static IDictionary<eClass, string> dicCustomErrors = new Dictionary<eClass, string>
        {
            { eClass.Cuestionario, "Ya existe un cuestionario con ese título" },
            { eClass.Usuario, "Ya existe ese nombre de usuario" },
            { eClass.Modelo, "Ya existe ese modelo" },
            { eClass.Pregunta, "Ya existe esa pregunta en el mismo bloque" },
            { eClass.Respuesta, "Ya se ha respondido esa pregunta" }
        };

        public static object getMsg(eError err, eClass? obj = null)
        {
            if (obj != null && err == eError.UniqueConstraint)
            {
                return new { Message = dicCustomErrors[obj.Value] };
            }
                
            return dicGeneralErrors[err];
        }
    }
}