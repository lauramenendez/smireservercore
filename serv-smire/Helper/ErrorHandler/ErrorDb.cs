﻿using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace serv_smire.Helper.ErrorHandler
{
    public static class ErrorDb
    {
        public static eError getErrorCode(SqlException err)
        {
            switch (err.ErrorCode)
            {
                case 2601:
                    return eError.UniqueIndex;
                case 2627:
                    return eError.UniqueConstraint;
                default:
                    return eError.Unknown;
            }
        }

        public static eError getErrorCode(MySqlException err)
        {
            switch (err.ErrorCode)
            {
                case 2601:
                    return eError.UniqueIndex;
                case 1169:
                    return eError.UniqueConstraint;
                default:
                    return eError.Unknown;
            }
        }
    }
}