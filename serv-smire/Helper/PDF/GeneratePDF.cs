﻿using HandlebarsDotNet;
using serv_smire.Models.Cuestionarios.TiposRespuestas;
using serv_smire.Models.Cuestionarios;
using DinkToPdf;
using DinkToPdf.Contracts;
using HtmlAgilityPack;
using System.Text;

namespace serv_smire.Helper.PDF
{
    /// <summary>
    /// Clase encargada de generar el PDF a partir de los datos del cuestionario
    /// </summary>
    public class GeneratePDF
    {
        private readonly IConverter _converter;

        public GeneratePDF(IConverter converter)
        {
            _converter = converter;
        }

        public byte[] FromSource(Cuestionario cuestionario)
        {
            string source =
                @"<div class=""entry"">
                    <h1>{{titulo}}</h1>
                    <div class=""instrucciones"">
                        {{{instrucciones}}}
                    </div>
                    <div class=""bloques"">
                        {{#each bloques}}
                            <h3>{{nombre}}</h3>
                            <div class=""preguntas"">
                                {{#each preguntas}}
                                    <i>{{posicion}} - {{enunciado}}</i>
                                    <div class=""tipos"">
                                        {{#each tipos}}
                                            {{{contenido}}}
                                        {{/each}}
                                    </div>
                                {{/each}}
                            </div>
                        {{/each}}
                    </div>
                </div>";

            // Obtenemos el HTML
            var template = Handlebars.Compile(source);
            var result = template(GetData(cuestionario));

            // Lo pasamos a PDF
            var doc = new HtmlToPdfDocument()
            {
                GlobalSettings = {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                },
                Objects = {
                    new ObjectSettings() {
                        PagesCount = true,
                        HtmlContent = result,
                        WebSettings = { DefaultEncoding = "utf-8" },
                        HeaderSettings = { FontSize = 9, Right = "Pág. [page] de [toPage]", Line = true, Spacing = 2.812 }
                    }
                }
            };
            return _converter.Convert(doc);
        }

        private dynamic GetData(Cuestionario cuestionario)
        {
            var bloquesData = new object[cuestionario.ModeloEscogido.Bloques.Count];
            int i = 0;
            foreach (Bloque bloque in cuestionario.ModeloEscogido.Bloques)
            {
                var preguntasData = new object[bloque.Preguntas.Count];
                int j = 0;
                foreach (Pregunta pregunta in bloque.Preguntas)
                {
                    var tiposData = new object[pregunta.TiposRespuestas.Count];
                    int k = 0;
                    foreach (ITipoRespuesta tipo in pregunta.TiposRespuestas)
                    {
                        tiposData[k] = new { contenido = tipo.GenerateHTML() };
                        k++;
                    }

                    // Extraer el texto que hay dentro de la pregunta
                    HtmlDocument doc = new HtmlDocument();
                    string html = pregunta.Enunciado;
                    doc.LoadHtml(html);

                    var sb = new StringBuilder();
                    foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//text()"))
                    {
                        sb.Append(node.InnerText);
                    }

                    preguntasData[j] = new { enunciado = sb.ToString(), posicion = pregunta.Posicion, tipos = tiposData };
                    j++;
                }

                bloquesData[i] = new { nombre = bloque.Nombre, preguntas = preguntasData };
                i++;
            }

            return new
            {
                titulo = cuestionario.Titulo,
                instrucciones = cuestionario.Instrucciones,
                bloques = bloquesData
            };
        }
    }
}