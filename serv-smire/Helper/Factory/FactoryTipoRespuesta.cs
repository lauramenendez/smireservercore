﻿using serv_smire.Helper.Enumerators;
using serv_smire.Models.Cuestionarios.TiposRespuestas;

namespace serv_smire.Helper.Factory
{
    /// <summary>
    /// Clase que crea el objeto que corresponde al tipo de respuesta
    /// </summary>
    public static class FactoryTipoRespuesta
    {
        /// <summary>
        /// Dado el nombre del tipo, de vuelve el objeto creado de ese tipo
        /// </summary>
        /// <param name="nombre">Nombre del tipo de respuesta</param>
        /// <returns></returns>
        public static ITipoRespuesta CreateTipo(string nombre)
        {
            switch (nombre)
            {
                case ETiposRespuesta.Unica:
                    return new Unica();
                case ETiposRespuesta.FRS:
                    return new FRS();
                case ETiposRespuesta.Intervalo:
                    return new Intervalo();
                case ETiposRespuesta.Likert:
                    return new Likert();
                case ETiposRespuesta.Multiple:
                    return new Multiple();
                case ETiposRespuesta.Numero:
                    return new Numero();
                case ETiposRespuesta.Orden:
                    return new Orden();
                case ETiposRespuesta.Texto:
                    return new Texto();
                case ETiposRespuesta.VAnalogica:
                    return new VAnalogica();
                case ETiposRespuesta.NoAplicable:
                    return new NoAplicable();
                case ETiposRespuesta.Fecha:
                    return new Fecha();
                case ETiposRespuesta.Desplegable:
                    return new Desplegable();
                default:
                    return null;
            }
        }
    }
}