﻿
using DinkToPdf;
using DinkToPdf.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using serv_smire.Helper.Email;
using serv_smire.Helper.Enumerators;
using serv_smire.Helper.PDF;
using serv_smire.Helper.Security;
using serv_smire.Helper.Validate;
using serv_smire.Models;
using serv_smire.Services;
using serv_smire.Services.Impl;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace serv_smire
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            /*
             * SQL SERVER:
             * Connection string -> "Persist Security Info=False;Integrated Security=true;Initial Catalog=smire;server=(local)"
             * Método ->
             * services.AddDbContext<DataModel>(options => options
                .UseSqlServer(Configuration.GetConnectionString("DataModel"))
                .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));*/

            /*
             * MYSQL:
             * Connection string -> "server=localhost;port=3306;database=smire;uid=root;password=244646"
             * Método ->
             */
            services.AddDbContext<DataModel>(options => options
                .UseMySQL(Configuration.GetConnectionString("DataModel"))
                .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(ERoles.Admin, authBuilder => { authBuilder.RequireClaim("rol", ERoles.Admin); });
                options.AddPolicy(ERoles.Experto, authBuilder => { authBuilder.RequireClaim("rol", ERoles.Experto); });
                options.AddPolicy(ERoles.Privilegiado, authBuilder => { authBuilder.RequireClaim("rol", ERoles.Admin, ERoles.Experto); });
                options.AddPolicy(ERoles.Generico, authBuilder => { authBuilder.RequireClaim("rol", ERoles.Generico); });
            });

            services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddSingleton<IConfiguration>(Configuration);
            services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));

            services.AddTransient<JwtManager>();
            services.AddTransient<EmailSender>();
            services.AddTransient<ValidateUser>();
            services.AddTransient<GeneratePDF>();

            services.AddTransient<DbContext, DataModel>();
            services.AddTransient<ICuestionarioRepository, CuestionarioRepository>();
            services.AddTransient<IExpertoRepository, ExpertoRepository>();
            services.AddTransient<IGenericoRepository, GenericoRepository>();
            services.AddTransient<IInstruccionRepository, InstruccionRepository>();
            services.AddTransient<IModeloRepository, ModeloRepository>();
            services.AddTransient<IPreguntaRepository, PreguntaRepository>();
            services.AddTransient<IRespuestaRepository, RespuestaRepository>();

            services.AddMvc().AddJsonOptions(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                });

            string parent = Directory.GetParent(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)).FullName;
            services.AddDataProtection()
                .PersistKeysToFileSystem(new DirectoryInfo(parent))
                .SetApplicationName("SMIRE_REST");
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.Use(async (HttpContext context, Func<Task> next) =>
            {
                await next.Invoke();

                if (context.Response.StatusCode == 404 && !context.Request.Path.Value.Contains("/api"))
                {
                    context.Request.Path = new PathString("/index.html");
                    await next.Invoke();
                }
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Para el despliegue (antes de la autenticacion)
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            // Para Angular
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseAuthentication();
            app.UseCors("CorsPolicy");

            app.UseMvc();
        }
    }
}
