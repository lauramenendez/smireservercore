﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using serv_smire.Models.Cuestionarios;

namespace serv_smire.Controllers
{
    /// <summary>
    /// Interfaz con los métodos que ofrece el servicio REST para gestionar las preguntas
    /// de un cuestionario
    /// </summary>
    public interface IPreguntasController
    {
        //************ MÉTODOS GET **************

        /// <summary>
        /// Un usuario experto solicita todas las preguntas de un cuestionario
        /// </summary>
        /// <param name="id">ID del cuestionario</param>
        /// <returns></returns>
        ICollection<Pregunta> GetPreguntas(long id);

        //************ OPERACIONES CRUD **************

        /// <summary>
        /// Un usuario experto actualiza los datos de una pregunta
        /// </summary>
        /// <param name="id">ID de la pregunta</param>
        /// <param name="pregunta">Datos de la pregunta</param>
        /// <returns></returns>
        IActionResult PutPregunta(long id, Pregunta pregunta);

        /// <summary>
        /// Un usuario experto actualiza las posiciones de dos preguntas
        /// </summary>
        /// <param name="preguntas">IDs de las preguntas a intercambiar</param>
        /// <returns></returns>
        IActionResult PutPosiciones(Pregunta[] preguntas);

        /// <summary>
        /// Un usuario experto crea una nueva pregunta
        /// </summary>
        /// <param name="pregunta">Datos de la nueva pregunta</param>
        /// <returns></returns>
        IActionResult PostPregunta(Pregunta pregunta);

        /// <summary>
        /// Un usuario experto borra una pregunta
        /// </summary>
        /// <param name="id">ID de la pregunta</param>
        /// <returns></returns>
        IActionResult DeletePregunta(long id);
    }
}
