﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using serv_smire.Models.Usuario;

namespace tfgserver.Controllers
{
    /// <summary>
    /// Interfaz con los métodos que ofrece el servicio REST para gestionar los usuarios genéricos
    /// </summary>
    public interface IGenericosController
    {
        //************ MÉTODOS GET **************

        /// <summary>
        /// Comprueba la disponibilidad del nombre de usuario
        /// </summary>
        /// <param name="username">Nombre de usuario</param>
        /// <returns>Verdadero si esta disponible, falso si no</returns>
        bool GetDisponibilidadUsuario(string username);

        /// <summary>
        /// Un usuario privilegiado solicita una lista de los usuarios genéricos del sistema
        /// </summary>
        /// <returns></returns>
        ICollection<Generico> GetGenericos();

        //************ OPERACIONES CRUD **************

        /// <summary>
        /// El usuario experto actualiza un usuario genérico que haya creado
        /// </summary>
        /// <param name="id">ID del usuario genérico</param>
        /// <param name="generico">Datos del usuario genérico</param>
        /// <returns></returns>
        IActionResult PutGenerico(long id, Generico generico);

        /// <summary>
        /// El usuario experto crea un nuevo usuario genérico
        /// </summary>
        /// <param name="generico">Datos del usuario genérico</param>
        /// <returns></returns>
        IActionResult PostGenerico(Generico generico);

        /// <summary>
        /// Un usuario experto borra un usuario genérico
        /// </summary>
        /// <param name="id">Id del usuario genérico</param>
        /// <returns></returns>
        IActionResult DeleteGenerico(long id);
    }
}
