﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using serv_smire.Models.Usuario;

namespace serv_smire.Controllers
{
    /// <summary>
    /// Interfaz con los métodos que ofrece el servicio REST para gestionar los usuarios expertos
    /// </summary>
    public interface IExpertosController
    {
        //************ MÉTODOS GET **************

        /// <summary>
        /// Comprueba la disponibilidad del nombre de usuario
        /// </summary>
        /// <param name="username">Nombre del usuario</param>
        /// <returns>Verdadero si esta disponible, falso si no</returns>
        bool GetDisponibilidadUsuario(string username);

        /// <summary>
        /// El administrador solicita una lista de todos los usuarios expertos
        /// </summary>
        /// <returns>Usuarios expertos del sistema</returns>
        ICollection<Experto> GetExpertos();

        //************ OPERACIONES CRUD **************

        /// <summary>
        /// Se registra un nuevo usuario experto y se envia un correo al administrador
        /// </summary>
        /// <param name="experto">Datos del nuevo usuario</param>
        /// <returns></returns>
        IActionResult PostExperto(Experto experto);

        /// <summary>
        /// El administrador da de baja un usuario experto, siempre que no sea el administrador
        /// </summary>
        /// <param name="id">ID del usuario</param>
        /// <returns></returns>
        IActionResult DeleteExperto(long id);

        /// <summary>
        /// Un usuario experto actualiza sus datos
        /// </summary>
        /// <param name="id">ID del usuario</param>
        /// <param name="experto">Nuevos datos del usuario</param>
        /// <returns></returns>
        IActionResult PutExperto(long id, Experto experto);

        /// <summary>
        /// Un usuario experto actualiza su contraseña
        /// </summary>
        /// <param name="id">ID del usuario</param>
        /// <param name="experto">Nuevos datos del usuario</param>
        /// <returns></returns>
        IActionResult PutContrasena(long id, Experto experto);
    }
}
