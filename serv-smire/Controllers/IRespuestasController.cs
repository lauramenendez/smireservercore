﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using serv_smire.Models.Cuestionarios;

namespace serv_smire.Controllers
{
    /// <summary>
    /// Interfaz con los métodos que ofrece el servicio REST para gestionar las respuestas
    /// a un cuestionario
    /// </summary>
    public interface IRespuestasController
    {
        //************ MÉTODOS GET **************

        /// <summary>
        /// Un usuario experto solicita todas las respuestas de un cuestionario
        /// </summary>
        /// <param name="username">Nombre del usuario</param>
        /// <param name="cuestionario">Titulo del cuestionario</param>
        /// <returns></returns>
        ICollection<Respuesta> GetRespuestas(string username, string cuestionario);

        //************ OPERACIONES CRUD **************

        /// <summary>
        /// Un usuario generico envía respuestas
        /// </summary>
        /// <param name="respuestas">Datos de las respuestas</param>
        /// <returns></returns>
        Task<IActionResult> PostRespuestas(Respuesta[] respuestas);
    }
}
