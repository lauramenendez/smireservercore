﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace serv_smire.Controllers
{
    /// <summary>
    /// Interfaz con los métodos que ofrece el servicio REST para gestionar las instrucciones
    /// de un cuestionario
    /// </summary>
    public interface IInstruccionController
    {
        //************ MÉTODOS GET **************

        /// <summary>
        /// Se solicita un fichero de instrucciones con un ID concreto
        /// </summary>
        /// <returns>Fichero de instruccion con ese ID</returns>
        IActionResult GetInstruccion(long id);

        //************ OPERACIONES CRUD **************

        /// <summary>
        /// Se almacenan los ficheros de instrucciones de un cuestionario
        /// en la base de datos
        /// </summary>
        /// <param name="cuestionarioId">ID del cuestionario al que pertenecen</param>
        /// <returns></returns>
        Task<IActionResult> PostInstrucciones(long cuestionarioId);

        /// <summary>
        /// Elimina todas las instrucciones del cuestionario con ese ID
        /// </summary>
        /// <param name="cuestionarioId">ID del cuestionario al que pertenecen</param>
        /// <returns></returns>
        IActionResult DeleteInstrucciones(long cuestionarioId);
    }
}
