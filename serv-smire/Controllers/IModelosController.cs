﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using serv_smire.Models.Cuestionarios;

namespace serv_smire.Controllers
{
    /// <summary>
    /// Interfaz con los métodos que ofrece el servicio REST para gestionar los modelos
    /// </summary>
    public interface IModelosController
    {
        //************ MÉTODOS GET **************

        /// <summary>
        /// Un usuario experto solicita todos los modelos de un cuestionario
        /// </summary>
        /// <param name="cuestionario">Título del cuestionario</param>
        /// <returns></returns>
        ICollection<Modelo> GetModelos(string cuestionario);

        //************ OPERACIONES CRUD **************

        /// <summary>
        /// Se crea un nuevo modelo para un cuestionario
        /// </summary>
        /// <param name="modelo">Datos del nuevo modelo</param>
        /// <returns></returns>
        IActionResult PostModelo(Modelo modelo);

        /// <summary>
        /// Se elimina un modelo según su ID
        /// </summary>
        /// <param name="id">ID del modelo</param>
        /// <returns></returns>
        IActionResult DeleteModelo(long id);
    }
}
