﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using serv_smire.Models.Usuario;

namespace serv_smire.Controllers
{
    /// <summary>
    /// Interfaz con los métodos que ofrece el servicio REST para iniciar sesión en el sistema
    /// </summary>
    public interface ILoginController
    {
        /// <summary>
        /// Se envia el usuario y contrasena del experto para validarlo
        /// contra la base de datos
        /// </summary>
        /// <param name="experto">Datos del usuario</param>
        /// <returns></returns>
        IActionResult PostExperto(Experto experto);

        /// <summary>
        /// Se envia el usuario, contrasena, cuestionario e identificador 
        /// para validarlo contra la base de datos
        /// </summary>
        /// <param name="generico">Datos del usuario</param>
        /// <returns></returns>
        Task<IActionResult> PostGenerico(Generico generico);
    }
}
