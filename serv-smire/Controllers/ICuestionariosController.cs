﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using serv_smire.Models.Cuestionarios;

namespace serv_smire.Controllers
{
    /// <summary>
    /// Interfaz con los métodos que ofrece el servicio REST para gestionar los cuestionarios
    /// </summary>
    public interface ICuestionariosController
    {
        //************ MÉTODOS GET **************

        /// <summary>
        /// El administrador solicita todos los cuestionarios
        /// </summary>
        /// <returns>Todos los cuestionarios del sistema</returns>
        ICollection<Cuestionario> GetCuestionarios();

        /// <summary>
        /// Un usuario no identificado solicita todos los cuestionarios activos
        /// </summary>
        /// <returns>Cuestionario con ese id</returns>
        Task<ICollection<Cuestionario>> GetCuestionariosActivos();

        /// <summary>
        /// Un usuario experto solicita sus cuestionarios
        /// </summary>
        /// <param name="user">ID del usuario</param>
        /// <returns>Cuestionarios del usuario</returns>
        ICollection<Cuestionario> GetCuestionariosByUser(long user);

        /// <summary>
        /// Un usuario experto solicita un cuestionario con un modelo
        /// </summary>
        /// <param name="idCuestionario">ID del cuestionario</param>
        /// <param name="idModelo">ID del modelo</param>
        /// <returns>Cuestionario con ese modelo</returns>
        Cuestionario GetCuestionario(long idCuestionario, long idModelo);

        /// <summary>
        /// Un usuario generico solicita el cuestionario que ya ha respondido
        /// </summary>
        /// <param name="idCuestionario">ID del cuestionario</param>
        /// <param name="identificador">Identificador del usuario</param>
        /// <returns>Cuestionario respondido</returns>
        Task<Cuestionario> GetCuestionarioRespondido(long idCuestionario, string identificador);

        //************ OPERACIONES CRUD **************

        /// <summary>
        /// Un usuario experto actualiza un cuestionario
        /// </summary>
        /// <param name="cuestionarioId">ID del cuestionario</param>
        /// <param name="cuestionario">Datos del cuestionario</param>
        /// <returns></returns>
        IActionResult PutCuestionario(long cuestionarioId, Cuestionario cuestionario);

        /// <summary>
        /// Un usuario experto actualiza las posiciones de los bloques de un cuestionario
        /// </summary>
        /// <param name="cuestionarioId">Id del cuestionario</param>
        /// <param name="bloques">Ids de los bloques a intercambiar</param>
        /// <returns></returns>
        IActionResult PutPosiciones(long cuestionarioId, long[] bloques);

        /// <summary>
        /// Un usuario experto crea un nuevo cuestionario
        /// </summary>
        /// <param name="cuestionario">Datos del nuevo cuestionario</param>
        /// <returns></returns>
        IActionResult PostCuestionario(Cuestionario cuestionario);

        /// <summary>
        /// Un usuario experto borra un cuestionario
        /// </summary>
        /// <param name="cuestionarioId">ID del cuestionario</param>
        /// <returns></returns>
        IActionResult DeleteCuestionario(long cuestionarioId);

        //************ OPERACIONES AUXILIARES **************

        /// <summary>
        /// Reune las respuestas de todos los cuestionarios, además
        /// actualiza el cuestionario para indicar que ya se han descargado las respuestas
        /// </summary>
        /// <param name="cuestionarios">Titulos de los cuestionarios</param>
        /// <returns>Las respuestas en CSV</returns>
        IActionResult DownloadRespuestas(string[] cuestionarios);

        /// <summary>
        /// Genera un PDF con las preguntas del cuestionario
        /// </summary>
        /// <param name="cuestionarioId"></param>
        /// <returns>El cuestionario en PDF</returns>
        IActionResult ExportCuestionario(long cuestionarioId);
    }
}
