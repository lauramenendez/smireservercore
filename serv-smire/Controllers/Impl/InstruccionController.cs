﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using serv_smire.Helper.Archivos;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Models.Cuestionarios;
using serv_smire.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using serv_smire.Helper.Enumerators;

namespace serv_smire.Controllers.Impl
{
    [Route("api/instrucciones")]
    public class InstruccionController : Controller, IInstruccionController
    {
        private readonly IInstruccionRepository _repository;
        private readonly ICuestionarioRepository _repositoryAux;

        private readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public InstruccionController(IInstruccionRepository repository, ICuestionarioRepository repositoryAux)
        {
            _repository = repository;
            _repositoryAux = repositoryAux;
        }

        //************ MÉTODOS GET **************

        // GET: api/instrucciones/{id}
        [HttpGet]
        [Route("{id}")]
        [AllowAnonymous]
        public IActionResult GetInstruccion(long id)
        {
            if (!InstruccionExists(id))
            {
                Log.Error("GetInstruccion(" + id + "): Not found");
                return NotFound(ErrorMsg.getMsg(eError.NotFound));
            }

            try
            {
                Instruccion instruccion = _repository.GetInstruccion(id);
                byte[] buffer = instruccion.Filedata;

                Log.Info("GetInstruccion(" + id + ")");
                return File(buffer, instruccion.Filetype, instruccion.Filename);
            }
            catch (Exception e)
            {
                Log.Error("GetInstruccion(" + id + "): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }
        }

        //************ OPERACIONES CRUD **************

        // POST: api/instrucciones/{cuestionarioId}
        [HttpPost]
        [Route("{cuestionarioId}")]
        [Authorize(ERoles.Experto)]
        public async Task<IActionResult> PostInstrucciones(long cuestionarioId)
        {
            if (HttpContext.Request.Form.Files == null)
            {
                Log.Error("AddInstruccion(" + cuestionarioId + "): Unsupported Media Type");
                return StatusCode(StatusCodes.Status415UnsupportedMediaType);
            }
            if (!CuestionarioExists(cuestionarioId))
            {
                Log.Error("AddInstruccion(" + cuestionarioId + "): Not found");
                return NotFound(ErrorMsg.getMsg(eError.NotFound));
            }

            try
            {
                var files = HttpContext.Request.Form.Files;
                ICollection<Instruccion> instrucciones = _repository.Add(await InstructionGenerator.Get(files, cuestionarioId));

                Log.Info("AddInstruccion(" + cuestionarioId + ")");
                return Created("Instrucciones", instrucciones);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("AddInstruccion(" + cuestionarioId + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("AddInstruccion(" + cuestionarioId + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Error("AddInstruccion(" + cuestionarioId + "): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }
        }

        // DELETE: api/instrucciones/{cuestionarioId}
        [HttpDelete]
        [Route("{cuestionarioId}")]
        [Authorize(ERoles.Experto)]
        public IActionResult DeleteInstrucciones(long cuestionarioId)
        {
            if (!CuestionarioExists(cuestionarioId))
            {
                Log.Error("DeleteInstrucciones(" + cuestionarioId + "): Not found");
                return NotFound(ErrorMsg.getMsg(eError.NotFound));
            }

            try
            {
                _repository.Delete(cuestionarioId);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("DeleteInstrucciones(" + cuestionarioId + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("DeleteInstrucciones(" + cuestionarioId + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Error("DeleteInstrucciones(" + cuestionarioId + "): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("DeleteInstrucciones(" + cuestionarioId + ")");
            return Ok(new { Message = "Se han eliminado con exito" });
        }

        //************ OTRAS **************

        private bool InstruccionExists(long id)
        {
            return _repository.Exists(id);
        }

        private bool CuestionarioExists(long id)
        {
            return _repositoryAux.Exists(id);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}