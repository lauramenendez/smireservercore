﻿using log4net;
using System;
using System.Collections.Generic;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Models.Usuario;
using serv_smire.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tfgserver.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using serv_smire.Helper.Enumerators;

namespace serv_smire.Controllers.Impl
{
    [Route("api/genericos")]
    public class GenericosController : Controller, IGenericosController
    {
        private readonly IGenericoRepository _repository;
        private readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public GenericosController(IGenericoRepository repository)
        {
            _repository = repository;
        }

        //************ MÉTODOS GET **************

        // GET: api/genericos/check/{username}
        [HttpGet]
        [Route("check/{username}")]
        [AllowAnonymous]
        public bool GetDisponibilidadUsuario(string username)
        {
            Log.Info("GetDisponibilidadUsuario()");
            return _repository.CheckExistUsername(username);
        }

        // GET: api/Genericos
        /// <summary>
        /// Se solicita una lista de los usuarios genericos del sistema
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(ERoles.Privilegiado)]
        public ICollection<Generico> GetGenericos()
        {
            Log.Info("GetGenericos()");
            return _repository.GetGenericos();
        }

        //************ OPERACIONES CRUD **************

        // PUT: api/Genericos/{id}
        [HttpPut]
        [Route("{id}")]
        [Authorize(ERoles.Experto)]
        public IActionResult PutGenerico(long id, [FromBody] Generico generico)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("UpdateGenerico(" + id + "): Not found");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidModelState));
            }
            if (generico == null || id != generico.ID)
            {
                Log.Error("UpdateGenerico(" + id + "): Incorrect parameter");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }
            if (!GenericoExists(id))
            {
                Log.Error("UpdateGenerico(" + id + "): Not found");
                return NotFound(ErrorMsg.getMsg(eError.NotFound));
            }
            if (GenericoExists(generico.Usuario, id))
            {
                Log.Error("UpdateGenerico(" + id + "): Already exist");
                return BadRequest(ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Usuario));
            }

            try
            {
                _repository.Update(generico);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("UpdateGenerico(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("UpdateGenerico(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Fatal("UpdateGenerico(" + id + "): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("UpdateGenerico(" + id + ")");
            return StatusCode(StatusCodes.Status204NoContent);
        }

        // POST: api/Genericos
        [HttpPost]
        [Authorize(ERoles.Experto)]
        public IActionResult PostGenerico([FromBody] Generico generico)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("AddGenerico(): Model invalid");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidModelState));
            }
            if (generico == null)
            {
                Log.Error("AddGenerico(): Parameter incorrect");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }
            if (GenericoExists(generico.ID) || GenericoExists(generico.Usuario))
            {
                Log.Error("AddGenerico(): Already exist");
                return BadRequest(ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Usuario));
            }

            try
            {
                _repository.Add(generico);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("AddGenerico(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("AddGenerico(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Fatal("AddGenerico(): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("AddGenerico()");
            return Created("Genericos", generico);
        }

        // DELETE: api/genericos/{id}
        [HttpDelete]
        [Route("{id}")]
        [Authorize(ERoles.Experto)]
        public IActionResult DeleteGenerico(long id)
        {
            Generico generico = _repository.GetGenerico(id);
            if (generico == null)
            {
                Log.Error("DeleteGenerico(" + id + "): Not found");
                return NotFound(ErrorMsg.getMsg(eError.NotFound));
            }

            try
            {
                _repository.Delete(id);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("DeleteGenerico(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("DeleteGenerico(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (InvalidOperationException e)
            {
                Log.Error("DeleteGenerico(" + id + "): " + e.Message);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                Log.Fatal("DeleteGenerico(" + id + "): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("DeleteGenerico(" + id + ")");
            return Ok(generico);
        }

        //************ OTRAS **************

        private bool GenericoExists(long id)
        {
            return _repository.Exists(id);
        }

        private bool GenericoExists(string usuario, long id = 0)
        {
            return _repository.Exists(usuario, id);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}