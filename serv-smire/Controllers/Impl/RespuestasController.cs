﻿using log4net;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Models.Cuestionarios;
using serv_smire.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using serv_smire.Helper.Enumerators;

namespace serv_smire.Controllers.Impl
{
    [Route("api/respuestas")]
    public class RespuestasController : Controller, IRespuestasController
    {
        private readonly IRespuestaRepository _repository;
        private readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public RespuestasController(IRespuestaRepository repository)
        {
            _repository = repository;
        }

        //************ MÉTODOS GET **************

        // GET: api/Respuestas/{username}/{cuestionario}
        [HttpGet]
        [Route("{username}/{cuestionario}")]
        [Authorize(ERoles.Experto)]
        public ICollection<Respuesta> GetRespuestas(string username, string cuestionario)
        {
            Log.Info("GetRespuestas(" + username + ", " + cuestionario + ")");
            return _repository.GetRespuestas(username, cuestionario);
        }

        //************ OPERACIONES CRUD **************

        // POST: api/Respuestas
        [HttpPost]
        [Authorize(ERoles.Generico)]
        public async Task<IActionResult> PostRespuestas([FromBody] Respuesta[] respuestas)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("AddRespuestas(): Model invalid");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidModelState));
            }
            if (respuestas == null)
            {
                Log.Error("AddRespuestas(): Respuesta null");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }

            try
            {
                await _repository.Add(respuestas);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("AddRespuestas(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("AddRespuestas(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Fatal("AddRespuestas(): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("AddRespuestas()");
            return Ok(respuestas);
        }
    }
}