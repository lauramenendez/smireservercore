﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Models.Cuestionarios;
using serv_smire.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using serv_smire.Helper.Enumerators;
using MySql.Data.MySqlClient;

namespace serv_smire.Controllers.Impl
{
    [Route("api/cuestionarios")]
    public class CuestionariosController : Controller, ICuestionariosController
    {
        private readonly ICuestionarioRepository _repository;
        private readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public CuestionariosController(ICuestionarioRepository repository)
        {
            _repository = repository;
        }

        //************ MÉTODOS GET **************

        // GET: api/cuestionarios/activos
        [HttpGet]
        [Route("activos")]
        [AllowAnonymous]
        public async Task<ICollection<Cuestionario>> GetCuestionariosActivos()
        {
            Log.Info("GetCuestionariosActivos()");
            return await _repository.GetCuestionariosActivos();
        }

        // GET: api/cuestionarios/respondido/{idCuestionario}/{identificador}
        [HttpGet]
        [Route("respondido/{idCuestionario}/{identificador}")]
        [Authorize(ERoles.Generico)]
        public async Task<Cuestionario> GetCuestionarioRespondido(long idCuestionario, string identificador)
        {
            Log.Info("GetCuestionarioRespondido(" + idCuestionario + ", " + identificador + ")");
            return await _repository.GetCuestionarioModelo(idCuestionario, identificador);
        }

        // GET: api/cuestionarios/all
        [HttpGet]
        [Route("all")]
        [Authorize(ERoles.Admin)]
        public ICollection<Cuestionario> GetCuestionarios()
        {
            Log.Info("GetCuestionarios()");
            return _repository.GetCuestionarios();
        }

        // GET: api/cuestionarios/experto/{user}
        [HttpGet]
        [Route("experto/{user}")]
        [Authorize(ERoles.Experto)]
        public ICollection<Cuestionario> GetCuestionariosByUser(long user)
        {
            Log.Info("GetCuestionariosByUser(" + user + ")");
            return _repository.GetCuestionariosUsuario(user);
        }

        // GET: api/cuestionarios/{idCuestionario}/{idModelo}
        [HttpGet]
        [Route("{idCuestionario}/{idModelo}")]
        [Authorize(ERoles.Experto)]
        public Cuestionario GetCuestionario(long idCuestionario, long idModelo)
        {
            Log.Info("GetCuestionario(" + idCuestionario + ", " + idModelo + ")");
            return _repository.GetCuestionarioModelo(idCuestionario, idModelo);
        }

        //************ OPERACIONES CRUD **************

        // PUT: api/Cuestionarios/{id}
        [HttpPut]
        [Route("{cuestionarioId}")]
        [Authorize(ERoles.Experto)]
        public IActionResult PutCuestionario(long cuestionarioId, [FromBody] Cuestionario cuestionario)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("UpdateCuestionario(" + cuestionarioId + "): Model invalid");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidModelState));
            }
            if (cuestionario == null || cuestionarioId != cuestionario.ID)
            {
                Log.Error("UpdateCuestionario(" + cuestionarioId + "): Incorrect parameters");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }
            if (!CuestionarioExists(cuestionarioId))
            {
                Log.Error("UpdateCuestionario(" + cuestionarioId + "): Not found");
                return NotFound(ErrorMsg.getMsg(eError.NotFound));
            }
            if (CuestionarioExists(cuestionario.Titulo, cuestionarioId))
            {
                Log.Error("UpdateCuestionario(): Already exist");
                return BadRequest(ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Cuestionario));
            }

            try
            {
                Cuestionario actualizado = _repository.Update(cuestionario);

                Log.Info("UpdateCuestionario(" + cuestionarioId + ")");
                return Ok(actualizado);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("UpdateCuestionario(" + cuestionarioId + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("UpdateCuestionario(" + cuestionarioId + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Fatal("UpdateCuestionario(" + cuestionarioId + "): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }
        }

        // PUT: api/Cuestionarios/{cuestionarioId}/posiciones
        [HttpPut]
        [Route("{cuestionarioId}/posiciones")]
        [Authorize(ERoles.Experto)]
        public IActionResult PutPosiciones(long cuestionarioId, [FromBody] long[] bloques)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("UpdatePosiciones(" + cuestionarioId + "): Model invalid");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidModelState));
            }
            if (bloques == null || bloques.Length != 2)
            {
                Log.Error("UpdateCuestionario(" + cuestionarioId + "): Incorrect parameter");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }
            if (!CuestionarioExists(cuestionarioId) || !BloqueExists(cuestionarioId, bloques[0]) || !BloqueExists(cuestionarioId, bloques[1]))
            {
                Log.Error("UpdatePosiciones(" + cuestionarioId + "): Not found");
                return NotFound(ErrorMsg.getMsg(eError.NotFound));
            }

            try
            {
                _repository.UpdatePosiciones(cuestionarioId, bloques[0], bloques[1]);
            }
            catch (DbUpdateConcurrencyException e) 
            {
                Log.Error("UpdatePosiciones(" + cuestionarioId + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("UpdatePosiciones(" + cuestionarioId + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Fatal("UpdatePosiciones(" + cuestionarioId + "): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("UpdatePosiciones(" + cuestionarioId + ")");
            return Ok(bloques);
        }

        // POST: api/cuestionarios/save
        [HttpPost]
        [Route("save")]
        [Authorize(ERoles.Experto)]
        public IActionResult PostCuestionario([FromBody] Cuestionario cuestionario)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("AddCuestionario(): Model invalid");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidModelState));
            }
            if (cuestionario == null)
            {
                Log.Error("AddCuestionario(): Cuestionario null");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }
            if (CuestionarioExists(cuestionario.ID) || CuestionarioExists(cuestionario.Titulo))
            {
                Log.Error("AddCuestionario(): Already exist");
                return BadRequest(ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Cuestionario));
            }

            try
            {
                cuestionario = _repository.Add(cuestionario);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("AddCuestionario(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("AddCuestionario(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(ErrorDb.getErrorCode((MySqlException) e.InnerException), eClass.Cuestionario));
            }
            catch (Exception e)
            {
                Log.Fatal("AddCuestionario(): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("AddCuestionario()");
            return Created("Cuestionarios", cuestionario);
        }

        // DELETE: api/Cuestionarios/{cuestionarioId}
        [HttpDelete]
        [Route("{cuestionarioId}")]
        [Authorize(ERoles.Experto)]
        public IActionResult DeleteCuestionario(long cuestionarioId)
        {
            Cuestionario cuestionario = _repository.GetCuestionario(cuestionarioId);
            if (cuestionario == null)
            {
                Log.Error("AddCuestionario(" + cuestionarioId + "): Not found");
                return NotFound();
            }

            try
            { 
                _repository.Delete(cuestionarioId);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("DeleteCuestionario(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("DeleteCuestionario(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (InvalidOperationException e)
            {
                Log.Error("DeleteCuestionario(): " + e.Message);
                return BadRequest(new { Message = e.Message });
            }
            catch (Exception e)
            {
                Log.Fatal("DeleteCuestionario(): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("DeleteCuestionario(" + cuestionarioId + ")");
            return Ok(cuestionario);
        }

        //************ OPERACIONES AUXILIARES **************

        // PUT: api/cuestionarios/download
        [HttpPut]
        [Authorize(ERoles.Privilegiado)]
        [Route("download")]
        public IActionResult DownloadRespuestas([FromBody] string[] cuestionarios)
        {
            foreach(string cuestionario in cuestionarios) {
                if (!CuestionarioExists(cuestionario))
                {
                    Log.Error("GetRespuestas(): Not found " + cuestionario);
                    return NotFound(ErrorMsg.getMsg(eError.NotFound));
                }
            }

            try
            {
                var respuestas = _repository.GetRespuestas(cuestionarios);
                Log.Info("GetRespuestas()");
                return Ok(respuestas);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("DeleteCuestionario(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("GetRespuestas(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Fatal("GetRespuestas(): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }
        }

        // GET: api/cuestionarios/export/{cuestionarioId}
        [HttpGet]
        [Authorize(ERoles.Privilegiado)]
        [Route("export/{cuestionarioId}")]
        public IActionResult ExportCuestionario(long cuestionarioId)
        {
            Cuestionario cuestionario = _repository.GetCuestionario(cuestionarioId);
            if (cuestionario == null)
            {
                Log.Error("ExportCuestionario(): Not found");
                return NotFound(ErrorMsg.getMsg(eError.NotFound));
            }

            try
            {
                byte[] buffer = _repository.ExportCuestionario(cuestionarioId);
                
                Log.Info("ExportCuestionario()");
                return File(buffer, "application/pdf", cuestionario.Titulo);
            }
            catch (Exception e)
            {
                Log.Error("ExportCuestionario(): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.NotFound));
            }
        }

        //************ OTRAS **************

        private bool CuestionarioExists(long id)
        {
            return _repository.Exists(id);
        }

        private bool CuestionarioExists(string titulo, long id = 0)
        {
            return _repository.Exists(titulo, id);
        }

        private bool BloqueExists(long idCuestionario, long idBloque)
        {
            return _repository.BloqueExists(idCuestionario, idBloque);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}