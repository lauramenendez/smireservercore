﻿using System;
using log4net;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using serv_smire.Models.Cuestionarios;
using serv_smire.Services;
using serv_smire.Helper.ErrorHandler;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using serv_smire.Helper.Enumerators;

namespace serv_smire.Controllers.Impl
{
    [Route("api/modelos")]
    public class ModelosController : Controller, IModelosController
    {
        private readonly IModeloRepository _repository;
        private readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ModelosController(IModeloRepository repository)
        {
            _repository = repository;
        }

        //************ MÉTODOS GET **************

        // GET: api/Modelos/{cuestionario}
        [HttpGet]
        [Route("{cuestionario}")]
        [Authorize(ERoles.Experto)]
        public ICollection<Modelo> GetModelos(string cuestionario)
        {
            return _repository.GetModelos(cuestionario);
        }

        //************ OPERACIONES CRUD **************

        // POST: api/Modelos
        [HttpPost]
        [Authorize(ERoles.Experto)]
        public IActionResult PostModelo([FromBody] Modelo modelo)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("AddModelo(): Model invalid");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidModelState));
            }
            if (modelo == null)
            {
                Log.Error("AddModelo(): Modelo null");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }
            if (ModeloExists(modelo.ID) || ModeloExists(modelo.Cuestionario_ID, modelo.Nombre))
            {
                Log.Error("AddModelo(): Already exist");
                return BadRequest(ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Modelo));
            }

            try
            {
                modelo = _repository.Add(modelo);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("AddModelo(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("AddModelo(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Fatal("AddModelo(): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("AddModelo()");
            return Created("Modelos", modelo);
        }

        // DELETE: api/Modelos/{id}
        [HttpDelete]
        [Route("{id}")]
        [Authorize(ERoles.Experto)]
        public IActionResult DeleteModelo(long id)
        {
            Modelo modelo = _repository.GetModelo(id);
            if (modelo == null)
            {
                Log.Error("DeleteModelo(" + id + "): Not found");
                return NotFound(ErrorMsg.getMsg(eError.NotFound));
            }

            try
            {
                _repository.Delete(id);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("DeleteModelo(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("DeleteModelo(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Fatal("DeleteModelo(" + id + "): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("DeleteModelo(" + id + ")");
            return Ok(modelo);
        }

        //************ OTRAS **************

        private bool ModeloExists(long id)
        {
            return _repository.Exists(id);
        }

        private bool ModeloExists(long? cuestionarioId, string nombre)
        {
            return _repository.Exists(cuestionarioId, nombre);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}