﻿using log4net;
using System;
using System.Collections.Generic;
using serv_smire.Helper.Email;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Models.Usuario;
using serv_smire.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using serv_smire.Helper.Enumerators;

namespace serv_smire.Controllers.Impl
{
    [Route("api/expertos")]
    public class ExpertosController : Controller, IExpertosController
    {
        private readonly IExpertoRepository _repository;
        private readonly EmailSender _sender;
        private readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ExpertosController(IExpertoRepository repository, EmailSender sender)
        {
            _repository = repository;
            _sender = sender;
        }

        //************ MÉTODOS GET **************

        // GET: api/expertos/check/{username}
        [HttpGet]
        [AllowAnonymous]
        [Route("check/{username}")]
        public bool GetDisponibilidadUsuario(string username)
        {
            Log.Info("GetDisponibilidadUsuario()");
            return _repository.CheckExistUsername(username);
        }

        // GET: api/Expertos
        [HttpGet]
        [Authorize(ERoles.Admin)]
        public ICollection<Experto> GetExpertos()
        {
            Log.Info("GetExpertos()");
            return _repository.GetExpertos();
        }

        //************ OPERACIONES CRUD **************

        // POST: api/Expertos
        [HttpPost]
        [AllowAnonymous]
        public IActionResult PostExperto([FromBody] Experto experto)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("AddExperto(): Model invalid");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidModelState));
            }
            if (experto == null)
            {
                Log.Error("AddExperto(): Incorrect parameter");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }
            if (ExpertoExists(experto.ID) || ExpertoExists(experto.Usuario))
            {
                Log.Error("AddExperto(): Already exist");
                return BadRequest(ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Usuario));
            }

            try
            {
                _repository.Add(experto);
                // Enviamos un correo a los administradores con los datos del nuevo usuario para que lo confirme
                if (_sender != null)
                {
                    _sender.Send(experto, _repository.GetCorreosAdmin());
                }
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("AddExperto(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("AddExperto(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Fatal("AddExperto(): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("AddExperto()");
            return Created("Expertos", experto);
        }

        // DELETE: api/Expertos/{id}
        [HttpDelete]
        [Route("{id}")]
        [Authorize(ERoles.Admin)]
        public IActionResult DeleteExperto(long id)
        {
            Experto experto = _repository.GetExperto(id);
            if (experto == null)
            {
                Log.Error("DeleteExperto(" + id + "): Not found");
                return NotFound(ErrorMsg.getMsg(eError.NotFound));
            }
            if (experto.EsAdmin)
            {
                Log.Error("DeleteExperto(" + id + "): Not admin");
                return BadRequest(ErrorMsg.getMsg(eError.DeleteAdmin));
            }

            try
            {
                _repository.Delete(id);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("DeleteExperto(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("DeleteExperto(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Fatal("DeleteExperto(" + id + "): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("DeleteExperto(" + id + ")");
            return Ok(experto);
        }

        // PUT: api/Expertos/{id}
        [HttpPut]
        [Route("{id}")]
        [Authorize(ERoles.Privilegiado)]
        public IActionResult PutExperto(long id, [FromBody] Experto experto)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("UpdateExperto(" + id + "): Model invalid");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidModelState));
            }
            if (experto == null || id != experto.ID)
            {
                Log.Error("UpdateExperto(" + id + "): Incorrect parameter");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }
            if(!ExpertoExists(id))
            {
                Log.Error("UpdateExperto(" + id + "): Not found");
                return NotFound(ErrorMsg.getMsg(eError.NotFound));
            }
            if (ExpertoExists(experto.Usuario, id))
            {
                Log.Error("UpdateExperto(" + id + "): Already exist");
                return BadRequest(ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Usuario));
            }

            try
            {
                _repository.Update(experto);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("UpdateExperto(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("UpdateExperto(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Fatal("UpdateExperto(" + id + "): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Error("UpdateExperto(" + id + ")");
            return Ok(experto);
        }

        // PUT: api/Expertos/contrasena/{id}
        [HttpPut]
        [Authorize(ERoles.Privilegiado)]
        [Route("contrasena/{id}")]
        public IActionResult PutContrasena(long id, [FromBody] Experto experto)
        {
            if (experto == null || id != experto.ID)
            {
                Log.Error("UpdateContrasena(" + id + "): Incorrect parameter");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }
            if (string.IsNullOrEmpty(experto.Contrasena) || string.IsNullOrEmpty(experto.NuevaContrasena) 
                || string.IsNullOrEmpty(experto.RepContrasena))
            {
                Log.Error("UpdateContrasena(" + id + "): Incorrect parameter");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }
            if (!ExpertoExists(id))
            {
                Log.Error("UpdateContrasena(" + id + "): Not found");
                return NotFound(ErrorMsg.getMsg(eError.NotFound));
            }

            try
            {
                _repository.UpdateContrasena(experto);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("UpdateContrasena(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("UpdateContrasena(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (InvalidOperationException e)
            {
                Log.Fatal("UpdateContrasena(" + id + "): " + e.Message);
                return BadRequest(new { Message = e.Message });
            }
            catch (Exception e)
            {
                Log.Fatal("UpdateContrasena(" + id + "): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Error("UpdateContrasena(" + id + ")");
            return Ok(experto);
        }

        //************ OTRAS **************

        private bool ExpertoExists(long id)
        {
            return _repository.Exists(id);
        }

        private bool ExpertoExists(string user, long id = 0)
        {
            return _repository.Exists(user, id);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}