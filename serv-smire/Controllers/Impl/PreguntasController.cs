﻿using log4net;
using System;
using System.Collections.Generic;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Models.Cuestionarios;
using serv_smire.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using serv_smire.Helper.Enumerators;
using Microsoft.EntityFrameworkCore;

namespace serv_smire.Controllers.Impl
{
    [Route("api/preguntas")]
    public class PreguntasController : Controller, IPreguntasController
    {
        private readonly IPreguntaRepository _repository;
        private readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public PreguntasController(IPreguntaRepository repository)
        {
            _repository = repository;
        }

        //************ MÉTODOS GET **************

        // GET: api/preguntas/{id}
        [HttpGet]
        [Route("{id}")]
        [Authorize(ERoles.Experto)]
        public ICollection<Pregunta> GetPreguntas(long id)
        {
            Log.Info("GetPreguntas(" + id + ")"); 
            return _repository.GetPreguntas(id);
        }

        //************ OPERACIONES CRUD **************

        // PUT: api/Preguntas/{id}
        [HttpPut]
        [Route("{id}")]
        [Authorize(ERoles.Experto)]
        public IActionResult PutPregunta(long id, [FromBody] Pregunta pregunta)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("UpdatePregunta(" + id + "): Not found");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidModelState));
            }
            if (pregunta == null || id != pregunta.ID)
            {
                Log.Error("UpdatePregunta(" + id + "): Incorrect parameter");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }
            if (!PreguntaExists(id))
            {
                Log.Error("UpdatePregunta(" + id + "): Not found");
                return NotFound(ErrorMsg.getMsg(eError.NotFound));
            }

            try
            {
                _repository.Update(pregunta);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("UpdatePregunta(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("UpdatePregunta(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Fatal("UpdatePregunta(" + id + "): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("UpdatePregunta(" + id + ")");
            return StatusCode(StatusCodes.Status204NoContent);
        }

        // PUT: api/Preguntas/posiciones
        [HttpPut]
        [Route("posiciones")]
        [Authorize(ERoles.Experto)]
        public IActionResult PutPosiciones([FromBody] Pregunta[] preguntas)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("UpdatePosiciones(): Model invalid");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidModelState));
            }
            if (preguntas == null || preguntas.Length != 2)
            {
                Log.Error("UpdatePosiciones(): Incorrect parameter");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }
            if (!PreguntaExists(preguntas[0].ID) || !PreguntaExists(preguntas[1].ID))
            {
                Log.Error("UpdatePosiciones(): Not found");
                return NotFound(ErrorMsg.getMsg(eError.NotFound));
            }

            try
            {
                _repository.UpdatePosiciones(preguntas[0], preguntas[1]);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("UpdatePosiciones(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("UpdatePosiciones(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Fatal("UpdatePosiciones(): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("UpdatePosiciones()");
            return Ok(preguntas);
        }

        // POST: api/Preguntas
        [HttpPost]
        [Authorize(ERoles.Experto)]
        public IActionResult PostPregunta([FromBody] Pregunta pregunta)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("AddPregunta(): Model invalid");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidModelState));
            }
            if (pregunta == null)
            {
                Log.Error("AddPregunta(): Incorrect parameter");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }
            if (PreguntaExists(pregunta.ID))
            {
                Log.Error("AddPregunta(): Already exist");
                return BadRequest(ErrorMsg.getMsg(eError.UniqueConstraint, eClass.Pregunta));
            }

            try
            {
                _repository.Add(pregunta);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("AddPregunta(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("AddPregunta(): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Fatal("AddPregunta(): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("AddPregunta()");
            return Created("Preguntas", pregunta);
        }

        // DELETE: api/Preguntas/{id}
        [HttpDelete]
        [Route("{id}")]
        [Authorize(ERoles.Experto)]
        public IActionResult DeletePregunta(long id)
        {
            Pregunta pregunta = _repository.GetPregunta(id);
            if (pregunta == null)
            {
                Log.Error("DeletePregunta(" + id + "): Not found");
                return NotFound(ErrorMsg.getMsg(eError.NotFound));
            }

            try
            {
                _repository.Delete(id);
            }
            catch (DbUpdateConcurrencyException e)
            {
                Log.Error("DeletePregunta(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (DbUpdateException e)
            {
                Log.Error("DeletePregunta(" + id + "): " + e.InnerException.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Database));
            }
            catch (Exception e)
            {
                Log.Fatal("DeletePregunta(" + id + "): " + e.Message);
                return BadRequest(ErrorMsg.getMsg(eError.Unknown));
            }

            Log.Info("DeletePregunta(" + id + ")");
            return Ok(pregunta);
        }

        //************ OTRAS **************

        private bool PreguntaExists(long id)
        {
            return _repository.Exists(id);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}