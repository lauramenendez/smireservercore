﻿using System;
using System.Net;
using serv_smire.Models.Usuario;
using serv_smire.Services;
using serv_smire.Helper.Validate;
using log4net;
using serv_smire.Helper.ErrorHandler;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace serv_smire.Controllers.Impl
{
    [Route("api/login")]
    public class LoginController : Controller, ILoginController
    {
        private readonly IGenericoRepository _repositoryGenerico;
        private readonly IExpertoRepository _repositoryExperto;
        private readonly ICuestionarioRepository _repositoryCuestionario;

        private readonly ValidateUser _validate;

        private readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public LoginController(IGenericoRepository repositoryGenerico, IExpertoRepository repositoryExperto, 
            ICuestionarioRepository repositoryCuestionario, ValidateUser validate)
        {
            _repositoryGenerico = repositoryGenerico;
            _repositoryExperto = repositoryExperto;
            _repositoryCuestionario = repositoryCuestionario;
            _validate = validate;
        }

        // POST api/login/experto
        [HttpPost]
        [AllowAnonymous]
        [Route("experto")]
        public IActionResult PostExperto([FromBody] Experto experto)
        {
            if (experto == null || string.IsNullOrEmpty(experto.Usuario) || string.IsNullOrEmpty(experto.Contrasena))
            {
                Log.Error("LoginExperto: Invalid parameters");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }

            try
            {
                Experto e = _validate.Validate(experto, _repositoryExperto);

                Log.Info("LoginExperto: " + e.Usuario.ToUpper(CultureInfo.InvariantCulture));
                return Ok(e);
            }
            catch (UnauthorizedAccessException e)
            {
                Log.Error("LoginExperto: " + experto.Usuario.ToUpper(CultureInfo.InvariantCulture) + " unauthorized -> " + e.Message);
                return StatusCode(StatusCodes.Status401Unauthorized, new { e.Message });
            }
            catch (Exception e)
            {
                Log.Error("LoginExperto: " + experto.Usuario.ToUpper(CultureInfo.InvariantCulture) + " -> " + e.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST api/login/generico
        [HttpPost]
        [AllowAnonymous]
        [Route("generico")]
        public async Task<IActionResult> PostGenerico([FromBody] Generico generico)
        {
            if (generico == null || string.IsNullOrEmpty(generico.Usuario)
                || string.IsNullOrEmpty(generico.Contrasena) || generico.CuestionarioEscogidoID <= 0)
            {
                Log.Error("LoginGenerico: Invalid parameters");
                return BadRequest(ErrorMsg.getMsg(eError.InvalidParam));
            }

            try
            {
                Generico g = await _validate.Validate(generico, _repositoryGenerico, _repositoryCuestionario);

                Log.Info("LoginGenerico: " + g.Usuario.ToUpper(CultureInfo.InvariantCulture));
                return Ok(g);
            }
            catch (UnauthorizedAccessException e)
            {
                Log.Error("LoginGenerico: " + generico.Usuario.ToUpper(CultureInfo.InvariantCulture) + " unauthorized -> " + e.Message);
                return StatusCode(StatusCodes.Status401Unauthorized, new { e.Message });
            }
            catch (Exception e)
            {
                Log.Error("LoginGenerico: " + generico.Usuario.ToUpper(CultureInfo.InvariantCulture) + " -> " + e.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
