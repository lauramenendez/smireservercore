namespace serv_smire.Models.Cuestionarios
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Usuario;

    /// <summary>
    /// Esta clase representa un cuestionario del sistema
    /// </summary>
    [Table("Cuestionario")]
    public class Cuestionario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cuestionario()
        {
            Modelos = new HashSet<Modelo>();
        }

        public long ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Titulo { get; set; }

        [StringLength(int.MaxValue)]
        public string Instrucciones { get; set; }

        public DateTime FechaCreacion { get; set; }

        public DateTime FechaModificacion { get; set; }

        public bool Activo { get; set; }

        [NotMapped]
        public IDictionary<long, string> Respuestas { get; set; }

        public bool RespuestasDescargadas { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? NumeroRespuestas { get; set; }

        [NotMapped]
        public Modelo ModeloEscogido { get; set; }

        public long? Generico_ID { get; set; }

        [NotMapped]
        public string GenericoUsername { get; set; }

        private Generico generico;
        public virtual Generico Generico {
            get {
                if (generico != null)
                    generico.Cuestionarios = null;
                return generico;
            }
            set
            {
                generico = value;
            }
        }

        public long? Experto_ID { get; set; }

        private Experto experto;
        public virtual Experto Experto
        {
            get
            {
                if (experto != null)
                    experto.Cuestionarios = null;
                return experto;
            }
            set
            {
                experto = value;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Modelo> Modelos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Instruccion> ArchivosInstrucciones { get; set; }
    }
}
