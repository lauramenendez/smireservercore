namespace serv_smire.Models.Cuestionarios
{
    using Helper.RandomUtil;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Esta clase representa un bloque de preguntas de un cuestionario dentro de un modelo
    /// </summary>
    [Table("Bloque")]
    public class Bloque : ICloneable
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Bloque()
        {
            Preguntas = new HashSet<Pregunta>();
        }

        public long ID { get; set; }

        [Required]
        [StringLength(int.MaxValue)]
        public string Nombre { get; set; }

        public int Posicion { get; set; }

        public long? Modelo_ID { get; set; }

        public virtual Modelo Modelo { get; set; }

        [NotMapped]
        public bool Deleted { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pregunta> Preguntas { get; set; }

        /// <summary>
        /// Se encarga de cambiar las posiciones de las preguntas de forma aleatoria
        /// </summary>
        /// <param name="nuevo"></param>
        public void ConmutarPreguntas(Modelo nuevo)
        {
            int i = 0;
            int[] preguntasPos = new Random().PosicionesAleatorias(Preguntas.Count);

            foreach (Pregunta pregunta in Preguntas)
            {
                pregunta.Posicion = nuevo.Orden_Preguntas ? preguntasPos[i] : pregunta.Posicion;
                pregunta.ConmutarTipos(nuevo);
                i++;
            }
        }

        /// <summary>
        /// Clona el bloque de preguntas
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new Bloque
            {
                Nombre = Nombre,
                Posicion = Posicion,
                Modelo_ID = Modelo_ID,
                Preguntas = Preguntas.Select(p => p.Clone()).Cast<Pregunta>().ToList()
            };
        }

        /// <summary>
        /// Ordena las preguntas de un bloque por su posici�n
        /// </summary>
        public void Ordenar()
        {
            Preguntas = Preguntas.OrderBy(p => p.Posicion).ToList();

            foreach (Pregunta pregunta in Preguntas)
            {
                pregunta.Ordenar();
            }
        }
    }
}
