namespace serv_smire.Models.Cuestionarios
{
    using System;
    using Newtonsoft.Json;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using TiposRespuestas;

    /// <summary>
    /// Esta clase representa las opciones disponibles para las preguntas de tipo:
    /// �nica, m�ltiple, orden y likert
    /// </summary>
    [Table("Etiqueta")]
    public class Etiqueta : ICloneable
    {
        public long ID { get; set; }

        [StringLength(50)]
        public string Nombre { get; set; }

        public int Posicion { get; set; }

        public double? Valor { get; set; }

        public long? TipoRespuesta_ID { get; set; }

        [NotMapped]
        public bool Deleted { get; set; }

        [JsonIgnore]
        public virtual TipoRespuesta TipoRespuesta { get; set; }

        /// <summary>
        /// Clona la etiqueta
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new Etiqueta()
            {
                Nombre = Nombre,
                Posicion = Posicion,
                Valor = Valor,
                TipoRespuesta_ID = TipoRespuesta_ID
            };
        }
    }
}
