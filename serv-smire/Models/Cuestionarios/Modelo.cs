namespace serv_smire.Models.Cuestionarios
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Helper.RandomUtil;
    using System.Linq;

    /// <summary>
    /// Clase que representa un modelo de un cuestionario
    /// </summary>
    [Table("Modelo")]
    public class Modelo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Modelo()
        {
            Bloques = new HashSet<Bloque>();
        }

        public long ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        /// <value>Indica si el orden de los bloques es aleatorio</value>
        public bool Orden_Bloques { get; set; }

        /// <value>Indica si el orden de las preguntas es aleatorio</value>
        public bool Orden_Preguntas { get; set; }

        /// <value>Indica si el orden de los tipos es aleatorio</value>
        public bool Orden_Tipos { get; set; }

        public long? Cuestionario_ID { get; set; }

        public virtual Cuestionario Cuestionario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bloque> Bloques { get; set; }

        /// <summary>
        /// Se encarga de cambiar las posiciones de los bloques de forma aleatoria
        /// </summary>
        /// <param name="original"></param>
        public void ConmutarBloques(Modelo original)
        {
            int i = 0;
            int[] bloquesPos = new Random().PosicionesAleatorias(original.Bloques.Count);

            foreach (Bloque bloque in original.Bloques)
            {
                var b = bloque.Clone() as Bloque;
                b.Posicion = Orden_Bloques ? bloquesPos[i] : bloque.Posicion;
                b.ConmutarPreguntas(this);

                Bloques.Add(b);
                i++;
            }

            Ordenar();
        }

        /// <summary>
        /// Ordena los bloques por su posici�n
        /// </summary>
        public void Ordenar()
        {             
            Bloques = Bloques.OrderBy(b => b.Posicion).ToList();

            foreach (Bloque bloque in Bloques)
            {
                bloque.Ordenar();
            }
        }
    }
}
