namespace serv_smire.Models.Cuestionarios
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using TiposRespuestas;
    using System.Linq;
    using Helper.RandomUtil;

    /// <summary>
    /// Clase que representa una pregunta del cuestionario
    /// </summary>
    [Table("Pregunta")]
    public class Pregunta : ICloneable
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Pregunta()
        {
            TiposRespuestas = new HashSet<TipoRespuesta>();
        }

        public long ID { get; set; }

        [Required]
        [StringLength(int.MaxValue)]
        public string Enunciado { get; set; }

        public int Posicion { get; set; }

        public long? Bloque_ID { get; set; }

        public virtual Bloque Bloque { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TipoRespuesta> TiposRespuestas { get; set; }

        [NotMapped]
        public string NombreBloque { get; set; }

        /// <summary>
        /// Se encarga de cambiar las posiciones de los tipos de respuesta de forma aleatoria
        /// </summary>
        /// <param name="nuevo"></param>
        public void ConmutarTipos(Modelo nuevo)
        {
            int i = 0;
            int[] tiposPos = new Random().PosicionesAleatorias(TiposRespuestas.Count);

            foreach (TipoRespuesta tipo in TiposRespuestas)
            {
                tipo.Posicion = nuevo.Orden_Tipos ? tiposPos[i] : tipo.Posicion;
                tipo.ConmutarOpciones(nuevo);
                i++;
            }
        }

        /// <summary>
        /// Clona la pregunta
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new Pregunta
            {
                Enunciado = Enunciado,
                Posicion = Posicion,
                Bloque_ID = Bloque_ID,
                TiposRespuestas = TiposRespuestas.Select(t => t.Clone()).Cast<TipoRespuesta>().ToList()
            };
        }

        /// <summary>
        /// Ordena los tipos de respuesta de una pregunta por su posici�n
        /// </summary>
        public void Ordenar()
        {
            TiposRespuestas = TiposRespuestas.OrderBy(tr => tr.Posicion).ToList();

            foreach (TipoRespuesta tipo in TiposRespuestas)
            {
                tipo.Ordenar();
            }
        }
    }
}
