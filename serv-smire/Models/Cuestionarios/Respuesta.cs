namespace serv_smire.Models.Cuestionarios
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using TiposRespuestas;

    /// <summary>
    /// Clase que representa una respuesta a un cuestionario (m�s bien a 
    /// un tipo de respuesta que pertenece a un cuestionario)
    /// </summary>
    [Table("Respuesta")]
    public class Respuesta
    {
        /// <value>Identificador del usuario</value>
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string Identificador { get; set; }

        /// <value>Valor de la respuesta</value>
        [StringLength(int.MaxValue)]
        public string Valor { get; set; }

        public DateTime FechaRespuesta { get; set; } = DateTime.Now;

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long TipoRespuesta_ID { get; set; }

        public virtual TipoRespuesta TipoRespuesta { get; set; }
    }
}
