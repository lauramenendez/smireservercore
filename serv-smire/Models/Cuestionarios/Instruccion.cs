namespace serv_smire.Models.Cuestionarios
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Esta clase representa un fichero de instrucción de un cuestionario
    /// </summary>
    [Table("Instruccion")]
    public class Instruccion
    {
        public long ID { get; set; }

        [Required]
        [StringLength(int.MaxValue)]
        public string Filename { get; set; }

        [Required]
        public byte[] Filedata { get; set; }

        [StringLength(50)]
        public string Filetype { get; set; }

        public long Cuestionario_ID { get; set; }

        public virtual Cuestionario Cuestionario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Instruccion() { }

        public Instruccion(string fileName, byte[] file, long cuestionario_ID, string filetype)
        {
            Filename = fileName;
            Filedata = file;
            Cuestionario_ID = cuestionario_ID;
            Filetype = filetype;
        }
    }
}
