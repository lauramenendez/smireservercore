﻿namespace serv_smire.Models.Cuestionarios.TiposRespuestas
{
    /// <summary>
    /// Interfaz que identifica los tipos de respuesta
    /// </summary>
    public interface ITipoRespuesta
    {
        /// <summary>
        /// Saber si un tipo es de respuesta obligatoria o no
        /// </summary>
        /// <returns></returns>
        bool IsObligatoria();

        /// <summary>
        /// Comprueba si el formato de la respuesta es correcto según el tipo
        /// </summary>
        /// <param name="respuesta"></param>
        /// <returns></returns>
        bool CheckFormato(string respuesta);

        /// <summary>
        /// Se encarga de cambiar las posiciones de las etiquetas (opciones) si tiene de forma aleatoria
        /// </summary>
        /// <param name="nuevo"></param>
        void ConmutarOpciones(Modelo nuevo);

        /// <summary>
        /// Devuelve el HTML que representa ese tipo para poder exportar el 
        /// cuestionario a PDF
        /// </summary>
        /// <returns></returns>
        string GenerateHTML();

        /// <summary>
        /// Devuelve el nombre del tipo en concreto
        /// </summary>
        /// <returns></returns>
        string GetNombreTipo();
    }
}
