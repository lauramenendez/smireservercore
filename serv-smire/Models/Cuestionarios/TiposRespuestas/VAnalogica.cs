﻿using System;
using serv_smire.Helper.Enumerators;

namespace serv_smire.Models.Cuestionarios.TiposRespuestas
{
    public class VAnalogica : TipoRespuesta
    {
        public override string NombreTipo { get; set; } = ETiposRespuesta.VAnalogica;

        public override bool CheckFormatoEspecifico(string respuesta)
        {
            String[] partes = respuesta.Split(';');
            double valorNumerico;
            if (partes.Length == 1 && Double.TryParse(partes[0], out valorNumerico))
            {
                return true;
            }
            return false;
        }

        public override string GenerateHTML()
        {
            string source =
                @"<form>
                    <input type=""number""/>
                </form>";
            return source;
        }

        public override object Clone()
        {
            return new VAnalogica
            {
                ValorDefecto = ValorDefecto,
                Posicion = Posicion,
                Obligatoria = Obligatoria,
                Pregunta_ID = Pregunta_ID,
                NombreTipo = NombreTipo,
                Minimo = Minimo,
                Maximo = Maximo,
                Escala = Escala
            };
        }
    }
}