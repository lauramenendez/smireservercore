﻿using serv_smire.Helper.Enumerators;

namespace serv_smire.Models.Cuestionarios.TiposRespuestas
{
    public class Texto : TipoRespuesta
    {
        public override string NombreTipo { get; set; } = ETiposRespuesta.Texto;

        public override bool CheckFormatoEspecifico(string respuesta)
        {
            return true;
        }

        public override string GenerateHTML()
        {
            string source =
                @"<form>
                    <textarea rows=""8"" cols=""150""></textarea>
                </form>";
            return source;
        }

        public override object Clone()
        {
            return new Texto
            {
                Posicion = Posicion,
                Obligatoria = Obligatoria,
                Pregunta_ID = Pregunta_ID,
                NombreTipo = NombreTipo
            };
        }
    }
}