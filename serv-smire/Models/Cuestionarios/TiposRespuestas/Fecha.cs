﻿using System;
using System.Globalization;
using serv_smire.Helper.Enumerators;

namespace serv_smire.Models.Cuestionarios.TiposRespuestas
{
    public class Fecha : TipoRespuesta
    {
        public override string NombreTipo { get; set; } = ETiposRespuesta.Fecha;

        /// <summary>
        /// La cadena debe tener formato fecha 
        /// </summary>
        /// <param name="respuesta"></param>
        /// <returns></returns>
        public override bool CheckFormatoEspecifico(string respuesta)
        {
            DateTime dt;
            return DateTime.TryParseExact(respuesta, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
        }

        public override string GenerateHTML()
        {
            string source =
                @"<form>
                    <input type=""number""/>
                </form>";
            return source;
        }

        public override object Clone()
        {
            return new Fecha
            {
                Posicion = Posicion,
                Obligatoria = Obligatoria,
                Pregunta_ID = Pregunta_ID,
                NombreTipo = NombreTipo
            };
        }
  
    }
}