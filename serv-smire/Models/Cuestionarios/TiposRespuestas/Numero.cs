﻿using System;
using System.Linq;
using serv_smire.Helper.Enumerators;

namespace serv_smire.Models.Cuestionarios.TiposRespuestas
{
    public class Numero : TipoRespuesta
    {
        public override string NombreTipo { get; set; } = ETiposRespuesta.Numero;

        public int? Decimales { get; set; }

        public override bool CheckFormatoEspecifico(string respuesta)
        {
            double valorNumerico;
            if (Double.TryParse(respuesta, out valorNumerico))
            {
                return true;
            }
            return false;
        }

        public override string GenerateHTML()
        {
            string source =
                @"<form>
                    <input type=""number""/>
                </form>";
            return source;
        }

        public override object Clone()
        {
            return new Numero
            {
                Posicion = Posicion,
                Obligatoria = Obligatoria,
                Escala = Escala,
                Minimo = Minimo,
                Maximo = Maximo,
                Decimales = Decimales,
                Pregunta_ID = Pregunta_ID,
                NombreTipo = NombreTipo,
                Etiquetas = Etiquetas.Select(e => e.Clone()).Cast<Etiqueta>().ToList()
            };
        }
    }
}