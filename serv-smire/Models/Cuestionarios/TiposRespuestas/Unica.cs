﻿using HandlebarsDotNet;
using System.Linq;
using serv_smire.Helper.Enumerators;

namespace serv_smire.Models.Cuestionarios.TiposRespuestas
{
    public class Unica : TipoRespuesta
    {
        public override string NombreTipo { get; set; } = ETiposRespuesta.Unica;

        /// <summary>
        /// La opcion escogida debe estar entre las respuestas posibles
        /// </summary>
        /// <param name="respuesta"></param>
        /// <returns></returns>
        public override bool CheckFormatoEspecifico(string respuesta)
        {
            return ContieneEtiqueta(respuesta);
        }

        public override string GenerateHTML()
        {
            string source =
                @"<div class=""entry"">
                    <form class=""etiquetas"">
                        {{#each etiquetas}}
                            <input type=""radio"">{{nombre}}</input><br/>
                        {{/each}}
                    </form>
                </div>";

            var template = Handlebars.Compile(source);

            var etiquetasData = new object[Etiquetas.Count];
            int i = 0;
            foreach (Etiqueta etiqueta in Etiquetas)
            {
                etiquetasData[i] = new { nombre = etiqueta.Nombre };
                i++;
            }
            return template(new { etiquetas = etiquetasData });
        }

        public override object Clone()
        {
            return new Unica
            { 
                Posicion = Posicion,
                Pregunta_ID = Pregunta_ID,
                NombreTipo = NombreTipo,
                Obligatoria = Obligatoria,
                Etiquetas = Etiquetas.Select(e => e.Clone()).Cast<Etiqueta>().ToList()
            };
        }
    }
}