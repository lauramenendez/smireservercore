﻿using HandlebarsDotNet;
using System;
using System.ComponentModel.DataAnnotations;
using serv_smire.Helper.Enumerators;
using System.ComponentModel.DataAnnotations.Schema;
using serv_smire.Helper.RandomUtil;

namespace serv_smire.Models.Cuestionarios.TiposRespuestas
{
    public class FRS : TipoRespuesta
    {
        public override string NombreTipo { get; set; } = ETiposRespuesta.FRS;

        [StringLength(50)]
        public string TipoFRS { get; set; }

        [NotMapped]
        public bool Invertido { get; set; }

        /// <summary>
        /// El formato debe ser 4 numeros separados por ; 
        /// </summary>
        /// <param name="respuesta"></param>
        /// <returns></returns>
        public override bool CheckFormatoEspecifico(string respuesta)
        {
            String[] partes = respuesta.Split(';');
            if (partes.Length == 4)
            {
                double valorNumerico;
                for (int i = 0; i < partes.Length; i++)
                {
                    if (!Double.TryParse(partes[i], out valorNumerico))
                    {
                        return false;
                    }
                }

                return true;
            }
            return false;
        }

        public override void ConmutarOpciones(Modelo nuevo)
        {
            Invertido = true;
        }

        public override string GenerateHTML()
        {
            string ident = RndUtil.GetAlphanumericString(20);
            string source =
                @"
                <div style=""text-align: center; width: 100%;"">
                    <p>VALORACIONES PLENAMENTE COMPATIBLES</p>
                </div>
                <canvas id=""{{trapecio}}"" width=""1300"" height=""200""></canvas>
                <div style=""text-align: center; width: 100%;"">
                    <p>VALORACIONES ALGO COMPATIBLES</p>
                </div>
                <script>
                    var inicioX = 10;
                    var finX = 1290;
                    var inicioY = 10;
                    var finY = 190;

                    var c = document.getElementById(""{{trapecio}}"");
                    var ctx = c.getContext(""2d"");

                    ctx.font = ""15px Times New Roman"";
                    
                    ctx.moveTo(inicioX, finY-30);
                    ctx.lineTo(finX, finY-30);
                    ctx.stroke();

                    ctx.moveTo(inicioX, inicioY);
                    ctx.lineTo(finX, inicioY);
                    ctx.stroke();

                    var escala = [0, 20, 40, 60, 80, 100];
                    var rango = {{maximo}} - {{minimo}};
                    var intervalo = (finX - inicioX)/(escala.length-1);

                    var x = 10; 
                    for(var i=0; i<escala.length; i++) {
                        ctx.moveTo(x, inicioY);
                        ctx.lineTo(x, finY-20);
                        ctx.stroke(); 
                        
                        var valor = rango*(escala[i]/100);
                        var cifras = valor.toString().length;
    
                        ctx.fillText(valor+"""", x-(4*cifras), finY-5); 
                        
                        x+=intervalo;
                    }

                    var x1 = 10 + (intervalo/2); 
                    for(var i=0; i<escala.length-1; i++) {
                        ctx.beginPath();
                        ctx.moveTo(x1, finY-20);
                        ctx.lineTo(x1, finY-15);
                        ctx.stroke();

                        x1+=intervalo;
                    }

                    var x2 = 10 + (intervalo/4); 
                    for(var i=0; i<(escala.length-1)*2; i++) {
                        ctx.beginPath();
                        ctx.moveTo(x2, finY-20);
                        ctx.lineTo(x2, finY-18);
                        ctx.stroke();
            
                        x2+=intervalo/2;
                    }
                </script>";

            var template = Handlebars.Compile(source);
            return template(new { minimo = Minimo, maximo = Maximo, trapecio = ident });
        }

        public override string GetNombreTipo()
        {
            return String.Format("{0}-{1}", NombreTipo, TipoFRS);
        }

        public override object Clone()
        {
            return new FRS
            { 
                ValorDefecto = ValorDefecto,
                Posicion = Posicion,
                Obligatoria = Obligatoria,
                Pregunta_ID = Pregunta_ID,
                NombreTipo = NombreTipo,
                Minimo = Minimo,
                Maximo = Maximo,
                Escala = Escala,
                TipoFRS = TipoFRS
            };
        }
    }
}