﻿using HandlebarsDotNet;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using serv_smire.Helper.Enumerators;

namespace serv_smire.Models.Cuestionarios.TiposRespuestas
{
    public class Likert : TipoRespuesta
    {
        public override string NombreTipo { get; set; } = ETiposRespuesta.Likert;

        [StringLength(50)]
        public string TipoLikert { get; set; }

        public int? NRespuestas { get; set; }

        /// <summary>
        /// Si es de tipo numerica, comprobamos que la respuesta es un numero
        /// Si es de tipo textual, comprobamos que la respuesta coincide con alguna de las posibles respuestas
        /// </summary>
        /// <param name="respuesta"></param>
        /// <returns></returns>
        public override bool CheckFormatoEspecifico(string respuesta)
        {
            if (TipoLikert.Equals(ETiposLikert.Numero))
            {
                double valorNumerico;
                if (Double.TryParse(respuesta, out valorNumerico))
                {
                    return true;
                }
            }
            else if (TipoLikert.Equals(ETiposLikert.Texto))
            {
                return ContieneEtiqueta(respuesta);
            }

            return false;
        }

        public override void ConmutarOpciones(Modelo nuevo)
        {
            int i = Etiquetas.Count;
            foreach (Etiqueta etiqueta in Etiquetas)
            {
                etiqueta.Posicion = i;
                i--;
            }
        }

        public override string GenerateHTML()
        {
            string source =
                @"
                <style>
                    .etiquetas {
                        text-align: justify;
                    }
                    .etiquetas label {
                         display: inline-block;
                    }
                    .etiquetas:after {
                      content: '';
                      width: 100%; 
                      display: inline-block;
                    }
                    label {
                        text-align: center;
                    }
                </style>
                <div class=""etiquetas"">
                    {{#each etiquetas}}
                        <label>
                            <input type=""radio""/><br/>
                            {{nombre}}
                        </label>
                    {{/each}}
                </div>";

            var template = Handlebars.Compile(source);

            var etiquetasData = new object[Etiquetas.Count];
            int i = 0;
            foreach (Etiqueta etiqueta in Etiquetas)
            {
                if (TipoLikert.Equals(ETiposLikert.Texto))
                {
                    etiquetasData[i] = new { nombre = etiqueta.Nombre };
                }
                else
                {
                    etiquetasData[i] = new { nombre = etiqueta.Valor };
                }
                i++;
            }
            return template(new { etiquetas = etiquetasData });
        }

        public override string GetNombreTipo()
        {
            return String.Format("{0}-{1}", NombreTipo, TipoLikert);
        }

        public override object Clone()
        {
            return new Likert
            {
                Posicion = Posicion,
                Obligatoria = Obligatoria,
                Pregunta_ID = Pregunta_ID,
                NombreTipo = NombreTipo,
                TipoLikert = TipoLikert,
                NRespuestas = NRespuestas,
                Minimo = Minimo,
                Maximo = Maximo,
                Escala = Escala,
                Etiquetas = Etiquetas.Select(e => e.Clone()).Cast<Etiqueta>().ToList()
            };
        }
    }
}