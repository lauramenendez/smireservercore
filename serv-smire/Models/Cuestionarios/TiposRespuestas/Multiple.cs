﻿using HandlebarsDotNet;
using System;
using System.Linq;
using serv_smire.Helper.Enumerators;

namespace serv_smire.Models.Cuestionarios.TiposRespuestas
{
    public class Multiple : TipoRespuesta
    {
        public override string NombreTipo { get; set; } = ETiposRespuesta.Multiple;

        /// <summary>
        /// Todas las opciones escogidas deben estar entre las respuestas posibles
        /// </summary>
        /// <param name="respuesta"></param>
        /// <returns></returns>
        public override bool CheckFormatoEspecifico(string respuesta)
        {
            String[] partes = respuesta.Split(';');
            if (partes.Length >= 1)
            {
                for(int i=0; i<partes.Length; i++)
                {
                    if (!ContieneEtiqueta(partes[i]))
                    {
                        return false;
                    }
                }

                return true;
            }
          
            return false;
        }

        public override string GenerateHTML()
        {
            string source =
                @"<div class=""entry"">
                    <form class=""etiquetas"">
                        {{#each etiquetas}}
                            <input type=""checkbox"">{{nombre}}</input><br/>
                        {{/each}}
                    </form>
                </div>";

            var template = Handlebars.Compile(source);

            var etiquetasData = new object[Etiquetas.Count];
            int i = 0;
            foreach (Etiqueta etiqueta in Etiquetas)
            {
                etiquetasData[i] = new { nombre = etiqueta.Nombre };
                i++;
            }
            return template(new { etiquetas = etiquetasData });
        }

        public override object Clone()
        {
            return new Multiple
            {
                Posicion = Posicion,
                Obligatoria = Obligatoria,
                Pregunta_ID = Pregunta_ID,
                NombreTipo = NombreTipo,
                Etiquetas = Etiquetas.Select(e => e.Clone()).Cast<Etiqueta>().ToList()
            };
        }
    }
}