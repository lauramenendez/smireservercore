namespace serv_smire.Models.Cuestionarios.TiposRespuestas
{
    using Helper.RandomUtil;
    using Helper.JSON;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using serv_smire.Helper.ErrorHandler;

    /// <summary>
    /// Representa de forma abstracta un tipo de respuesta
    /// </summary>
    [Table("TipoRespuesta")]
    [JsonConverter(typeof(JsonTipoRespuesta))]
    public abstract class TipoRespuesta : ITipoRespuesta, ICloneable
    {
        /// <summary>
        /// Datos para generar el c�digo alfanum�rico
        /// </summary>
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        const int length = 20;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        protected TipoRespuesta()
        {
            Etiquetas = new HashSet<Etiqueta>();
            Respuestas = new HashSet<Respuesta>();
        }

        public long ID { get; set; }
        
        [StringLength(50)]
        public string ValorDefecto { get; set; }

        public int Posicion { get; set; }

        public double? Escala { get; set; }

        public double? Minimo { get; set; }

        public double? Maximo { get; set; }

        public bool Obligatoria { get; set; }

        public long? Pregunta_ID { get; set; }

        public virtual Pregunta Pregunta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Etiqueta> Etiquetas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Respuesta> Respuestas { get; set; }
        
        public abstract string NombreTipo { get; set; }

        [NotMapped]
        public bool Deleted { get; set; }

        /// <summary>
        /// La respuesta puede estar vacia en cualquier caso
        /// </summary>
        /// <param name="respuesta"></param>
        /// <returns></returns>
        public bool CheckFormato(string respuesta)
        {
            if(string.IsNullOrEmpty(respuesta))
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidRespuesta).ToString());
            }

            if (respuesta.Equals("N/A"))
            {
                return true;
            }

            return CheckFormatoEspecifico(respuesta);
        }

        public abstract bool CheckFormatoEspecifico(string respuesta);

        public virtual void ConmutarOpciones(Modelo nuevo)
        {
            int i = 0;
            int[] opcionesPos = new Random().PosicionesAleatorias(Etiquetas.Count);

            foreach (Etiqueta etiqueta in Etiquetas)
            {
                etiqueta.Posicion = nuevo.Orden_Tipos ? opcionesPos[i] : etiqueta.Posicion;
                i++;
            }
        }

        public virtual string GenerateHTML()
        {
            return "<p>Tipo no implementado</p>";
        }

        public virtual string GetNombreTipo()
        {
            return NombreTipo;
        }

        public abstract object Clone();

        public bool IsObligatoria()
        {
            return Obligatoria;
        }

        /// <summary>
        /// Ordena las etiquetas por su posici�n
        /// </summary>
        public void Ordenar()
        {
            Etiquetas = Etiquetas.OrderBy(e => e.Posicion).ToList();
        }

        /// <summary>
        /// M�todo para saber si una pregunta que tiene varias opciones
        /// contiene la que se le pasa como par�metro
        /// </summary>
        /// <param name="respuesta"></param>
        /// <returns></returns>
        protected bool ContieneEtiqueta(string respuesta)
        {
            foreach (Etiqueta etiqueta in Etiquetas)
            {
                if (etiqueta.Nombre.Equals(respuesta))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
