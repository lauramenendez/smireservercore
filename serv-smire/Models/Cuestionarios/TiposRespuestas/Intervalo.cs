﻿using System;
using serv_smire.Helper.Enumerators;

namespace serv_smire.Models.Cuestionarios.TiposRespuestas
{
    public class Intervalo : TipoRespuesta
    {
        public override string NombreTipo { get; set; } = ETiposRespuesta.Intervalo;

        /// <summary>
        /// El formato debe ser 2 numeros separados por ;
        /// </summary>
        /// <param name="respuesta"></param>
        /// <returns></returns>
        public override bool CheckFormatoEspecifico(string respuesta)
        {
            String[] partes = respuesta.Split(';');
            if (partes.Length == 2)
            {
                double valorNumerico;
                for (int i = 0; i < partes.Length; i++)
                {
                    if (!Double.TryParse(partes[i], out valorNumerico))
                    {
                        return false;
                    }
                }

                return true;
            }
            return false;
        }

        public override string GenerateHTML()
        {
            string source =
                @"<form>
                    Entre <input type=""number""/> y <input type=""number""/>
                </form>";
            return source;
        }

        public override object Clone()
        {
            return new Intervalo
            { 
                ValorDefecto = ValorDefecto,
                Posicion = Posicion,
                Obligatoria = Obligatoria,
                Pregunta_ID = Pregunta_ID,
                NombreTipo = NombreTipo,
                Minimo = Minimo,
                Maximo = Maximo,
                Escala = Escala
            };
        }
    }
}