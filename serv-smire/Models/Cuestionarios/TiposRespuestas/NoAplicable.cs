﻿using serv_smire.Helper.Enumerators;

namespace serv_smire.Models.Cuestionarios.TiposRespuestas
{
    public class NoAplicable : TipoRespuesta
    {
        public override string NombreTipo { get; set; } = ETiposRespuesta.NoAplicable;

        public override bool CheckFormatoEspecifico(string respuesta)
        {
            return respuesta.Equals("true") || respuesta.Equals("false");
        }

        public override string GenerateHTML()
        {
            string source =
                @"<form>
                    <input type=""radio"">No aplicable</input>
                </form>";
            return source;
        }

        public override object Clone()
        {
            return new NoAplicable
            {
                Posicion = Posicion,
                Obligatoria = Obligatoria,
                Pregunta_ID = Pregunta_ID,
                NombreTipo = NombreTipo
            };
        }
    }
}