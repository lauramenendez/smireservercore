﻿using HandlebarsDotNet;
using System;
using System.Linq;
using serv_smire.Helper.Enumerators;

namespace serv_smire.Models.Cuestionarios.TiposRespuestas
{
    public class Orden : TipoRespuesta
    {
        public override string NombreTipo { get; set; } = ETiposRespuesta.Orden;

        /// <summary>
        /// Todas las opciones escogidas deben estar entre las respuestas posibles
        /// </summary>
        /// <param name="respuesta"></param>
        /// <returns></returns>
        public override bool CheckFormatoEspecifico(string respuesta)
        {
            String[] partes = respuesta.Split(';');
            if (partes.Length == Etiquetas.Count)
            {
                for (int i = 0; i < partes.Length; i++)
                {
                    if (!ContieneEtiqueta(partes[i]))
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public override string GenerateHTML()
        {
            string source =
                @"<div class=""entry"">
                    {{#each etiquetas}}
                        <p>.... {{nombre}}</p>
                    {{/each}}
                </div>";

            var template = Handlebars.Compile(source);

            var etiquetasData = new object[Etiquetas.Count];
            int i = 0;
            foreach (Etiqueta etiqueta in Etiquetas)
            {
                etiquetasData[i] = new { nombre = etiqueta.Nombre };
                i++;
            }
            return template(new { etiquetas = etiquetasData });
        }

        public override object Clone()
        {
            return new Orden
            {
                Posicion = Posicion,
                Obligatoria = Obligatoria,
                Pregunta_ID = Pregunta_ID,
                NombreTipo = NombreTipo,
                Etiquetas = Etiquetas.Select(e => e.Clone()).Cast<Etiqueta>().ToList()
            };
        }
    }
}