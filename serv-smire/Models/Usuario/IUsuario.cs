﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serv_smire.Models.Usuario
{
    /// <summary>
    /// Interfaz que representa un usuario,
    /// al que se le puede solicitar su rol
    /// </summary>
    public interface IUsuario
    {
        string GetRole();
    }
}
