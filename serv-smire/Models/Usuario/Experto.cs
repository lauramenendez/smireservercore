namespace serv_smire.Models.Usuario
{
    using Cuestionarios;
    using Helper.Security;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using serv_smire.Helper.Enumerators;

    /// <summary>
    /// Clase que representa un usuario experto (administrador y experto) del sistema
    /// </summary>
    [Table("Experto")]
    public class Experto : IUsuario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Experto()
        {
            Genericos = new HashSet<Generico>();
            Cuestionarios = new HashSet<Cuestionario>();
        }

        public long ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(50)]
        public string Apellidos { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(50)]
        public string Departamento { get; set; }

        [Required]
        [StringLength(50)]
        public string Usuario { get; set; }

        [StringLength(50)]
        public string Contrasena { get; set; }

        [StringLength(10)]
        public string Codigo { get; set; }

        public bool EsAdmin { get; set; } = false;

        public bool Aceptado { get; set; } = false;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Generico> Genericos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cuestionario> Cuestionarios { get; set; }

        [NotMapped]
        public string Token { get; set; }

        [NotMapped]
        public string RepContrasena { get; set; }

        [NotMapped]
        public string NuevaContrasena { get; set; }

        /// <summary>
        /// Devuelve el rol del usuario, puede ser experto o administrador
        /// </summary>
        /// <returns></returns>
        public string GetRole()
        {
            return EsAdmin ? ERoles.Admin : ERoles.Experto;
        }
    }
}
