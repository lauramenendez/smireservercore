namespace serv_smire.Models.Usuario
{
    using Cuestionarios;
    using Helper.Security;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using serv_smire.Helper.Enumerators;

    /// <summary>
    /// Representa un usuario gen�rico del sistema
    /// </summary>
    [Table("Generico")]
    public class Generico : IUsuario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Generico()
        {
            Cuestionarios = new HashSet<Cuestionario>();
        }

        public long ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Usuario { get; set; }

        [StringLength(50)]
        public string Contrasena { get; set; }

        public long? Experto_ID { get; set; }

        public virtual Experto Experto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cuestionario> Cuestionarios { get; set; }

        [NotMapped]
        public long CuestionarioEscogidoID { get; set; }

        [NotMapped]
        public Cuestionario CuestionarioEscogido { get; set; }

        [NotMapped]
        public string Identificador { get; set; }

        [NotMapped]
        public string Token { get; set; }

        [NotMapped]
        public string Creador { get; set; }

        /// <summary>
        /// Devuelve el rol del usuario (gen�rico)
        /// </summary>
        /// <returns></returns>
        public string GetRole()
        {
            return ERoles.Generico;
        }
    }
}
