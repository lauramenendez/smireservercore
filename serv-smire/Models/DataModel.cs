namespace serv_smire.Models
{

    using Cuestionarios;
    using Usuario;
    using Cuestionarios.TiposRespuestas;
    using Microsoft.EntityFrameworkCore;
    using serv_smire.Helper.Enumerators;

    public class DataModel : DbContext
    {
        public string ModeloOriginal { get; } = "modelo 0";

        public DataModel(DbContextOptions<DataModel> options) : base(options) { }

        public virtual DbSet<Bloque> Bloques { get; set; }
        public virtual DbSet<Cuestionario> Cuestionarios { get; set; }
        public virtual DbSet<Etiqueta> Etiquetas { get; set; }
        public virtual DbSet<Experto> Expertos { get; set; }
        public virtual DbSet<Generico> Genericos { get; set; }
        public virtual DbSet<Modelo> Modelos { get; set; }
        public virtual DbSet<Pregunta> Preguntas { get; set; }
        public virtual DbSet<Respuesta> Respuestas { get; set; }
        public virtual DbSet<TipoRespuesta> TipoRespuestas { get; set; }
        public virtual DbSet<Instruccion> Instruccion { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Cuestionarios
            modelBuilder.Entity<Cuestionario>()
                .Property(e => e.Titulo)
                .IsUnicode(false);

            modelBuilder.Entity<Cuestionario>()
                .Property(e => e.Instrucciones)
                .IsUnicode(false);

            modelBuilder.Entity<Cuestionario>()
                .HasMany(e => e.Modelos)
                .WithOne(e => e.Cuestionario)
                .HasForeignKey(e => e.Cuestionario_ID)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Cuestionario>()
                 .HasMany(e => e.ArchivosInstrucciones)
                 .WithOne(e => e.Cuestionario)
                 .HasForeignKey(e => e.Cuestionario_ID)
                 .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Modelo>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Modelo>()
                .HasMany(e => e.Bloques)
                .WithOne(e => e.Modelo)
                .HasForeignKey(e => e.Modelo_ID)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Bloque>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Bloque>()
                .HasMany(e => e.Preguntas)
                .WithOne(e => e.Bloque)
                .HasForeignKey(e => e.Bloque_ID)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Pregunta>()
               .Property(e => e.Enunciado)
               .IsUnicode(false);

            modelBuilder.Entity<Pregunta>()
                .HasMany(e => e.TiposRespuestas)
                .WithOne(e => e.Pregunta)
                .HasForeignKey(e => e.Pregunta_ID)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TipoRespuesta>()
               .Property(e => e.ValorDefecto)
               .IsUnicode(false);

            modelBuilder.Entity<TipoRespuesta>()
                .HasMany(e => e.Etiquetas)
                .WithOne(e => e.TipoRespuesta)
                .HasForeignKey(e => e.TipoRespuesta_ID)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TipoRespuesta>()
                .HasMany(e => e.Respuestas)
                .WithOne(e => e.TipoRespuesta)
                .HasForeignKey(e => e.TipoRespuesta_ID);

            modelBuilder.Entity<TipoRespuesta>()
                .HasDiscriminator<string>("NombreTipo")
                .HasValue<Desplegable>(ETiposRespuesta.Desplegable)
                .HasValue<Fecha>(ETiposRespuesta.Fecha)
                .HasValue<FRS>(ETiposRespuesta.FRS)
                .HasValue<Intervalo>(ETiposRespuesta.Intervalo)
                .HasValue<Likert>(ETiposRespuesta.Likert)
                .HasValue<Multiple>(ETiposRespuesta.Multiple)
                .HasValue<NoAplicable>(ETiposRespuesta.NoAplicable)
                .HasValue<Numero>(ETiposRespuesta.Numero)
                .HasValue<Orden>(ETiposRespuesta.Orden)
                .HasValue<Texto>(ETiposRespuesta.Texto)
                .HasValue<Unica>(ETiposRespuesta.Unica)
                .HasValue<VAnalogica>(ETiposRespuesta.VAnalogica);

            modelBuilder.Entity<Etiqueta>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Respuesta>()
                .Property(e => e.Identificador)
                .IsUnicode(false);

            modelBuilder.Entity<Respuesta>()
                .Property(e => e.Valor)
                .IsUnicode(false);

            modelBuilder.Entity<Respuesta>()
                .HasKey(r => new { r.Identificador, r.TipoRespuesta_ID });

            modelBuilder.Entity<Instruccion>()
                .Property(e => e.Filename)
                .IsUnicode(false);

            // Usuarios expertos
            modelBuilder.Entity<Experto>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Experto>()
                .Property(e => e.Departamento)
                .IsUnicode(false);

            modelBuilder.Entity<Experto>()
                 .HasMany(e => e.Cuestionarios)
                 .WithOne(e => e.Experto)
                 .HasForeignKey(e => e.Experto_ID);

            modelBuilder.Entity<Experto>()
                .HasMany(e => e.Genericos)
                .WithOne(e => e.Experto)
                .HasForeignKey(e => e.Experto_ID);

            // Usuarios genericos
            modelBuilder.Entity<Generico>()
                .Property(e => e.Usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Generico>()
                .Property(e => e.Contrasena)
                .IsUnicode(false);

            modelBuilder.Entity<Generico>()
                .HasMany(e => e.Cuestionarios)
                .WithOne(e => e.Generico)
                .HasForeignKey(e => e.Generico_ID);
        }
    }
}
