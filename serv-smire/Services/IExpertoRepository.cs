﻿using System;
using System.Collections.Generic;
using serv_smire.Models.Usuario;

namespace serv_smire.Services
{
    /// <summary>
    /// Clase que representa las operaciones a realizar contra la base de datos 
    /// para gestionar los usuarios expertos
    /// </summary>
    public interface IExpertoRepository : IDisposable
    {
        // ************ OPERACIONES GET ************

        /// <summary>
        /// Devuelve un usuario experto segun su ID
        /// </summary>
        /// <param name="id">ID del usuario</param>
        /// <returns></returns>
        Experto GetExperto(long id);

        /// <summary>
        /// Devuelve un usuario experto segun su nombre de usuario
        /// </summary>
        /// <param name="usuario">Nombre de usuario</param>
        /// <returns></returns>
        Experto GetExperto(string usuario);

        /// <summary>
        /// Devuelve un usuario experto segun su nombre de usuario e ID
        /// </summary>
        /// <param name="usuario">Nombre de usuario</param>
        /// <param name="id">ID del usuario</param>
        /// <returns></returns>
        Experto GetExperto(string usuario, long id);

        /// <summary>
        /// Devuelve todos los usuarios expertos
        /// </summary>
        /// <returns></returns>
        ICollection<Experto> GetExpertos();

        // ************ OPERACIONES CRUD ************

        /// <summary>
        /// Actualiza los datos de un usuario experto
        /// </summary>
        /// <param name="usuario">Datos usuario experto</param>
        void Update(Experto usuario);

        /// <summary>
        /// Actualiza la contraseña de un usuario experto
        /// </summary>
        /// <param name="usuario">Datos usuario experto</param>
        void UpdateContrasena(Experto usuario);

        /// <summary>
        /// Elimina los datos de un usuario experto
        /// </summary>
        /// <param name="expertoId">ID del usuario a eliminar</param>
        void Delete(long expertoId);

        /// <summary>
        /// Añade un nuevo usuario experto
        /// </summary>
        /// <param name="experto">Nuevos datos del usuario</param>
        void Add(Experto experto);

        // ************ OTRAS OPERACIONES ************

        /// <summary>
        /// Devuelve una lista con los correos de los administradores del sistema
        /// </summary>
        /// <returns></returns>
        ICollection<string> GetCorreosAdmin();

        /// <summary>
        /// Comprueba si el nombre de usuario esta disponible en el sistema
        /// </summary>
        /// <param name="username"></param>
        /// <returns>True si esta disponible, false si no</returns>
        bool CheckExistUsername(string username);

        // ************ AUXILIARES ************

        /// <summary>
        /// Comprueba si el usuario existe
        /// </summary>
        /// <param name="id">ID del usuario</param>
        /// <returns></returns>
        bool Exists(long id);

        /// <summary>
        /// Comprueba si el usuario existe excluyendose asi mismo
        /// </summary>
        /// <param name="user"></param>
        /// <param name="id">ID del usuario</param>
        /// <returns></returns>
        bool Exists(string user, long id);
    }
}
