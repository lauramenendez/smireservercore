﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using serv_smire.Models.Cuestionarios;

namespace serv_smire.Services
{
    /// <summary>
    /// Clase que representa las operaciones a realizar contra la base de datos 
    /// para gestionar los cuestionarios
    /// </summary>
    public interface ICuestionarioRepository : IDisposable
    {
        // ************ OPERACIONES GET ************

        /// <summary>
        /// Devuelve el cuestionario a traves del ID con todos los modelos
        /// </summary>
        /// <param name="id">ID del cuestionario</param>
        /// <returns></returns>
        Cuestionario GetCuestionario(long id);

        /// <summary>
        /// Devuelve una lista con todos los cuestionarios de la base de datos
        /// </summary>
        /// <returns></returns>
        ICollection<Cuestionario> GetCuestionarios();

        /// <summary>
        /// Devuelve una lista con todos los cuestionarios activos
        /// </summary>
        /// <returns></returns>
        Task<ICollection<Cuestionario>> GetCuestionariosActivos();

        /// <summary>
        /// Devuelve los cuestionarios de un usuario experto concreto
        /// </summary>
        /// <param name="idUser">ID usuario experto</param>
        /// <returns></returns>
        ICollection<Cuestionario> GetCuestionariosUsuario(long idUser);

        /// <summary>
        /// Devuelve el cuestionario a traves del ID y con un modelo aleatorio escogido
        /// </summary>
        /// <param name="idCuestionario">ID del cuestionario</param>
        /// <returns></returns>
        Task<Cuestionario> GetCuestionarioModelo(long idCuestionario);

        /// <summary>
        /// Devuelve el cuestionario a traves del ID con el modelo indicado por su ID
        /// </summary>
        /// <param name="idCuestionario">ID del cuestionario</param>
        /// <param name="idModelo">ID del modelo</param>
        /// <returns></returns>
        Cuestionario GetCuestionarioModelo(long idCuestionario, long idModelo);

        /// <summary>
        /// Devuelve el cuestionario a traves del ID, las respuestas que ya dio el usuario (si hay) y el modelo que habia respondido
        /// </summary>
        /// <param name="idCuestionario">ID del cuestionario</param>
        /// <param name="identificador">Identificador del usuario generico</param>
        /// <returns></returns>
        Task<Cuestionario> GetCuestionarioModelo(long idCuestionario, string identificador);

        // ************ OPERACIONES CRUD ************

        /// <summary>
        /// Actualiza el cuestionario dado
        /// </summary>
        /// <param name="cuestionario">Datos del cuestionario</param>
        Cuestionario Update(Cuestionario cuestionario);

        /// <summary>
        /// Añade el cuestionario dado
        /// </summary>
        /// <param name="cuestionario">Datos del cuestionario</param>
        Cuestionario Add(Cuestionario cuestionario);

        /// <summary>
        /// Elimina el cuestionario escogido junto con todos sus bloques y preguntas
        /// </summary>
        /// <param name="cuestionarioId"></param>
        void Delete(long cuestionarioId);

        // ************ OTRAS OPERACIONES ************

        /// <summary>
        /// Intercambia las posiciones de los bloques del cuestionario
        /// </summary>
        /// <param name="cuestionarioId">ID del cuestionario al que pertenecen</param>
        /// <param name="bloque1">ID del bloque 1 a actualizar</param>
        /// <param name="bloque2">ID del bloque 2 a actualizar</param>
        void UpdatePosiciones(long cuestionarioId, long bloque1, long bloque2);

        /// <summary>
        /// Devuelve las respuestas de un cuestionario
        /// </summary>
        /// <param name="cuestionarios"></param>
        /// <returns></returns>
        IDictionary<string, object> GetRespuestas(string[] cuestionarios);

        /// <summary>
        /// Devuelve el fichero (PDF) con el cuestionario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        byte[] ExportCuestionario(long id);

        // ************ AUXILIARES ************

        /// <summary>
        /// Comprueba si el cuestionario con ese ID existe
        /// </summary>
        /// <param name="id">ID del cuestionario</param>
        /// <returns></returns>
        bool Exists(long id);

        /// <summary>
        /// Comprueba si el cuestionario con ese titulo existe excluyendose asi mismo
        /// </summary>
        /// <param name="titulo">Titulo del cuestionario</param>
        /// <param name="id">ID del cuestionario</param>
        /// <returns></returns>
        bool Exists(string titulo, long id = 0);

        /// <summary>
        /// Comprueba si el bloque existe y pertenece al cuestionario
        /// </summary>
        /// <param name="cuestionarioId">ID del cuestionario</param>
        /// <param name="idBloque">ID del bloque</param>
        /// <returns></returns>
        bool BloqueExists(long cuestionarioId, long idBloque);
    }
}
