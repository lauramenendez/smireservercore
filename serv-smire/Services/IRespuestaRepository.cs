﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using serv_smire.Models.Cuestionarios;

namespace serv_smire.Services
{
    /// <summary>
    /// Clase que representa las operaciones a realizar contra la base de datos 
    /// para gestionar las respuestas
    /// </summary>
    public interface IRespuestaRepository : IDisposable
    {
        // ************ OPERACIONES GET ************

        /// <summary>
        /// Devuelve una lista de todas las respuestas hechas por un identificador (persona)
        /// </summary>
        /// <param name="identificador">Persona que respondio</param>
        /// <returns></returns>
        ICollection<Respuesta> GetRespuestas(string identificador);

        /// <summary>
        /// Devuelve una lista de todas las respuestas a un cuestionario
        /// </summary>
        /// <param name="username">Usuario experto que creo el cuestionario</param>
        /// <param name="cuestionario">Titulo del cuestionario</param>
        /// <returns></returns>
        ICollection<Respuesta> GetRespuestas(string username, string cuestionario);

        // ************ OPERACIONES CRUD ************

        /// <summary>
        /// Añade nuevas respuestas
        /// </summary>
        /// <param name="respuestas">Datos de la respuestas</param>
        Task Add(Respuesta[] respuestas);

        /// <summary>
        /// Elimina las respuestas por identificador
        /// </summary>
        /// <param name="identificador">Persona que respondio</param>
        void Delete(string identificador);
    }
}
