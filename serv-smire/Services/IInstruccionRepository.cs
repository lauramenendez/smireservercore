﻿using System;
using System.Collections.Generic;
using serv_smire.Models.Cuestionarios;

namespace serv_smire.Services
{
    /// <summary>
    /// Clase que representa las operaciones a realizar contra la base de datos 
    /// para gestionar los archivos de las instrucciones de un cuestionario
    /// </summary>
    public interface IInstruccionRepository : IDisposable
    {
        // ************ OPERACIONES GET ************

        /// <summary>
        /// Devuelve el archivo instruccion con ese ID
        /// </summary>
        /// <param name="id">ID de la instruccion</param>
        Instruccion GetInstruccion(long id);

        // ************ OPERACIONES CRUD ************

        /// <summary>
        /// Añade instrucciones al cuestionario
        /// </summary>
        /// <param name="instrucciones">Instrucciones</param>
        ICollection<Instruccion> Add(ICollection<Instruccion> instrucciones);

        /// <summary>
        /// Elimina todos los archivos de instrucciones asociados a un cuestionario
        /// </summary>
        /// <param name="cuestionarioId">ID del cuestionario</param>
        void Delete(long cuestionarioId);

        // ************ AUXILIARES ************

        /// <summary>
        /// Comprueba si la instruccion con ese ID existe
        /// </summary>
        /// <param name="id">ID de la instruccion</param>
        /// <returns></returns>
        bool Exists(long id);
    }
}
