﻿using System;
using System.Collections.Generic;
using serv_smire.Models.Cuestionarios;

namespace serv_smire.Services
{
    /// <summary>
    /// Clase que representa las operaciones a realizar contra la base de datos 
    /// para gestionar los modelos de un cuestionario
    /// </summary>
    public interface IModeloRepository : IDisposable
    {
        // ************ OPERACIONES GET ************

        /// <summary>
        /// Devuelve el modelo con el ID indicado
        /// </summary>
        /// <param name="id">Modelo a devolver</param>
        /// <returns></returns>
        Modelo GetModelo(long id);

        /// <summary>
        /// Devuelve todos los modelos de un cuestionario
        /// </summary>
        /// <param name="cuestionario">Titulo del cuestionario</param>
        /// <returns></returns>
        ICollection<Modelo> GetModelos(string cuestionario);

        // ************ OPERACIONES CRUD ************

        /// <summary>
        /// Añade un nuevo modelo
        /// </summary>
        /// <param name="modelo">Nuevos datos del modelo</param>
        Modelo Add(Modelo modelo);

        /// <summary>
        /// Elimina un modelo
        /// </summary>
        /// <param name="modeloId">ID del modelo a eliminar</param>
        void Delete(long modeloId);

        // ************ AUXILIARES ************

        /// <summary>
        /// Comprueba si el modelo existe
        /// </summary>
        /// <param name="id">Id del modelo</param>
        /// <returns></returns>
        bool Exists(long id);

        /// <summary>
        /// Comprueba si el modelo existe
        /// </summary>
        /// <param name="id">ID del modelo</param>
        /// <param name="nombre">Nombre del modelo</param>
        /// <returns></returns>
        bool Exists(long? id, string nombre);
    }
}
