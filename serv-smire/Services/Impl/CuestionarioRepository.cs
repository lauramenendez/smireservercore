﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Models;
using serv_smire.Models.Cuestionarios;
using serv_smire.Helper.PDF;
using System.Globalization;
using System.Threading.Tasks;
using serv_smire.Helper.Enumerators;

namespace serv_smire.Services.Impl
{
    public class CuestionarioRepository : ICuestionarioRepository
    {
        /// <summary>
        /// Representa la base de datos
        /// </summary>
        private readonly DataModel db;
        private readonly GeneratePDF pdf;

        public CuestionarioRepository(DataModel _db, GeneratePDF _pdf)
        {
            db = _db;
            pdf = _pdf;
        }

        // ************ OPERACIONES GET ************

        public Cuestionario GetCuestionario(long id)
        {
            return db.Cuestionarios
                .AsNoTracking()
                .Include(c => c.Modelos)
                    .ThenInclude(m => m.Bloques)
                        .ThenInclude(b => b.Preguntas)
                            .ThenInclude(p => p.TiposRespuestas)
                                .ThenInclude(t => t.Etiquetas)
                .Include(c => c.ArchivosInstrucciones)
                .Where(c => c.ID == id)
                .SingleOrDefault();
        }

        private async Task<Cuestionario> GetCuestionarioAsync(long id)
        {
            return await db.Cuestionarios
                .AsNoTracking()
                .Include(c => c.Modelos)
                    .ThenInclude(m => m.Bloques)
                        .ThenInclude(b => b.Preguntas)
                            .ThenInclude(p => p.TiposRespuestas)
                                .ThenInclude(t => t.Etiquetas)
                .Include(c => c.ArchivosInstrucciones)
                .Where(c => c.ID == id)
                .SingleOrDefaultAsync();
        }

        public ICollection<Cuestionario> GetCuestionariosUsuario(long idUser)
        {
            ICollection<Cuestionario> cuestionarios = db.Cuestionarios
                .AsNoTracking()
                .Include(c => c.Modelos)
                    .ThenInclude(m => m.Bloques)
                .Include(c => c.ArchivosInstrucciones)
                .Include(c => c.Generico)
                .Where(c => c.Experto.ID == idUser)
                .ToList();

            foreach (Cuestionario c in cuestionarios)
            {
                foreach (Modelo m in c.Modelos)
                {
                    m.Bloques = m.Bloques.OrderBy(b => b.Posicion).ToList();
                }
            }

            return cuestionarios;
        }

        public ICollection<Cuestionario> GetCuestionarios()
        {
            return db.Cuestionarios
                .AsNoTracking()
                .Include(c => c.Generico)
                .Include(c => c.Experto)
                .ToList();
        }

        public async Task<ICollection<Cuestionario>> GetCuestionariosActivos()
        {
            return await db.Cuestionarios
                .AsNoTracking()
                .Include(c => c.Generico)
                .Where(c => c.Activo)
                .Select(c => new Cuestionario
                {
                    ID = c.ID,
                    Titulo = c.Titulo,
                    GenericoUsername = c.Generico.Usuario,
                    Activo = c.Activo
                })
                .ToListAsync();
        }

        public Cuestionario GetCuestionarioModelo(long idCuestionario, long idModelo)
        {
            Cuestionario cuest = GetCuestionario(idCuestionario);

            if (cuest != null)
            {
                if (cuest.Modelos.Any(m => m.ID == idModelo))
                {
                    cuest.ModeloEscogido = cuest.Modelos.Where(m => m.ID == idModelo).SingleOrDefault();
                }
                else
                {
                    cuest.ModeloEscogido = cuest.Modelos.ElementAt(0);
                }

                cuest.ModeloEscogido.Ordenar();

                cuest.Modelos = null;
                return cuest;
            }
            else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        public async Task<Cuestionario> GetCuestionarioModelo(long idCuestionario)
        {
            // Busco el cuestionario con ese id
            Cuestionario cuest = await GetCuestionarioAsync(idCuestionario);

            if (cuest != null)
            {
                // Escojo un modelo aleatorio
                Modelo modelo = cuest.Modelos.ElementAt(new Random().Next(cuest.Modelos.Count));

                // Ordeno segun su posicion
                modelo.Ordenar();

                // Elimino la lista de modelos
                cuest.Modelos = null;

                // Asigno el modelo escogido y devuelvo el cuestionario con este
                cuest.ModeloEscogido = modelo;
                return cuest;
            }
            else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        public async Task<Cuestionario> GetCuestionarioModelo(long idCuestionario, string identificador)
        {
            // Compruebo si hay respuestas
            ICollection<Respuesta> respuestas = await db.Respuestas
                .AsNoTracking()
                .Include(r => r.TipoRespuesta.Pregunta.Bloque)
                .Where(r => r.Identificador.Equals(identificador) && r.TipoRespuesta.Pregunta.Bloque.Modelo.Cuestionario_ID == idCuestionario)
                .ToListAsync();

            // Si las hay entonces devuelvo el modelo respondido con sus respuestas
            if (respuestas.Count > 0) {
                // Busco el cuestionario con ese id
                Cuestionario cuest = await GetCuestionarioAsync(idCuestionario);

                if (cuest != null)
                {
                    // Escojo el modelo ya respondido
                    var modeloId = respuestas.FirstOrDefault().TipoRespuesta.Pregunta.Bloque.Modelo_ID;
                    Modelo modelo = cuest.Modelos.Where(m => m.ID == modeloId).SingleOrDefault();

                    // Ordeno segun su posicion
                    modelo.Ordenar();

                    // Elimino la lista de modelos
                    cuest.Modelos = null;

                    // Asigno el modelo escogido y devuelvo el cuestionario con este
                    cuest.ModeloEscogido = modelo;

                    // Obtenemos las respuestas (tipo, valor)
                    cuest.Respuestas = respuestas.Aggregate(new Dictionary<long, string>(), (dic, resp) =>
                    {
                        dic.Add(resp.TipoRespuesta_ID, resp.Valor);
                        return dic;
                    });

                    return cuest;
                }
                else
                {
                    throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
                }
            } else
            {
                // Si no hay respuestas, devuelvo un modelo aleatorio
                return await GetCuestionarioModelo(idCuestionario);
            }
        }

        // ************ OPERACIONES CRUD ************
        public Cuestionario Add(Cuestionario cuestionario)
        {
            // Viene con el modelo 0 y los bloques creados
            if (cuestionario != null)
            {
                db.Cuestionarios.Attach(cuestionario);
                db.Cuestionarios.Add(cuestionario);
                db.SaveChanges();

                return GetCuestionario(cuestionario.ID);
            }

            return null;
        }

        public void Delete(long cuestionarioId)
        {
            // Solo podemos borrarlo si ya se han descargado las respuestas o no tiene ninguna respuesta
            Cuestionario cuestionario = db.Cuestionarios.AsNoTracking().Where(c => c.ID == cuestionarioId).SingleOrDefault();
            if (cuestionario != null && (cuestionario.NumeroRespuestas == 0 || cuestionario.RespuestasDescargadas))
            {
                db.Cuestionarios.Remove(cuestionario);
                db.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.DeleteCuestionario).ToString());
            }
        }

        public Cuestionario Update(Cuestionario cuestionario)
        {
            if (cuestionario != null)
            {
                Modelo modelo = cuestionario.Modelos
                    .Where(m => m.Nombre.ToLower(CultureInfo.InvariantCulture).Equals(db.ModeloOriginal))
                    .SingleOrDefault();

                db.Entry(cuestionario).State = EntityState.Modified;

                if (modelo != null)
                {
                    DeleteBloques(modelo.Bloques, cuestionario.ID);
                    UpdateBloques(modelo);
                }
                db.SaveChanges();

                return cuestionario;
            }
            else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        // ************ OTRAS OPERACIONES ************

        public void UpdatePosiciones(long cuestionarioId, long bloque1, long bloque2)
        {
            Bloque b1 = db.Bloques
                .AsNoTracking()
                .Where(b => b.Modelo.Cuestionario_ID == cuestionarioId && b.ID == bloque1)
                .SingleOrDefault();
            Bloque b2 = db.Bloques
                .AsNoTracking()
                .Where(b => b.Modelo.Cuestionario_ID == cuestionarioId && b.ID == bloque2)
                .SingleOrDefault();

            if (b1 != null && b2 != null)
            {
                int pos1 = b1.Posicion;
                b1.Posicion = b2.Posicion;
                b2.Posicion = pos1;

                db.Entry(b1).State = EntityState.Modified;
                db.Entry(b2).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        public IDictionary<string, object> GetRespuestas(string[] cuestionarios)
        {
            var respCuestionarios = new Dictionary<string, object>();

            if (cuestionarios != null)
            {
                foreach (string titulo in cuestionarios)
                {
                    var respuestas = db.Respuestas
                        .AsNoTracking()
                        .Include(r => r.TipoRespuesta.Pregunta.Bloque.Modelo)
                        .Where(r => r.TipoRespuesta.Pregunta.Bloque.Modelo.Cuestionario.Titulo.Equals(titulo)
                            && !r.TipoRespuesta.NombreTipo.Equals(ETiposRespuesta.NoAplicable))
                        .ToList();

                    var ordenadas = respuestas
                        .OrderBy(r => r.TipoRespuesta.Pregunta.Bloque.Modelo.Nombre)
                        .ThenBy(r => r.TipoRespuesta.Pregunta.Bloque.Posicion)
                        .ThenBy(r => r.TipoRespuesta.Pregunta.Posicion)
                        .ToList();

                    var formateadas = ordenadas.Select(r => new
                    {
                        r.Identificador,
                        Modelo = r.TipoRespuesta.Pregunta.Bloque.Modelo.Nombre,
                        Bloque = r.TipoRespuesta.Pregunta.Bloque.Nombre,
                        Pregunta = r.TipoRespuesta.Pregunta.Posicion,
                        Tipo = r.TipoRespuesta.GetNombreTipo(),
                        r.Valor,
                        Fecha = r.FechaRespuesta.ToString(),
                    }).ToList();

                    respCuestionarios.Add(titulo, formateadas);

                    // Actualizar cuestionario (respuestas descargadas a verdadero)
                    Cuestionario cuestionario = db.Cuestionarios.AsNoTracking().Where(c => c.Titulo.Equals(titulo)).SingleOrDefault();
                    cuestionario.RespuestasDescargadas = true;

                    db.Entry(cuestionario).State = EntityState.Modified;
                }

                db.SaveChanges();
            }
            return respCuestionarios;
        }

        public byte[] ExportCuestionario(long id)
        {
            // El modelo 0, el original
            Cuestionario cuestionario = GetCuestionarioModelo(id, -1);
            return pdf.FromSource(cuestionario);
        }

        // ************ AUXILIARES ************

        private void DeleteBloques(ICollection<Bloque> bloques, long cuestionarioID)
        {
            ICollection<Bloque> bloquesBorrar = bloques.Where(b => b.Deleted).ToList();

            foreach(Bloque bloque in bloquesBorrar)
            {
                db.Bloques.Remove(db.Bloques.Find(bloque.ID));
            }
        }

        private void UpdateBloques(Modelo modelo)
        {
            int posicion = 1;
            foreach (Bloque bloque in modelo.Bloques.Where(b => !b.Deleted).OrderBy(b => b.Posicion).ToList())
            {
                bloque.Posicion = posicion;

                if (bloque.ID == default(long))
                {
                    db.Entry(bloque).State = EntityState.Added;
                }
                else
                {
                    db.Entry(bloque).State = EntityState.Modified;
                }

                posicion++;
            }

            db.Entry(modelo).State = EntityState.Modified;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            db.Dispose();
        }

        public bool Exists(long id)
        {
            return db.Cuestionarios.AsNoTracking().Count(e => e.ID == id) > 0;
        }

        public bool Exists(string titulo, long id = 0)
        {
            if(id != 0)
            {
                return db.Cuestionarios.AsNoTracking().Count(e => e.Titulo.Equals(titulo) && e.ID != id) > 0;
            } 
            return db.Cuestionarios.AsNoTracking().Count(e => e.Titulo.Equals(titulo)) > 0;
        }

        public bool BloqueExists(long cuestionarioId, long idBloque)
        {
            return db.Bloques.AsNoTracking().Count(b => b.ID == idBloque && b.Modelo.Cuestionario_ID == cuestionarioId) > 0;
        }
    }
}