﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using serv_smire.Helper.Validate;
using serv_smire.Models;
using serv_smire.Models.Cuestionarios;
using serv_smire.Models.Cuestionarios.TiposRespuestas;

namespace serv_smire.Services.Impl
{
    public class RespuestaRepository : IRespuestaRepository
    {
        /// <summary>
        /// Representa la base de datos
        /// </summary>
        private readonly DataModel db;

        public RespuestaRepository(DataModel _db)
        {
            db = _db;
        }

        // ************ OPERACIONES GET ************
        public ICollection<Respuesta> GetRespuestas(string identificador)
        {
            return db.Respuestas
                .AsNoTracking()
                .Where(r => r.Identificador.Equals(identificador))
                .ToList();
        }

        public ICollection<Respuesta> GetRespuestas(string username, string cuestionario)
        {
            return db.Respuestas
                .AsNoTracking()
                .Where(r => r.TipoRespuesta.Pregunta.Bloque.Modelo.Cuestionario.Titulo.Equals(cuestionario)
                    && r.TipoRespuesta.Pregunta.Bloque.Modelo.Cuestionario.Experto.Usuario.Equals(username))
                .ToList();
        }

        // ************ OPERACIONES CRUD ************
        public async Task Add(Respuesta[] respuestas)
        {
            if (respuestas != null && respuestas.Length > 0)
            {
                foreach (Respuesta respuesta in respuestas)
                {
                    // Tipo de respuesta
                    TipoRespuesta tipo = await db.TipoRespuestas
                        .AsNoTracking()
                        .Include(t => t.Etiquetas)
                        .Where(t => t.ID == respuesta.TipoRespuesta_ID)
                        .SingleOrDefaultAsync();

                    if (tipo != null)
                    {
                        // Comprobamos que el formato de respuesta es correcto
                        ValidateRespuesta.Validate(tipo, respuesta.Valor);

                        // Comprobamos si existe para añadirla o modificarla si ya existe
                        bool exists = await Exists(respuesta.Identificador, tipo.ID);
                        db.Entry(respuesta).State = (!exists ? EntityState.Added : EntityState.Modified);
                    }
                }

                // Guardamos todos los cambios
                await db.SaveChangesAsync();
            }
        }

        public void Delete(string identificador)
        {
            ICollection<Respuesta> respuestas = GetRespuestas(identificador);
            db.Respuestas.RemoveRange(respuestas);
            db.SaveChanges();
        }

        // ************ OTRAS OPERACIONES ************
        private async Task<bool> Exists(string identificador, long tipo)
        {
        return await db.Respuestas.AsNoTracking().CountAsync(e => e.Identificador.Equals(identificador) && e.TipoRespuesta_ID == tipo) > 0;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            db.Dispose();
        }
    }
}