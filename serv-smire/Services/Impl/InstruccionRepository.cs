﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Models;
using serv_smire.Models.Cuestionarios;

namespace serv_smire.Services.Impl
{
    public class InstruccionRepository : IInstruccionRepository
    {
        /// <summary>
        /// Representa la base de datos
        /// </summary>
        private readonly DataModel db;

        public InstruccionRepository(DataModel _db)
        {
            db = _db;
        }

        // ************ OPERACIONES GET ************

        public Instruccion GetInstruccion(long id)
        {
            return db.Instruccion.AsNoTracking().Where(i => i.ID == id).SingleOrDefault();
        }

        // ************ OPERACIONES CRUD ************

        public ICollection<Instruccion> Add(ICollection<Instruccion> instrucciones)
        {
            if (instrucciones != null)
            {
                foreach (Instruccion instruccion in instrucciones)
                {
                    if (!db.Instruccion.AsNoTracking().Any(i => i.Filename.Equals(instruccion.Filename)))
                    {
                        db.Entry(instruccion).State = EntityState.Added;
                    }
                }

                db.SaveChanges();
                return instrucciones;                
            }
            else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        public void Delete(long cuestionarioId)
        {
            var instrucciones = db.Instruccion.AsNoTracking().Where(i => i.Cuestionario_ID == cuestionarioId);
            db.Instruccion.RemoveRange(instrucciones);
            db.SaveChanges();
        }

        // ************ AUXILIARES ************

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            db.Dispose();
        }

        public bool Exists(long id)
        {
            return db.Instruccion.AsNoTracking().Count(i => i.ID == id) > 0;
        }
    }
}