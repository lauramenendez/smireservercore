﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Models;
using serv_smire.Models.Cuestionarios;
using serv_smire.Models.Cuestionarios.TiposRespuestas;

namespace serv_smire.Services.Impl
{
    public class PreguntaRepository : IPreguntaRepository
    {
        /// <summary>
        /// Representa la base de datos
        /// </summary>
        private readonly DataModel db;

        public PreguntaRepository(DataModel _db)
        {
            db = _db;
        }

        // ************ OPERACIONES GET ************
        public Pregunta GetPregunta(long id)
        { 
            return db.Preguntas
                .AsNoTracking()
                .Include(pr => pr.TiposRespuestas)
                .Where(pr => pr.ID == id)
                .SingleOrDefault();
        }

        private Pregunta GetPreguntaSinTR(long id)
        {
            return db.Preguntas
                .AsNoTracking()
                .Where(pr => pr.ID == id)
                .SingleOrDefault();
        }

        public ICollection<Pregunta> GetPreguntas(long cuestionarioId)
        {
            ICollection<Pregunta> preguntas = db.Preguntas
                .AsNoTracking()
                .Include(t => t.Bloque)
                .Include(t => t.TiposRespuestas)
                    .ThenInclude(e => e.Etiquetas)
                .Where(t => t.Bloque.Modelo.Cuestionario.ID == cuestionarioId && t.Bloque.Modelo.Nombre.ToLower().Equals(db.ModeloOriginal))
                .ToList();

            return preguntas
                .OrderBy(t => t.Bloque.Posicion)
                .ThenBy(t => t.Posicion)
                .ToList();
        }

        // ************ OPERACIONES CRUD ************
        public void Add(Pregunta pregunta)
        {
            if (pregunta != null)
            {
                pregunta.Posicion = GetUltimaPosicion(pregunta.Bloque_ID);
                
                // Puede estar duplicada, eliminamos los ids
                foreach(TipoRespuesta tr in pregunta.TiposRespuestas)
                {
                    tr.ID = default(long);

                    foreach (Etiqueta e in tr.Etiquetas)
                    {
                        e.ID = default(long);
                    }
                }

                db.Preguntas.Attach(pregunta);
                db.Preguntas.Add(pregunta);
                db.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        public void Delete(long preguntaId)
        {
            Pregunta pregunta = GetPreguntaSinTR(preguntaId);
            if (pregunta != null)
            { 
                long? bloque_id = GetBloqueID(pregunta.ID);
                db.Preguntas.Remove(pregunta);
                db.SaveChanges();

                RecalcularPosiciones(bloque_id);
            }
            else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        public void Update(Pregunta pregunta)
        {
            if (pregunta != null && Exists(pregunta.ID))
            {
                // Si cambia de bloque, volvemos a calcular la posicion
                if (pregunta.Bloque_ID != GetBloqueID(pregunta.ID))
                {
                    pregunta.Posicion = GetUltimaPosicion(pregunta.Bloque_ID);
                }

                // Actualizamos los tipos de respuesta
                UpdateTipos(pregunta.TiposRespuestas);

                db.Entry(pregunta).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        public void UpdatePosiciones(Pregunta pregunta1, Pregunta pregunta2)
        {

            if (pregunta1 != null && pregunta2 != null 
                && Exists(pregunta1.ID) && Exists(pregunta2.ID))
            {
                int pos1 = pregunta1.Posicion;
                pregunta1.Posicion = pregunta2.Posicion;
                pregunta2.Posicion = pos1;

                db.Entry(pregunta1).State = EntityState.Modified;
                db.Entry(pregunta2).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        // ************ AUXILIARES ************

        /// <summary>
        /// Añadimos y actualizamos los tipos asociados a una pregunta, y ademas
        /// comprobamos si hay que eliminarlos y recalculamos sus posiciones
        /// </summary>
        /// <param name="tipos"></param>
        private void UpdateTipos(ICollection<TipoRespuesta> tipos)
        {
            if (tipos != null && tipos.Count > 0)
            {
                // Recalculamos las posiciones y miramos si hay que modificar/añadir
                List<TipoRespuesta> trs = tipos.Where(t => !t.Deleted).OrderBy(t => t.Posicion).ToList();
                int pos = 1;
                foreach (TipoRespuesta tr in trs)
                {
                    tr.Posicion = pos;
                    pos++;
                    
                    // Actualizamos las etiquetas de los tipos
                    if (tr.ID == default(long))
                    {
                        db.Entry(tr).State = EntityState.Added;
                        UpdateEtiquetas(tr.Etiquetas);
                    } else
                    {
                        UpdateEtiquetas(tr.Etiquetas);

                        tr.Etiquetas = tr.Etiquetas.Where(e => !e.Deleted).ToList();
                        db.Entry(tr).State = EntityState.Modified;
                    }                    
                }

                // Comprobamos si hay que eliminar algun tipo
                List<TipoRespuesta> trsEliminar = tipos.Where(t => t.Deleted).ToList();
                foreach (TipoRespuesta tr in trsEliminar)
                {
                    if (tr.ID != default(long))
                    {
                        if (!CheckIfAttachedTipo(tr.ID))
                        {
                            db.TipoRespuestas.Attach(tr);
                        }

                        db.Etiquetas.RemoveRange(tr.Etiquetas);
                        db.Entry(tr).State = EntityState.Deleted;
                    }
                }
            }
        }

        /// <summary>
        /// Añadimos y actualizamos las etiquetas asociadas una pregunta, y ademas
        /// comprobamos si hay que eliminarlas y recalculamos sus posiciones
        /// </summary>
        /// <param name="etiquetas"></param>
        private void UpdateEtiquetas(ICollection<Etiqueta> etiquetas)
        {
            if (etiquetas != null && etiquetas.Count > 0)
            {
                // Recalculamos las posiciones y miramos si hay que modificar/añadir
                List<Etiqueta> etiqs = etiquetas.Where(e => !e.Deleted).OrderBy(e => e.Posicion).ToList();
                int pos = 1;
                foreach (Etiqueta etiq in etiqs)
                {
                    etiq.Posicion = pos;
                    pos++;

                    db.Entry(etiq).State = (etiq.ID == default(long) ? EntityState.Added : EntityState.Modified);
                }

                // Comprobamos si hay que eliminar alguna
                List<Etiqueta> etiqsEliminar = etiquetas.Where(e => e.Deleted).ToList();
                foreach (Etiqueta etiq in etiqsEliminar)
                {
                    if (etiq.ID != default(long))
                    {
                        if(!CheckIfAttachedEtiqueta(etiq.ID))
                        {
                            db.Etiquetas.Attach(etiq);
                        }

                        db.Entry(etiq).State = EntityState.Deleted;
                    }
                }
            }
        }

        /// <summary>
        /// Recalculamos las posiciones de las preguntas dentro de un bloque
        /// (generalmente cuando se borra alguna)
        /// </summary>
        /// <param name="bloque_id"></param>
        private void RecalcularPosiciones(long? bloque_id)
        {
            // Recalculamos las posiciones de las preguntas del bloque
            var preguntas = db.Preguntas
                .AsNoTracking()
                .Where(p => p.Bloque_ID == bloque_id)
                .OrderBy(p => p.Posicion)
                .ToList();

            int pos = 1;
            foreach (Pregunta p in preguntas)
            {
                p.Posicion = pos;
                pos++;

                db.Entry(p).State = EntityState.Modified;
            }

            db.SaveChanges();
        }

        /// <summary>
        /// Devolvemos la ultima posicion del bloque
        /// </summary>
        /// <param name="bloqueID"></param>
        /// <returns></returns>
        private int GetUltimaPosicion(long? bloqueID)
        {
            Bloque bloque = db.Bloques
                .AsNoTracking()
                .Include(b => b.Preguntas)
                .Where(b => b.ID == bloqueID)
                .SingleOrDefault();

            if (bloque != null)
            {
                // Para que no empiece en 0 que queda feo
                return bloque.Preguntas.Count + 1;
            }

            return 1;
        }

        /// <summary>
        /// Devolvemos el ID del bloque asociado a esa pregunta
        /// </summary>
        /// <param name="preguntaID"></param>
        /// <returns></returns>
        private long? GetBloqueID(long preguntaID)
        {
            return db.Preguntas.AsNoTracking().Where(p => p.ID == preguntaID).SingleOrDefault().Bloque_ID;
        }


        // ************ OTRAS OPERACIONES ************
        public bool Exists(long id)
        {
            return db.Preguntas.AsNoTracking().Count(e => e.ID == id) > 0;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            db.Dispose();
        }

        private bool CheckIfAttachedTipo(long tipoId)
        {
            return db.TipoRespuestas.Local.Any(e => e.ID == tipoId);
        }

        private bool CheckIfAttachedEtiqueta(long etiqId)
        {
            return db.Etiquetas.Local.Any(e => e.ID == etiqId);
        }
    }
}