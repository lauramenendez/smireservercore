﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Helper.Security;
using serv_smire.Helper.Validate;
using serv_smire.Models;
using serv_smire.Models.Cuestionarios;
using serv_smire.Models.Usuario;

namespace serv_smire.Services.Impl
{
    public class ExpertoRepository : IExpertoRepository
    {
        /// <summary>
        /// Representa la base de datos
        /// </summary>
        private readonly DataModel db;

        public ExpertoRepository(DataModel _db)
        {
            db = _db;
        }

        // ************ OPERACIONES GET ************

        public Experto GetExperto(long id)
        {
            return db.Expertos.AsNoTracking().Where(e => e.ID == id).SingleOrDefault();
        }

        public Experto GetExperto(string usuario)
        {
            return db.Expertos.AsNoTracking().Where(p => p.Usuario.Equals(usuario)).SingleOrDefault();
        }

        public Experto GetExperto(string usuario, long id)
        {
            Experto e = GetExperto(id);
            if (e != null && e.Usuario.Equals(usuario))
            {
                return e;
            }
            return null;
        }

        public ICollection<Experto> GetExpertos()
        {
            return db.Expertos.AsNoTracking().Where(e => !e.EsAdmin).ToList();
        }

        // ************ OPERACIONES CRUD ************

        public void Add(Experto experto)
        {
            if (experto != null && ValidateContrasena.Tamaño(experto.Contrasena) && !experto.EsAdmin)
            {
                string sal = Encrypt.GenerateSalt(10);
                string contrasena = Encrypt.EncodePassword(experto.Contrasena, sal);
                experto.Contrasena = contrasena;
                experto.Codigo = sal;

                db.Expertos.Add(experto);
                db.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        public void Delete(long expertoId)
        {
            // Desactivo todos sus cuestionarios asociados antes de eliminarlo
            IQueryable<Cuestionario> cuestionarios = db.Cuestionarios.AsNoTracking().Where(c => c.Experto_ID == expertoId);
            foreach (Cuestionario cuestionario in cuestionarios)
            {
                cuestionario.Activo = false;
                db.Entry(cuestionario).State = EntityState.Modified;
            }

            Experto experto = GetExperto(expertoId);
            if (experto != null && !experto.EsAdmin)
            {
                db.Expertos.Remove(experto);
                db.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        public void Update(Experto usuario)
        {
            if (usuario != null)
            {
                Experto antiguo = GetExperto(usuario.ID);
                antiguo.Nombre = usuario.Nombre;
                antiguo.Apellidos = usuario.Apellidos;
                antiguo.Email = usuario.Email;
                antiguo.Departamento = usuario.Departamento;
                antiguo.Usuario = usuario.Usuario;
                antiguo.Aceptado = usuario.Aceptado;

                db.Entry(antiguo).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        public void UpdateContrasena(Experto usuario)
        {
            if (usuario != null)
            {
                ValidateContrasena.Tamaño(usuario.NuevaContrasena);

                Experto antiguo = GetExperto(usuario.ID);
                if (antiguo != null) {
                    string contrasena = ValidateContrasena.GetContrasenaEnviada(usuario.Contrasena, antiguo.Codigo);
                    string repContrasena = ValidateContrasena.GetContrasenaEnviada(usuario.RepContrasena, antiguo.Codigo);
                    string nuevaContrasena = ValidateContrasena.GetContrasenaEnviada(usuario.NuevaContrasena, antiguo.Codigo);

                    ValidateContrasena.Antigua(antiguo.Contrasena, contrasena);
                    ValidateContrasena.Coinciden(contrasena, repContrasena);
                    ValidateContrasena.Distinta(antiguo.Contrasena, nuevaContrasena);

                    var sal = Encrypt.GenerateSalt(10);
                    contrasena = Encrypt.EncodePassword(usuario.NuevaContrasena, sal);
                    antiguo.Contrasena = contrasena;
                    antiguo.Codigo = sal;

                    db.Entry(antiguo).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        // ************ OTRAS OPERACIONES ************

        public ICollection<string> GetCorreosAdmin()
        {
            return db.Expertos.AsNoTracking().Where(e => e.EsAdmin == true).Select(e => e.Email).ToList();
        }

        public bool CheckExistUsername(string username)
        {
            return db.Expertos.AsNoTracking().Where(e => e.Usuario.Equals(username)).Count() > 0;
        }

        // ************ AUXILIARES ************

        public bool Exists(long id)
        {
            return db.Expertos.AsNoTracking().Count(e => e.ID == id) > 0;
        }

        public bool Exists(string user, long id = 0)
        {
            if(id != 0)
            {
                return db.Expertos.AsNoTracking().Count(e => e.Usuario.Equals(user) && e.ID != id) > 0;
            }
            return db.Expertos.AsNoTracking().Count(e => e.Usuario.Equals(user)) > 0;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            db.Dispose();
        }
    }
}