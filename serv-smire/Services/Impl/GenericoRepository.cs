﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Helper.Security;
using serv_smire.Models;
using serv_smire.Models.Usuario;
using serv_smire.Helper.Validate;

namespace serv_smire.Services.Impl
{
    public class GenericoRepository : IGenericoRepository
    {
        /// <summary>
        /// Representa la base de datos
        /// </summary>
        private readonly DataModel db;

        public GenericoRepository(DataModel _db)
        {
            db = _db;
        }

        // ************ OPERACIONES GET ************

        public Generico GetGenerico(long id)
        {
            return db.Genericos.AsNoTracking().Where(g => g.ID == id).SingleOrDefault();
        }

        public Generico GetGenerico(string username)
        {
            return db.Genericos
                .AsNoTracking()
                .Include(g => g.Cuestionarios)
                .Where(g => g.Usuario.Equals(username))
                .SingleOrDefault();
        }

        public ICollection<Generico> GetGenericos()
        {
            ICollection<Generico> genericos = db.Genericos
                .AsNoTracking()
                .Include(g => g.Experto)
                .Include(g => g.Cuestionarios)
                .ToList();

            // Decodificamos las contraseñas para mostrarlas a los expertos
            return genericos
                .Select(g =>
                {
                    if (g.Experto != null)
                    {
                        g.Experto.Cuestionarios = null;
                        g.Experto.Genericos = null;
                    }

                    g.Contrasena = Encrypt.base64Decode(g.Contrasena);
                    return g;
                }).ToArray();
        }

        // ************ OPERACIONES CRUD ************

        public void Add(Generico generico)
        {
            if (generico != null && ValidateContrasena.Tamaño(generico.Contrasena))
            {
                generico.Contrasena = Encrypt.base64Encode(generico.Contrasena);

                db.Genericos.Add(generico);
                db.SaveChanges();
            } else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        public void Update(Generico generico)
        {
            if (generico != null)
            {
                ValidateContrasena.Tamaño(generico.Contrasena);

                // Comprobamos que no sea igual a la que habia
                Generico antiguo = GetGenerico(generico.ID);
                if (antiguo != null)
                {
                    string antiguaContrasena = Encrypt.base64Decode(antiguo.Contrasena);
                    antiguo.Usuario = generico.Usuario;
                    if (!antiguaContrasena.Equals(generico.Contrasena))
                    {
                        // Si no es igual, la encriptamos y se la asignamos 
                        antiguo.Contrasena = Encrypt.base64Encode(generico.Contrasena);
                    }

                    db.Entry(antiguo).State = EntityState.Modified;
                    db.SaveChanges();
                }
            } else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        public void Delete(long genericoId)
        {
            Generico generico = GetGenerico(genericoId);

            if (generico!= null && !db.Cuestionarios.AsNoTracking().Where(c => c.Generico_ID == genericoId).Any())
            {
                db.Genericos.Remove(generico);
                db.SaveChanges();
            } else
            {
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.DeleteGenerico).ToString());
            }
        }

        // ************ OTRAS OPERACIONES ************

        public bool CheckExistUsername(string username)
        {
            return db.Genericos.AsNoTracking().Where(e => e.Usuario.Equals(username)).Count() > 0;
        }

        public bool CheckIdentificador(string identificador)
        {
            return db.Respuestas.AsNoTracking().Where(r => r.Identificador.Equals(identificador)).Count() > 0;
        }

        // ************ AUXILIARES ************

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            db.Dispose();
        }

        public bool Exists(long id)
        {
            return db.Genericos.AsNoTracking().Count(e => e.ID == id) > 0;
        }

        public bool Exists(string user, long id = 0)
        {
            if (id != 0)
            {
                return db.Genericos.AsNoTracking().Where(e => e.Usuario.Equals(user) && e.ID != id).Count() > 0;
            }

            return db.Genericos.AsNoTracking().Where(e => e.Usuario.Equals(user)).Count() > 0;
        }
    }
}