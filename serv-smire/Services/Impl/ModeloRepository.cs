﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using serv_smire.Helper.ErrorHandler;
using serv_smire.Models;
using serv_smire.Models.Cuestionarios;

namespace serv_smire.Services.Impl
{
    public class ModeloRepository : IModeloRepository
    {
        /// <summary>
        /// Representa la base de datos
        /// </summary>
        private readonly DataModel db;

        public ModeloRepository(DataModel _db)
        {
            db = _db;
        }

        // ************ OPERACIONES GET ************

        public Modelo GetModelo(long id)
        {
            return db.Modelos.AsNoTracking().Where(m => m.ID == id).SingleOrDefault();
        }

        public ICollection<Modelo> GetModelos(string cuestionario)
        {
            return db.Modelos.AsNoTracking().Where(m => m.Cuestionario.Titulo.Equals(cuestionario)).ToList();
        }

        // ************ OPERACIONES CRUD ************

        public Modelo Add(Modelo modelo)
        {
            if (modelo != null) { 
                // Busco el modelo original, modelo 0, es decir el propio cuestionario
                Modelo original = db.Modelos
                    .AsNoTracking()
                    .Include(m => m.Bloques)
                    .ThenInclude(b => b.Preguntas)
                    .ThenInclude(p => p.TiposRespuestas)
                    .ThenInclude(t => t.Etiquetas)
                    .Where(m => m.Cuestionario.ID == modelo.Cuestionario_ID && m.Nombre.ToLower().Equals(db.ModeloOriginal))
                    .SingleOrDefault();

                if (original != null)
                {
                    // Le indico al nuevo modelo que a partir del original cree los bloques, con sus preguntas etc
                    // y escoja las posiciones en funcion de lo que haya optado el usuario
                    modelo.ConmutarBloques(original);
                    // Le asigno el nombre que toca
                    modelo.Nombre = NuevoNombre(original.Cuestionario_ID);

                    db.Modelos.Attach(modelo);
                    db.Modelos.Add(modelo);
                    db.SaveChanges();
                }
                else
                {
                    // No existe el cuestionario al cual se supone que pertenece el modelo
                    throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
                }

                return modelo;
            }
            else
            {
                // Parametro incorrecto
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        public void Delete(long modeloId)
        {
            Modelo modelo = GetModelo(modeloId);
            if (modelo != null && !modelo.Nombre.ToLower().Equals(db.ModeloOriginal))
            {
                db.Modelos.Remove(modelo);
                db.SaveChanges();
            }
            else
            {
                // Parametro incorrecto
                throw new InvalidOperationException(ErrorMsg.getMsg(eError.InvalidParam).ToString());
            }
        }

        // ************ AUXILIARES ************

        public bool Exists(long id)
        {
            return db.Modelos.AsNoTracking().Count(m => m.ID == id) > 0;
        }

        public bool Exists(long? id, string nombre)
        {
            return db.Modelos.AsNoTracking().Count(m => m.Nombre.Equals(nombre) && m.Cuestionario_ID == id) > 0;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            db.Dispose();
        }

        // ************ OTRAS OPERACIONES ************

        private string NuevoNombre(long? cuestionario)
        {
            var modelos = db.Modelos
                .AsNoTracking()
                .Where(m => m.Cuestionario_ID == cuestionario)
                .OrderBy(m => m.Nombre)
                .ToList();

            var ultimoModelo = modelos.ElementAt(modelos.Count - 1);

            int indice;
            bool correcta = Int32.TryParse(ultimoModelo.Nombre.Split(null)[1], out indice);

            if (correcta)
            {
                return "Modelo " + (indice + 1);
            }

            return null;
        }
    }
}