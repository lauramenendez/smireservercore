﻿using System;
using System.Collections.Generic;
using serv_smire.Models.Cuestionarios;

namespace serv_smire.Services
{
    /// <summary>
    /// Clase que representa las operaciones a realizar contra la base de datos 
    /// para gestionar las preguntas
    /// </summary>
    public interface IPreguntaRepository : IDisposable
    {
        // ************ OPERACIONES GET ************

        /// <summary>
        /// Devuelve una pregunta concreta segun su ID
        /// </summary>
        /// <param name="id">ID de la pregunta</param>
        /// <returns></returns>
        Pregunta GetPregunta(long id);

        /// <summary>
        /// Devuelve todas las preguntas de un cuestionario
        /// </summary>
        /// <param name="cuestionarioId">ID del cuestionario</param>
        /// <returns></returns>
        ICollection<Pregunta> GetPreguntas(long cuestionarioId);

        // ************ OPERACIONES CRUD ************

        /// <summary>
        /// Actualiza los datos de una pregunta
        /// </summary>
        /// <param name="pregunta">Pregunta a actualizar</param>
        void Update(Pregunta pregunta);

        /// <summary>
        /// Intercambia las posiciones de las preguntas
        /// </summary>
        /// <param name="pregunta1">Pregunta 1 a actualizar</param>
        /// <param name="pregunta2">Pregunta 2 a actualizar</param>
        void UpdatePosiciones(Pregunta pregunta1, Pregunta pregunta2);

        /// <summary>
        /// Añade una nueva pregunta
        /// </summary>
        /// <param name="pregunta">Datos de la nueva pregunta</param>
        void Add(Pregunta pregunta);

        /// <summary>
        /// Elimina una pregunta asi como sus tipos asociados
        /// </summary>
        /// <param name="preguntaId">Pregunta a eliminar</param>
        void Delete(long preguntaId);

        // ************ AUXILIARES ************

        /// <summary>
        /// Comprueba si la pregunta con ese ID existe
        /// </summary>
        /// <param name="id">ID de la pregunta</param>
        /// <returns></returns>
        bool Exists(long id);
    }
}
