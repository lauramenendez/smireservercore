﻿using System;
using System.Collections.Generic;
using serv_smire.Models.Usuario;

namespace serv_smire.Services
{
    /// <summary>
    /// Clase que representa las operaciones a realizar contra la base de datos 
    /// para gestionar los usuarios genéricos
    /// </summary>
    public interface IGenericoRepository : IDisposable
    {
        // ************ OPERACIONES GET ************

        /// <summary>
        /// Devuelve el usuario generico segun su ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Generico GetGenerico(long id);

        /// <summary>
        /// Devuelve el usuario generico segun su username junto con sus cuestionarios
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        Generico GetGenerico(string username);

        /// <summary>
        /// Devuelve todos los usuarios genericos con las contraseñas decodificadas
        /// </summary>
        /// <returns></returns>
        ICollection<Generico> GetGenericos();

        // ************ OPERACIONES CRUD ************

        /// <summary>
        /// Añade un nuevo usuario generico
        /// </summary>
        /// <param name="generico">Datos del usuario</param>
        void Add(Generico generico);

        /// <summary>
        /// Actualiza un usuario generico
        /// </summary>
        /// <param name="generico">Datos del usuario</param>
        void Update(Generico generico);

        /// <summary>
        /// Elimina un usuario generico
        /// </summary>
        /// <param name="genericoId">ID del usuario</param>
        void Delete(long genericoId);

        // ************ OTRAS OPERACIONES ************

        /// <summary>
        /// Comprueba si el nombre de usuario esta disponible en el sistema
        /// </summary>
        /// <param name="username"></param>
        /// <returns>True si esta disponible, false si no</returns>
        bool CheckExistUsername(string username);

        /// <summary>
        ///  Comprueba si el identificador existe
        /// </summary>
        /// <param name="identificador"></param>
        /// <returns>True si exsite, false si no</returns>
        bool CheckIdentificador(string identificador);

        // ************ AUXILIARES ************

        /// <summary>
        /// Comprueba si el usuario generico ya existe
        /// </summary>
        /// <param name="id">ID del usuario generico</param>
        /// <returns></returns>
        bool Exists(long id);

        /// <summary>
        /// Comprueba si el usuario generico ya existe excluyendose asi mismo
        /// </summary>
        /// <param name="user">Nombre del usuario generico</param>
        /// <param name="id">ID del usuario generico</param>
        /// <returns></returns>
        bool Exists(string user, long id = 0);
    }
}
